Rozdział 5: Listy dowiązane
***************************

W tym rozdziale przedstawimy strukturę danych znaną jako lista
dowiązana, która stanowi alternatywę dla opartej na tablicach sekwencji
Pythona. Zarówno sekwencje oparte na tablicach, jak i listy dowiązane
utrzymują elementy w określonej kolejności, używając bardzo różnych
stylów. Tablica zapewnia scentralizowaną reprezentację, z jedną częścią
pamięci używaną do przechowywania odwołań do elementów. Lista dowiązana
opiera się na bardziej rozproszonej reprezentacji, w której obiekt,
znany jako węzeł, jest przydzielany dla każdego elementu. Każdy węzeł
utrzymuje odniesienie do swojego elementu i jedno lub więcej odniesień
do sąsiednich węzłów w celu przedstawienia liniowego porządku sekwencji.

Jak zwykle, przy porównywaniu sekwencji opartych na tablicach i list
dowiązanych należy wziąć pod uwagę kompromis. Elementy listy dowiązanej
nie mogą być wydajnie dostępne za pomocą indeksu numerycznego i nie
możemy stwierdzić, badając węzeł, jego pozycji na liście. Jednak listy
dowiązane pozwalają uniknąć pewnych wad wymienionych powyżej dla
sekwencji opartych na macierzach.

Podrozdział 1: Listy dowiązane pojedyncze
-----------------------------------------

Lista dowiązana pojedynczo to zbiór węzłów, które tworzą ciąg liniowy.
Każdy węzeł przechowuje element pola, czyli odwołanie do obiektu
sekwencji, a także kolejne zmienne, czyli odwołanie do kolejnego węzła
listy, jak na poniższym rysunku.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura51.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Pierwszy i ostatni węzeł połączonej listy to odpowiednio początek i
koniec listy. Proces przechodzenia z jednego węzła do następnego, po
kolejnym odwołaniu do każdego węzła, aż do osiągnięcia końca listy,
nazywany jest przechodzeniem przez połączoną listę lub przeskakiwaniem
łączy lub przeskakiwaniem wskaźnika. Ogon jest węzłem, którego następną
referencją jest None.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura52.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Każdy węzeł jest reprezentowany jako obiekt, który przechowuje
odniesienie do swojego elementu i odwołanie do następnego węzła. Aby
reprezentować całą listę, musimy zachować odniesienie do nagłówka listy.
Każdy inny węzeł można zlokalizować, przechodząc przez listę. Oznacza
to, że nie ma potrzeby przechowywania bezpośredniego odniesienia do
końca listy, nawet jeśli jest to powszechna praktyka unikania takiego
przechodzenia. Podobnie przechowywana jest całkowita liczba węzłów
(rozmiar) listy, aby uniknąć przechodzenia przez listę w celu zliczenia
węzłów. Dla uproszczenia rozważymy, że element węzła jest osadzony
bezpośrednio w strukturze węzła, jak na poniższym rysunku.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura53.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

**Wstawianie elementu na początku listy dowiązanej pojedynczo**

LinkedList nie ma z góry ustalonego rozmiaru, ponieważ jest on równy lub
proporcjonalny do bieżącej liczby elementów. Na początku listy można
wstawić nowy element, jak pokazano w poniższym pseudokodzie. Tworzona
jest nowa instancja węzła, jej element jest ustawiany na nowy element,
jej następny link odnosi się do aktualnego nagłówka, a nagłówek listy
jest ustawiany tak, aby wskazywał na nowy węzeł::

 1  add_first(L,e):
 2    newnode = Node(e)
 3    newnode.next = L.head
 4    L.head = newnode
 5    L.size = L.size+1

Na poniższym rysunku pokazujemy wstawienie elementu na początku listy,
odpowiednio przed wstawieniem, po utworzeniu nowego węzła i po zmianie
nagłówka.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura54.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

**Wstawianie elementu na końcu listy dowiązanej pojedynczo**

Element można wstawić na końcu listy, pod warunkiem, że zachowamy
odniesienie do węzła końcowego, jak pokazano w poniższym pseudokodzie.
Tworzony jest nowy węzeł, z następną referencją równą None, następną
referencją bieżącego ogona wskazującą na ten nowy węzeł i z samą
referencją ogona zaktualizowaną do tego nowego węzła::

 1  add_last(L,e):
 2    newnode = Node(e)
 3    newnode.next = None
 4    L.tail.next = newnode
 5    L.tail = newnode
 6    L.size = L.size+1

Na poniższym rysunku pokazano wstawianie elementu na końcu listy
dowiązanej pojedynczo, odpowiednio przed wstawieniem, po utworzeniu
nowego węzła i po zmianie przypisania odniesienia do końca.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura55.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center
   
**Usuwanie elementu z listy dowiązanej pojedynczo**

Ta operacja jest zilustrowana w następującym fragmencie kodu::

 1  remove_first(L):
 2    if L.head == None: error  # the list is empty
 3    L.head = L.head.next
 4    L.size = L.size−1


Na poniższym rysunku przedstawiamy usuwanie elementu na początku listy
dowiązanej pojedynczo, odpowiednio przed usunięciem, po usunięciu starej
pozycji i w jej końcowej konfiguracji.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura56.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Zwróć uwagę, że usunięcie ostatniego węzła z listy dowiązanej pojedynczo
nie jest łatwym zadaniem. Aby to zrobić, musimy mieć dostęp do węzła
przed ostatnim węzłem, a można to zrobić wyjątkowo, zaczynając od
nagłówka listy i przeszukując ją przez całą jej zawartość. Wymaga to
liczby przeskoków łączy równej rozmiarowi listy. Jeśli chcemy skutecznie
wesprzeć taką operację, będziemy musieli podwójnie powiązać naszą listę.

Podrozdział 1.1: Implementacja stosu z listami dowiązanymi pojedynczo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

W tym podrozdziale przedstawiamy implementację w Pythonie abstrakcyjnego
typu danych stosu za pomocą listy dowiązanej pojedynczo. Ponieważ
wszystkie operacje na stosie wpływają na szczyt, orientujemy górę stosu
na początek listy. To najlepszy wybór: możemy sprawnie wstawiać i usuwać
elementy w stałym czasie na początku listy.

Klasa ``_Node`` jest zdefiniowana w celu reprezentowania każdego węzła
listy. Ta klasa nie może być bezpośrednio dostępna dla użytkownika
stosu, dlatego definiujemy ją jako niepubliczną, zagnieżdżoną klasę
klasy ``LinkedStack``. Definicja ``_Node`` jest pokazana w następującym kodzie::

 class _Node:
   __slots__ = '_element' , '_next'        # streamline memory usage
   def __init__ (self, element, next):     # initialize node’s fields
     self._element = element               # reference to user’s element
     self._next = next                     # reference to next node
       

Węzeł ma dwie zmienne instancji: ``_element`` i ``_next``. Definiujemy
``_slots_``, aby usprawnić wykorzystanie pamięci, ponieważ na jednej
liście może znajdować się wiele instancji węzłów. Konstruktor klasy
``_Node`` został zaprojektowany tak, aby umożliwić nam określenie wartości
początkowych dla obu pól nowo utworzonego węzła.

W poniższym kodzie każde wystąpienie stosu przechowuje dwie zmienne.
Element ``_head`` jest odwołaniem do węzła na początku listy (Brak, jeśli
stos jest pusty). Zmienna ``_size`` śledzi bieżącą liczbę elementów.

Implementacja ``push`` jest podobna do pseudokodu do wstawiania na początku
listy połączonej pojedynczo, jak opisano wcześniej. Wkładając nowy
element na stos, zmieniamy połączoną strukturę, wywołując konstruktor
klasy ``_Node`` w następujący sposób::

 self._head = self._Node(e, self._head)

Zauważ, że pole ``_next`` nowego węzła jest ustawione na istniejący górny
węzeł, a następnie ``self._head`` jest ponownie przypisywane do nowego
węzła::

 1  class LinkedStack:
 2    # Stack implementation with a singly linked list
 3
 4    # Node class nested in stack
 5    class Node:
 6      __slots__ = '_element' , '_next'
 7      def __init__(self, element, next):
 8        self._element = element
 9        self._next = next
 10
 11   # Stack methods
 12   def __init__(self):
 13     self._head = None
 14     self._size = 0
 15
 16   def __len__(self):
 17     return self._size
 18
 19   def is_empty(self):
 20     return self._size == 0
 21
 22   def push(self, e):
 23     self._head = self._Node(e, self._head)
 24     self._size += 1
 25
 26   def top(self):
 27     if self.is_empty( ):
 28       raise Empty('Stack is empty')
 29     return self._head._element
 30
 31   def pop(self):
 32     if self.is_empty( ):
 33       raise Empty('Stack is empty')
 34     answer = self._head._element
 35     self._head = self._head._next
 36     self._size −= 1
 37     return answer

Celem metody ``top`` jest zwrócenie elementu znajdującego się na szczycie
stosu. Gdy stos jest pusty, zgłaszany jest wyjątek ``Empty``. Gdy stos jest
niepusty, ``self._head`` jest odniesieniem do pierwszego węzła połączonej
listy, a górny element to ``self._head._element``. Jeśli chodzi o ``pop``,
lokalne odniesienie do elementu przechowywanego w usuwanym węźle jest
zachowywane i ten element jest zwracany. Zauważ, że wszystkie metody, w
najgorszym przypadku, używają stałego czasu.

Podrozdział 1.2: Implementacja kolejki z pojedynczo dowiązaną listą
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

W tej sekcji przedstawiamy implementację w Pythonie abstrakcyjnego typu
danych kolejki za pomocą listy dowiązanej pojedynczo. Musimy wykonać
operacje na obu końcach kolejki, co oznacza, że utrzymujemy referencje
nagłówka i ogona jako zmienne instancji dla każdej kolejki. Następnie
wyrównujemy przód kolejki z nagłówkiem listy, a tył kolejki z końcem
listy; dzięki temu jesteśmy w stanie kolejkować elementy z tyłu, a
kolejkować je od przodu. Implementacja klasy LinkedQueue jest podana w
poniższym kodzie::

 1  class LinkedQueue:
 2    # Queue implementation with a singly linked list
 3
 4    class Node:
 5      __slots__ = '_element' , '_next'
 6      def __init__(self, element, next):
 7        self._element = element
 8        self._next = next
 9
 10   def __init__ (self):
 11     self._head = None
 12     self._tail = None
 13     self._size = 0
 14
 15   def __len__ (self):
 16     return self._size
 17
 18   def is_empty(self):
 19     return self._size == 0
 20
 21   def first(self):
 22     if self.is_empty( ):
 23       raise Empty('Queue is empty')
 24     return self._head._element
 25
 26   def dequeue(self):
 27     if self.is_empty( ):
 28       raise Empty('Queue is empty')
 29     answer = self._head._element
 30     self._head = self._head._next
 31     self._size −= 1
 33     if self.is_empty( ):       # special case if queue is empty
 33       self._tail = None        # removed head had been the tail
 34     return answer
 35
 36   def enqueue(self, e):
 37     newnode = self._Node(e, None)
 38     if self.is_empty( ):
 39       self._head = newnode
 40     else:
 41       self._tail._next = newnode
 42     self._tail = newnode
 43     self._size += 1

Wiele aspektów poprzedniej implementacji jest podobnych do tych z klasy
``LinkedStack``. Ma przykład definicja klasy Node. Implementacja ``dequeue`` dla
``LinkedQueue`` jest podobna do tej z ``pop`` dla ``LinkedStack``: oba usuwają
początek połączonej listy, z tą różnicą, że kolejka utrzymuje
odniesienie do ogona. Ogólnie rzecz biorąc, operacja na początku nie ma
wpływu na ogon, ale gdy ``dequeue`` jest wywoływane w kolejce z jednym
elementem, usuwamy jednocześnie początek i koniec listy. Dlatego
ustawiamy ``self._tail`` na ``None``. Coś podobnego dzieje się przy
implementacji kolejki. Nowy węzeł jest nowym ogonem, ale gdy nowy węzeł
jest jedynym węzłem na liście, staje się również nowym nagłówkiem; w
przeciwnym razie nowy węzeł musi być połączony bezpośrednio za już
istniejącym węzłem końcowym. Wszystkie operacje przebiegają w najgorszym
przypadku stałym czasem, a wykorzystanie przestrzeni jest liniowe w
stosunku do aktualnej liczby elementów.

Podrozdział 2: Circulary LinkedList
-----------------------------------

Lista dowiązana, w której koniec listy wykorzystuje swoje następne
odniesienie do wskazywania z powrotem na początek listy, nazywana jest
listą dowiązana cyklicznie. Daje to ogólny model dla zbiorów danych,
które są cykliczne, to znaczy, które nie mają żadnego konkretnego
pojęcia początku i końca, ale zamiast tego zachowują odniesienie do
konkretnego węzła w celu użycia listy. Taki węzeł nazywa się bieżącym
węzłem i przechodzimy przez węzły listy za pomocą current.next.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura57.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Przykładem użycia listy dołączonej cyklicznie jest round-robin
scheduler, który iteruje kolekcję elementów w sposób cykliczny i
obsługuje każdy element, wykonując na nim daną akcję. Na przykład ten
harmonogram służy do przydzielania zasobów współużytkowanych przez wielu
klientów (takich jak wycinki czasu procesora przypisane do aplikacji
uruchomionych na komputerze). Harmonogram round-robin wielokrotnie
wykonuje następujące kroki w kolejce Q::

 1 e = Q.dequeue()
 2 Service element e
 3 Q.enqueue(e)

Te kroki można łatwo wykonać za pomocą wprowadzonej wcześniej klasy
``LinkedQueue``. Miemniej jednak zauważ, że musimy usunąć węzeł z listy
(dokonując korekty nagłówka listy i jego rozmiaru), aby utworzyć nowy
węzeł i wstawić go na końcu listy, zwiększając rozmiar. Można tego
uniknąć za pomocą listy połączonej kołowo, w której przeniesienie
elementu z nagłówka do końca listy można osiągnąć po prostu przesuwając
odwołanie, które wyznacza granicę kolejki.

W następnej sekcji przedstawiamy implementację klasy ``CircularQueue``
obsługującej kolejkę ADT, z nową metodą ``rotate()``, która przenosi
pierwszy element kolejki na tył. Harmonogram round-robin można wdrożyć,
wielokrotnie wykonując następujące kroki::

 1. Service element Q.front()
 2. Q.rotate()

Podrozdział 2.1: Implementacja kolejki z listą połączoną cyklicznie
-------------------------------------------------------------------

Aby zaimplementować kolejkę ADT z listą dowiązaną cyklicznie, opieramy
się na poprzednim pomyśle, że kolejka ma nagłówek i ogon, z następnym
odwołaniem do ogona połączonym z nagłówkiem. W tym przypadku jedyne dwie
zmienne instancji to ``_tail``, który jest odniesieniem do węzła końcowego
(lub None, gdy jest pusty) i ``_size``, bieżąca liczba elementów w kolejce.
Będziemy w stanie znaleźć początek kolejki, po prostu podążając za
następną referencją ogona, czyli ``self._tail._next``. Możemy zakolejkować
nowy węzeł, umieszczając go za ogonem i przed aktualną głową, czyniąc go
tym samym nowym ogonem. Aby zaimplementować operację usunięcia przodu
kolejki i wstawienia go z tyłu, dodajemy metodę ``rotate``, która w zasadzie
ustawia ``self._tail = self._tail._next`` (stara głowa staje się nowym
ogonem, a węzeł po tym, jak stara głowa stanie się nową głową). Pełna
implementacja jest podana w poniższym kodzie::

 1  class CircularQueue:
 2    # Queue implementation using circularly linked list
 3
 4    class _Node:
 6      __slots__ = '_element' , '_next'
 7      def __init__(self, element, next):
 8        self._element = element
 9        self._next = next
 7
 8    def __init__(self):
 9      self._tail = None
 10     self._size = 0
 11
 12   def __len__ (self):
 13     return self._size
 14
 15   def is_empty(self):
 16     return self._size == 0
 17
 18   def first(self):
 19     if self.is_empty( ):
 20       raise Empty('Queue is empty')
 21     head = self._tail._next
 22     return head._element
 23
 24   def dequeue(self):
 25     if self.is_empty( ):
 26       raise Empty('Queue is empty')
 27     oldhead = self._tail._next
 28     if self._size == 1:
 29       self._tail = None
 30     else:
 31       self._tail._next = oldhead._next
 32     self._size −= 1
 33     return oldhead._element
 34
 35   def enqueue(self, e):
 36     newnode = self._Node(e, None)
 37     if self.is_empty( ):
 38       newnode._next = newnode
 39     else:
 40       newnode._next = self._tail._next
 41       self._tail._next = newnode
 42     self._tail = newnode
 43     self._size += 1
 44
 45   def rotate(self):
 46     if self._size > 0:
 47       self._tail = self._tail._next


Podrozdział 3: Listy podwójnie dowiązane
----------------------------------------

Istnieje kilka ograniczeń w projektowaniu listy pojedynczo dowiązanej.
Chociaż możemy skutecznie wstawić węzeł na każdym końcu takiej listy i
możemy usunąć węzeł na początku, nie możemy skutecznie usunąć węzła na
końcu lub węzła z każdej pozycji wewnętrznej. Dzieje się tak, ponieważ
musimy zaktualizować również następne odwołanie do węzła poprzedzającego
węzeł, który ma zostać usunięty.

Lista podwójnie dowiązana to lista, w której każdy węzeł utrzymuje
odniesienie do węzła znajdującego się po nim (czyli ``next``) oraz do węzła
poprzedzającego (czyli do ``prev``). Zobaczymy, że pozwala to na proste
wstawianie i usuwanie w dowolnych pozycjach na liście.

**Header and Trailer Sentinels**

Standardowa implementacja podwójnie dołączonej listy obejmuje dwa
specjalne węzły na obu końcach listy, odpowiednio *header* i *trailer*.
Pełnią rolę strażników i nie przechowują żadnego elementu.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura58.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Na pustej liście pole ``next`` wskazuje ogon, a pole
``prev`` wskazuje na nagłówek. W przypadku listy niepustej
nagłówek znajduje się obok węzła zawierającego pierwszy element
sekwencji, a ``prev`` zwiastuna odnosi się do węzła zawierającego ostatni
element sekwencji. Dodanie dwóch dodatkowych węzłów upraszcza wymagane
operacje. W szczególności wszystkie wstawienia są wykonywane w ten sam
sposób, ponieważ nowy węzeł będzie zawsze umieszczany między parą
istniejących węzłów. Podobnie każdy element do usunięcia jest zawsze
przechowywany pomiędzy dwoma sąsiadami, po każdej stronie. Gdy lista
jest pusta, nie jest wymagany żaden szczególny przypadek.

**Wstawianie i usuwanie za pomocą listy podwójnie dowiązanej**

Każde wstawienie do podwójnie połączonej listy będzie miało miejsce
między parą istniejących węzłów. Na przykład, gdy nowy element jest
wstawiany na początku sekwencji, nowy węzeł przechodzi między nagłówkiem
a węzłem, który znajduje się obecnie za nagłówkiem. 

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura59.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center
   
Na poniższym rysunku pokazujemy, co się dzieje, gdy dodamy element do podwójnie powiązanej
listy z nagłówkiem i znacznikami przyczepy, przed operacją, po
utworzeniu nowego węzła i po podłączeniu sąsiadów do nowego węzła.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura510.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Jeśli chodzi o usunięcie węzła, dwoje sąsiadów węzła, który ma zostać
usunięty, jest ze sobą bezpośrednio połączonych, z pominięciem
oryginalnego węzła. W ten sposób ten węzeł nie jest już członkiem listy.
Ta sama implementacja jest używana podczas usuwania pierwszego (lub
ostatniego) elementu sekwencji, ponieważ nawet taki element będzie
przechowywany w węźle leżącym między dwoma innymi.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura511.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Podrozdział 3.1: Podstawowa implementacja listy podwójnie dowiązanej
--------------------------------------------------------------------

Poniżej przedstawiamy implementację podwójnie dowiązanej listy, a
mianowicie klasy ``DoubleLinkedBase``. Jest to klasa niskiego poziomu, co
oznacza, że nie zapewniamy dla niej interfejsu. Zobaczymy, że wstawienia
i usunięcia można wykonać w czasie \\(O (1)\\), w najgorszym przypadku, gdy
jesteśmy w stanie zidentyfikować położenie konkretnego węzła na liście.

Klasa ``Node``, której tutaj używamy, jest podobna do tej dla listy
dowiązanej pojedynczo, ale zawiera atrybut ``prev``, oprócz atrybutów ``next`` i
``element``. Konstruktor listy tworzy instancję dwóch węzłów wartowniczych i
łączy je bezpośrednio ze sobą. Utrzymywany jest rozmiar ``size``::

 1  class _DoublyLinkedBase:
 2
 3    class Node:
 4      __slots__ = '_element' , '_prev' , '_next'
 5      def __init__(self, element, prev, next):
 6        self._element = element
 7        self._prev = prev                # previous node reference
 8        self._next = next                # next node reference
 9
 10   def __init__(self):
 11     self._header = self._Node(None, None, None)
 12     self._trailer = self._Node(None, None, None)
 13     self._header._next = self._trailer        # trailer is after header
 14     self._trailer._prev = self._header        # header is before trailer
 15     self._size = 0
 16
 17   def __len__(self):
 18     return self._size
 19
 20   def is_empty(self):
 21     return self._size == 0
 22
 23   def _insert_between(self, e, predecessor, successor):
 25     newnode = self._Node(e, predecessor, successor)
 26     predecessor._next = newnode
 27     successor._prev = newnode
 28     self._size += 1
 29     return newnode
 30
 32   def _delete_node(self, node):
 33     predecessor = node._prev
 34     successor = node._next
 35     predecessor._next = successor
 36     successor._prev = predecessor
 37     self._size −= 1
 38     element = node._element                             # record deleted element
 39     node._prev = node._next = node._element = None      # useful for the garbage collection
 40     return element                                      # return deleted element

Metody ``_insert_between`` i ``_delete_node`` są zaprojektowane jako narzędzia
niepubliczne. Obsługują odpowiednio wstawianie i usuwanie, a jako
parametry wymagają co najmniej jednego odniesienia do węzła. Pierwsza
tworzy nowy węzeł, z polami zainicjowanymi do połączenia z określonymi
sąsiednimi węzłami. Następnie pola sąsiednich węzłów są aktualizowane
tak, aby zawierały najnowszy węzeł na liście. Metoda zwraca odwołanie do
nowego węzła. Ten ostatni łączy ze sobą sąsiadów węzła, który ma zostać
usunięty, co oznacza, że jest on pomijany na liście. Pola ``prev``, ``next`` i
``element`` są ustawione na ``None``, ponieważ może to pomóc garbage
collectorowi Pythona,

Podrozdział 3.2: Implementacja deque z podwójnie dowiązaną listą
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dwustronną kolejkę (deque) ADT można zaimplementować za pomocą tablicy,
a wszystkie operacje działają w zamortyzowanym czasie  \\(O (1)\\) ze
względu na konieczność zmiany rozmiaru tablicy. Tę samą strukturę danych
można zaimplementować za pomocą listy podwójnie dowiązanej. W tym
przypadku wszystkie operacje deque działają w najgorszym przypadku \\(O(1)\\). Następująca implementacja klasy LinkedDeque dziedziczy z
poprzedniej klasy ``_DoublyLinkedBase``. Opieramy się na metodach
dziedziczonych ``_init_`` (aby zainicjować nową instancję), ``_len_``,
``is_empty``, a także ``_insert_between`` (aby wstawić element na początku lub
na końcu) oraz ``_delete_node`` (aby usunąć element). Zgodnie z
oczekiwaniami, wstawiając element na początku kolejki, umieszczamy go
bezpośrednio między nagłówkiem a węzłem tuż za nagłówkiem. Wstawienie na
końcu deque jest umieszczane bezpośrednio przed węzłem. Operacje te
kończą się pomyślnie nawet wtedy, gdy kolejka jest pusta. W tym
przypadku nowy węzeł jest umieszczany między dwoma strażnikami::

 1  class LinkedDeque(_DoublyLinkedBase):
 2
 3    def first(self):
 4      if self.is_empty( ):
 5        raise Empty("Deque is empty")
 6      return self._header._next._element
 7
 8    def last(self):
 9      if self.is_empty( ):
 10       raise Empty("Deque is empty")
 11    return self._trailer._prev._element
 12
 13   def insert_first(self, e):
 14       self._insert_between(e, self._header, self._header._next)
 15
 16   def insert_last(self, e):
 17     self._insert_between(e, self._trailer._prev, self._trailer)
 18
 19   def delete_first(self):
 20     if self.is_empty( ):
 21      raise Empty("Deque is empty")
 22     return self._delete_node(self._header._next)
 23
 24   def delete_last(self):
 25     if self.is_empty( ):
 26       raise Empty("Deque is empty")
 27     return self._delete_node(self._trailer._prev)


Rozdział 4: Listy pozycyjne
---------------------------

Stosy, kolejki i kolejki podwójnie dowiązane umożliwiają operacje na
początku lub na końcu sekwencji (lub obu w ostatnim przypadku). W tej
sekcji przedstawiamy abstrakcyjny typ danych, który umożliwia
odwoływanie się do elementów w dowolnym miejscu sekwencji oraz
wykonywanie dowolnych wstawek i usunięć. Nazywa się to listą pozycyjną
ADT.

Prostym sposobem na zaimplementowanie takiej struktury jest użycie
sekwencji opartej na tablicy, w której indeksy całkowite wskazują
lokalizację elementu lub lokalizację, w której powinno nastąpić
wstawienie lub usunięcie. Nie można jednak sprawnie znaleźć elementu
listy połączonej, znając tylko jego indeks, ponieważ operacja ta wymaga
przemierzania listy, zaczynając od początku lub końca, i zliczając
elementy. Co więcej, indeks wpisu może ulec zmianie z powodu wstawiania
lub usuwania, które mają miejsce w sekwencji.

Potrzebujemy abstrakcji, w której istnieje inny sposób opisania pozycji
oraz obsługi sytuacji, takich jak usuwanie lub wstawianie elementu z /
do listy w czasie  \\(O (1)\\). Taka abstrakcja może być oparta na liście
dowiązanej, ponieważ odniesienie do węzła może być użyte do opisania
pozycji każdego elementu w sekwencji. Losowe wstawianie i usuwanie jest
już zaimplementowane za pomocą metod, takich jak ``_insert_between`` i
``_delete_node``, które akceptują odwołania do węzłów jako parametry. Nawet
jeśli jest to atrakcyjne rozwiązanie, istnieją powody, aby z niego nie
korzystać. Po pierwsze, takie bezpośrednie użycie referencji węzła
narusza zasady abstrakcji i enkapsulacji, ponieważ pozwala użytkownikom
bezpośrednio manipulować węzłami, potencjalnie podważając spójność
listy. Następnie lepiej jest zaprojektować bardziej elastyczną,
użyteczną i solidną strukturę danych, w której szczegóły niskiego
poziomu nie są dostępne dla użytkownika i którą można łatwo
przeprojektować, aby poprawić jej wydajność. W ten sposób możemy również
podać pojęcie pozycji nienumerycznej, nawet jeśli używamy sekwencji
opartej na tablicy. Z tych powodów, zamiast polegać bezpośrednio na
węzłach, wprowadzamy niezależną abstrakcję pozycji, aby określić
położenie elementu na liście, a następnie pełną listę pozycyjną ADT,
która może zawierać podwójnie połączoną listę.

Podrozdział 4.1: Abstrakcyjny typ danych listy pozycyjnej
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

W celu zdefiniowania abstrakcji ciągu elementów wraz ze sposobem
identyfikacji położenia elementu wprowadzamy ADT listę pozycyjną oraz
typ danych abstrakcja pozycji. Pozycja jest znacznikiem na liście i nie
mogą mieć na nią wpływu zmiany na liście. Pozycja staje się nieważna
tylko wtedy, gdy wydane zostanie wyraźne polecenie jej usunięcia. Metoda
``p.element()`` zwraca element przechowywany w pozycji p.

Metody obsługiwane przez listę L to:

-  **L.first()**: zwraca pozycję pierwszego elementu L lub None, jeśli L
   jest puste;
-  **L.last ()**: zwraca pozycję ostatniego elementu L lub None, jeśli L
   jest puste;
-  **L.before (p)**: zwróć pozycję L bezpośrednio przed pozycją p lub Brak,
   jeśli p jest pierwszą pozycją;
-  **L.after (p)**: zwraca pozycję L bezpośrednio po pozycji p, lub Brak,
   jeśli p jest ostatnią pozycją;
-  **L.is_empty ()**: zwraca True, jeśli lista L nie zawiera żadnych
   elementów;
-  **len (L)**: zwraca liczbę elementów na liście;
-  **iter (L)**: zwraca iterator do przodu dla elementów listy.

Lista pozycyjna ADT zawiera również następujące metody aktualizacji:

-  **L.add_first(e)**: wstawia nowy element e na początku L, zwracając jego
   pozycję;
-  **L.add_last(e)**: wstawia nowy element e z tyłu L, zwracając jego
   pozycję;
-  **L.add_before(p, e)**: wstawia nowy element e przed pozycją p w L,
   zwracając jego pozycję;
-  **L.add_after(p, e)**: wstawia nowy element e po pozycji p w L,
   zwracając jego pozycję;
-  **L.replace(p, e)**: zamień element na pozycji p na element e, zwracając
   element, który był na pozycji p.
-  **L.delete(p)**: usuń i zwróć element w pozycji p w L, unieważniając
   pozycję.

Błąd występuje, jeśli p nie jest prawidłową pozycją listy. Zauważ, że
metody ``first ()`` i ``last ()`` zwracają powiązane pozycje. Pierwszy element
listy pozycyjnej można określić, wywołując ``L.first().element()``. Służy do
nawigacji po liście. Na przykład poniższy kod wypisuje wszystkie
elementy listy pozycyjnej ``poslis``::

 marker = poslis.first( )
 while marker is not None:
   print(marker.element( ))
   marker = poslis.after(marker)

Żadna wartość nie jest zwracana, gdy ``after`` jest wywołane na ostatniej
pozycji. Podobnie None jest zwracane, gdy metoda ``before`` jest wywoływana
na początku listy lub gdy ``first`` lub ``last`` są wywoływane na pustej
liście. W poniższej tabeli przedstawiono serię operacji na liście
pozycyjnej L. Zmienne, takie jak p, q i r, służą do identyfikowania
wystąpień pozycji.

==================== ===================== ==============
Operacje             Zwrócone wartości       L
==================== ===================== ==============
L.add_last(3)        p                     3p
L.first( )           p                     3p
L.add_after(p,5)     q                     3p, 5q
L.before(q)          p                     3p, 5q
L.add_before(q,3)    r                     3p, 3r, 5q
r.element( )         3                     3p, 3r, 5q
L.after(p)           r                     3p, 3r, 5q
L.add first(7)       s                     7s, 3p, 3r, 5q
L.delete(L.last( ))  5                     7s, 3p, 3r
L.replace(p,10)      3                     7s, 10p, 3r
==================== ===================== ==============

Podrozdział 4.2: Implementacja list podwójnie dowiązanych
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

W tej sekcji przedstawiamy implementację klasy ``PositionalList`` przy
użyciu podwójnie dowiązanej listy. Lażda metoda klasy działa w
najgorszym przypadku  (O (1) ). W tym celu korzystamy z wprowadzonego
wcześniej ``DoublyLinkedBase`` i budujemy publiczny interfejs zgodnie z
wymaganiami listy pozycyjnej ADT.

Publiczna klasa ``Position`` jest zagnieżdżona w klasie PositionalList, a
jej wystąpienia reprezentują lokalizacje elementów na liście. Metody
``PositionalList`` mogą tworzyć instancje ``Position``, które są redundantne, to
znaczy, które odwołują się do tego samego węzła (na przykład, gdy
pierwszy i ostatni węzeł są takie same). Z tego powodu metody ``_eq_`` i
``_ne_`` są zdefiniowane w celu sprawdzenia, czy dwie pozycje odnoszą się
do tego samego węzła.

Niepubliczna metoda ``_validate`` sprawdza, czy pozycja jest prawidłowa i
określa węzeł bazowy. Zauważ, że pozycja zawiera odniesienie do węzła
połączonej listy, a także odniesienie do wystąpienia listy, które
zawiera ten węzeł. Oznacza to, że jesteśmy w stanie wykryć, kiedy
instancja pozycji nie należy do wskazanej listy i kiedy instancja
pozycji jest poprawna, ale odnosi się do węzła, który nie jest już
częścią tej listy (można to zrobić łatwo, ponieważ ``_delete_node`` klasy
bazowej ustawia poprzednie i następne odwołania usuniętego węzła na
Brak). Aby uzyskać dostęp do listy pozycyjnej, definiujemy metody, które
używają ``_validate`` w celu “odpakowania” dowolnej pozycji oraz
``_make_position`` do “zawinięcia” węzłów jako instancji Position do
powrotu do użytkownika::

 1  class PositionalList(_DoublyLinkedBase):
 2    # A sequential container of elements with positional access
 3
 4    class Position:
 5      # The location of a single element
 6      def __init__ (self, container, node):
 7        self._container = container
 8        self._node = node
 9
 10     def element(self):
 11       return self._node._element
 12
 13     def __eq__ (self, other):
 14       # Returns True if other is a position representing the same location
 15       return type(other) is type(self) and other._node is self._node
 16
 17     def __ne__ (self, other):
 18       # Returns True if other does not represent the same location
 19       return not (self == other)
 20
 21   #------------------------------- utility methods -------------------------------
 22
 23   def _validate(self, p):
 24     # Returns node in position p, or raise error if invalid
 25     if not isinstance(p, self.Position):
 26       raise TypeError('p must be proper Position type')
 27     if p._container is not self:
 28       raise ValueError('p does not belong to this container')
 29     if p._node._next is None:                  # invalid node
 30       raise ValueError('p is no longer valid')
 31     return p._node
 32
 33   def _make_position(self, node):
 34     # Returns Position instance for given node (None if sentinel)
 35     if node is self._header or node is self._trailer:
 36       return None
 37     else:
 38       return self.Position(self, node)
 39
 40   #------------------------------- accessors -------------------------------
 41
 42   def first(self):
 43     # Returns the first Position in the list
 44     return self._make_position(self._header._next)
 45
 46   def last(self):
 47     # Returns the last Position in the list
 48     return self._make_position(self._trailer._prev)
 49
 50   def before(self, p):
 51     # Returns the Position before Position p
 52     node = self._validate(p)
 53     return self._make_position(node._prev)
 54
 55   def after(self, p):
 56     # Returns the Position after Position p
 57     node = self._validate(p)
 58     return self._make_position(node._next)
 59
 60   def __iter__(self):
 61     # Generate a forward iteration of the elements of the list
 62     cursor = self.first( )
 63     while cursor is not None:
 64       yield cursor.element( )
 65       cursor = self.after(cursor)

W poniższym kodzie przedstawiamy metody aktualizacji::

 66   # inherited version overridden, returns Position, rather than Node
 67   def _insert_between(self, e, predecessor, successor):
 68     # Adds element between existing nodes and return new Position
 69     node = super()._insert_between(e, predecessor, successor)
 70     return self._make_position(node)
 71
 72   def add_first(self, e):
 73     # Inserts element e at the front of the list and returns Position
 74     return self._insert_between(e, self._header, self._header._next)
 75
 76   def add_last(self, e):
 77     # Inserts element e at the back of the list and returns Position
 78     return self._insert_between(e, self._trailer._prev, self._trailer)
 79
 80   def add_before(self, p, e):
 81     # Inserts element e into list before Position p and returns Position
 82     original = self._validate(p)
 83     return self._insert_between(e, original._prev, original)
 84
 85   def add_after(self, p, e):
 86     # Inserts element e into list after Position p and returns Position
 87     original = self._validate(p)
 88     return self._insert_between(e, original, original._next)
 89
 90   def delete(self, p):
 91     # Removes and returns the element at Position p
 92     original = self._validate(p)
 93     return self._delete_node(original)
 94
 95   def replace(self, p, e):
 96     # Replace the element at Position p with e
 97     original = self._validate(p)
 98     old_value = original._element           # temporarily store old element
 99     original._element = e                   # replace with new element
 100    return old_value                        # return the old element value


Podrozdział 5: Sortowanie listy pozycyjnej
------------------------------------------

W tej sekcji przedstawiamy implementację algorytmu sortowania przez
wstawianie operującego na liście pozycyjnej. Utrzymujemy znacznik
zmiennej, który reprezentuje skrajną prawą pozycję aktualnie
posortowanej części listy. Oś jest pozycją bezpośrednio za znacznikiem,
a do przesunięcia w lewo od znacznika używamy zmiennej ``move_left``, o ile
istnieje poprzedzający element o wartości większej niż oś obrotu. Na
poniższym rysunku przedstawiamy sytuację listy podczas procesu.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura512.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Poniżej pokazujemy kod dla insert sort::

 1  def insertion_sort(L):
 2
 3    if len(L) > 1:
 4      marker = L.first( )
 5      while marker != L.last( ):
 6        pivot = L.after(marker)
 7        value = pivot.element( )
 8        if value > marker.element( ):
 9          marker = pivot                        # pivot becomes new marker
 10       else:
 11         move_left = marker                    # find leftmost item greater than value
 12         while move_left != L.first( ) and L.before(move_left).element( ) > value:
 13           move_left = L.before(move_left)
 14         L.delete(pivot)
 15         L.add_before(move_left, value)        # insert value before move_left

Podrozdział 6: Sekwencje Array oraz Link-Based
----------------------------------------------

Struktury danych oparte na tablicach i łączach mają zalety i wady. W tej
sekcji omówimy niektóre z nich.

Podrozdział 6.1: Zalety sekwencji Array-Based
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

W sekwencji opartej na tablicach jesteśmy w stanie uzyskać dostęp do
ogólnego elementu w ciągu  \\(O (1) \\) - czas. W przeciwieństwie do tego,
zlokalizowanie  \\(k \\) - tego elementu w połączonej liście wymaga  \\(O (k)\\) - czasu (tj. liniowej złożoności czasowej), aby przejść przez listę.

Reprezentacje oparte na tablicach zużywają mniej pamięci niż struktury
połączone. Nawet jeśli długość tablicy dynamicznej może być dłuższa niż
liczba elementów, które przechowuje, zauważamy, że zarówno listy oparte
na tablicy, jak i listy połączone są strukturami referencyjnymi, więc
pamięć wymagana do przechowywania rzeczywistych elementów jest taka sama
dla obu struktur. Co więcej, w przypadku kontenera opartego na tablicy z
elementami  \\(n \\), typowy najgorszy przypadek ma miejsce, gdy tablica
dynamiczna o zmienionym rozmiarze alokuje pamięć na  \\(2n \\) odwołania do
obiektów. W przypadku list dowiązanych pamięć musi być poświęcona nie
tylko na przechowywanie referencji do każdego zawartego obiektu, ale
także na referencje, które łączą węzły. Tak więc pojedynczo dowiązana
lista długości  \\(n \\) zawsze wymaga referencji  \\(2n \\) (odniesienie do
elementu i następne odniesienie dla każdego węzła). Z podwójnie
dowiązaną listą istnieją  \\(3n\\) referencje.

Operacje z równoważnymi asymptotycznymi ograniczeniami zwykle są
bardziej wydajne w przypadku stałego czynnika z sekwencją opartą na
tablicy, w.r.t. te same operacje wykonywane przez połączoną strukturę.
Dzieje się tak, ponieważ trzeba utworzyć wystąpienie nowego węzła, gdy
wykonywane są połączone wersje niektórych operacji, takich jak typowa
operacja inqueue dla kolejki.

Podrozdział 6.2: Zalety sekwencji Link-Based
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Jeżeli niektóre operacje na danej strukturze danych są wykonywane w
systemie czasu rzeczywistego, ma to na celu zapewnienie natychmiastowych
odpowiedzi, ponieważ duże opóźnienie spowodowane pojedynczą
(zamortyzowaną) operacją może mieć negatywny wpływ. W tym przypadku
obserwujemy, że zamortyzowane granice są związane z rozszerzaniem lub
zmniejszaniem dynamicznej tablicy, podczas gdy struktury oparte na
łączach zapewniają bezpośrednio najgorsze ograniczenia czasowe dla ich
operacji.

Obsługa struktur Link-Based  \\(O (1) \\) - czas wstawiania i usuwania w
dowolnych pozycjach. Jest to najważniejsza zaleta listy dowiązanej, w
przeciwieństwie do sekwencji opartej na tablicy. Wstawianie lub usuwanie
elementu z końca listy opartej na tablicy może odbywać się w stałym
czasie, ale bardziej ogólne wstawienia i usunięcia są drogie. Wywołanie
insertu lub pop z index  \\(k \\), z klasą list opartą na tablicach Pythona,
używa  \\(O (n − k + 1) \\) - time, ponieważ musimy przesunąć wszystkie
kolejne elementy.

===================================
Algorithms
===================================

.. toctree::
   :maxdepth: 1

   Rozdzial_1.rst
   Rozdzial_1_zadania.rst
   Rozdzial_2.rst
   Rozdzial_2_zadania.rst
   Rozdzial_3.rst
   Rozdzial_3_zadania.rst
   Rozdzial_4.rst
   Rozdzial_4_zadania.rst
   Rozdzial_5.rst
   Rozdzial_5_zadania.rst
   Rozdzial_6.rst
   Rozdzial_6_zadania.rst

Rozdział 6: Sortowanie
**********************

Podrozdział 1: Wprowadzenie
---------------------------

Biorąc pod uwagę zbiór elementów i zakładając, że istnieje (w języku)
porządek nad nimi, sortowanie oznacza przeorganizowanie ich w taki
sposób, aby były uporządkowane od najmniejszego do największego. W
Pythonie domyślna kolejność obiektów jest definiowana za pomocą
operatora  \\(<\\), którego właściwość jest niezwrotna \\( (k \\) jest nie
mniejsza niż ona sama) i przechodnia (jeśli  \\(k_1 <k_2 \\) i  \\(k_2 <k_3 \\),
następnie  \\(k_1 <k_3 )\\). Ponadto Python ma wbudowane funkcje do
sortowania danych, w postaci metody ``sort`` klasy ``list`` (która zmienia
zawartość listy) oraz wbudowaną funkcję sortowania, która tworzy nową
listę zawierającą elementy dowolny zbiór w posortowanej kolejności. Te
wbudowane funkcje wykorzystują zaawansowane algorytmy i są wysoce
zoptymalizowane.

Programista zazwyczaj korzysta z wbudowanych funkcji sortowania,
ponieważ rzadkością jest posiadanie specjalnego żądania, które zmusza go
do zaimplementowania nowego, innego algorytmu sortowania. Jednak ważne
jest, aby zrozumieć, w jaki sposób budowane są algorytmy sortowania, aby
ocenić ich wydajność.

Sortowanie jest jednym z najczęściej badanych problemów w informatyce.
Zestawy danych są często przechowywane w kolejności posortowanej, na
przykład w celu umożliwienia wydajnego wyszukiwania za pomocą algorytmu
wyszukiwania binarnego. Wiele zaawansowanych algorytmów dla różnych
problemów opiera się na sortowaniu jako podprogramie.

Podrozdział 2: Algorytmy sortowania oparte na tablicach
-------------------------------------------------------

Podrodział 2.1: Bubble sort
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sortowanie bąbelkowe wykonuje bardzo proste zadanie: wykonuje wiele
przejść przez listę, porównuje pary sąsiednich wartości i wymienia je,
jeśli nie są we właściwej kolejności. Dla każdego przejścia największa
wartość jest przepuszczana do właściwej lokalizacji.

Kod Pythona sortowania bąbelkowego jest podany poniżej::

 1  def bubbleSort(alist):
 2    for passnum in range(len(alist)-1,0,-1):
 3      for i in range(passnum):
 4        if alist[i] > alist[i+1]:
 5          temp = alist[i]
 6          alist[i] = alist[i+1]
 7          alist[i+1] = temp


Zauważ, że jeśli lista lub tablica zawiera elementy  \\(n \\), będą  \\(n-1 \\)
porównania podczas pierwszego przebiegu,  \\(n-2 \\) podczas drugiego
przebiegu i tak dalej, aż do jednego porównania jest potrzebne w
końcowym  \\(n-1 \\) –tym przebiegu, z najmniejszą wartością we właściwej
pozycji. Całkowita liczba porównań jest sumą pierwszych  \\(n-1 \\) liczb
całkowitych, czyli  \\((n-1) n / 2 \\). Oznacza to, że algorytm ma złożoność
w  \\(O (n ^ 2) \\). W najlepszym przypadku lista jest już gotowa i nie
będzie wymieniona. Ale w najgorszym przypadku każde porównanie spowoduje
wymianę, przez co sortowanie bąbelkowe jest bardzo nieefektywną metodą
sortowania. Zwróć uwagę, że jeśli w trakcie przepustki nie ma wymiany,
lista jest już posortowana.

Poniższy rysunek przedstawia pierwszy przebieg sortowania bąbelkowego.
Zacieniowane elementy są porównywane i w razie potrzeby wymieniane.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura61.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Możemy napisać nieco zmodyfikowaną wersję algorytmu, krótkie sortowanie
bąbelkowe, które zatrzymuje się wcześnie, jeśli stwierdzi, że lista
została już posortowana::

 1  def shortbubbleSort(alist):
 2    exchanges = True
 3    passnum = len(alist)-1
 4    while passnum > 0 and exchanges:
 5      exchanges = False
 6      for i in range(passnum):
 7        if alist[i] > alist[i+1]:
 8          exchanges = True
 9          temp = alist[i]
 10         alist[i] = alist[i+1]
 11         alist[i+1] = temp
 12     passnum = passnum-1
 
 
Podrozdział 2.2: Select sort
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sortowanie przez wybór powoduje wiele przejść przez listę o długości  \\(n\\), szukając największej wartości i przenosząc ją do właściwej
lokalizacji. Oznacza to, że na każdy karnet przypada tylko jedna
wymiana. Po pierwszym przejściu największy przedmiot znajduje się we
właściwym miejscu. Po drugim przejściu kolejna co do wielkości jest już
na swoim miejscu. Proces trwa aż do przejścia  \\(n-1 \\), kiedy ostatni
element jest na swoim miejscu.

Kod Pythona SelectSort jest podany poniżej::

 1  def selectionSort(alist):
 2    for fillslot in range(len(alist)-1,0,-1):
 3      positionOfMax=0
 4      for location in range(1,fillslot+1):
 5        if alist[location] > alist[positionOfMax]:
 6           positionOfMax = location
 7      temp = alist[fillslot]
 9      alist[fillslot] = alist[positionOfMax]
 10     alist[positionOfMax] = temp

Nawet jeśli istnieje tylko jedna wymiana dla każdego przebiegu,
sortowanie przez selekcję wykonuje taką samą liczbę porównań, jak
sortowanie bąbelkowe, dlatego jego złożoność wynosi  \\(O (n ^ 2)) \\).

Poniższy rysunek przedstawia proces sortowania. Na każdym przejściu
największy pozostały element jest wybierany i umieszczany we właściwym
miejscu.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura62.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Podrozdział 2.3: Insert sort
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sortowanie przez wstawianie tworzy posortowaną podlistę (początkowo
tylko jeden element) na początkowych pozycjach listy i prawidłowo
wstawia do tej podlisty nowy element, tworząc nową posortowaną podlistę.
Zaczynając od listy z jedną pozycją (pierwszy element listy), każda
pozycja od drugiej do ostatniej jest porównywana z pozycjami z już
posortowanej podlisty. Większe pozycje są przesuwane w prawo, a bieżąca
pozycja jest wstawiana do podlisty po osiągnięciu mniejszej pozycji.

Kod Pythona dla sortowania przez wstawianie jest napisany poniżej::


 1  def insertionSort(alist):
 2    for index in range(1,len(alist)):
 3      currentvalue = alist[index]
 5      position = index
 6      while position > 0 and alist[position-1] > currentvalue:
 8        alist[position]=alist[position-1]
 9        position = position-1
 10     alist[position]=currentvalue


W celu posortowania pozycji  (n ) istnieją przejścia  \\(n-1 \\). Maksymalna
liczba porównań dla sortowania przez wstawianie jest znowu sumą
pierwszych  \\(n-1 \\) liczb całkowitych. To jest  \\(O (n ^ 2) \\). W
najlepszym przypadku przy każdym przejściu wystarczy wykonać tylko jedno
porównanie. Tak byłoby w przypadku już posortowanej listy.

Poniższy rysunek przedstawia proces sortowania wstawiania. Zacienione
pozycje reprezentują uporządkowane podlisty dla każdego przebiegu.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura63.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Podrozdział 2.4: Merge sort
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sortowanie przez scalanie jest algorytmem rekurencyjnym. Jeśli lista
jest pusta lub zawiera jeden element, jest sortowana (przypadek
podstawowy). Jeśli lista zawiera więcej niż jeden element, lista jest
dzielona, a sortowanie przez scalanie jest wywoływane rekursywnie na obu
połowach. Po posortowaniu dwóch połówek wykonywane jest scalanie, biorąc
dwie mniejsze posortowane listy i łącząc je w jedną, posortowaną, nową
listę.

Ten algorytm jest klasycznym przykładem strategii dziel i rządź. W kroku
dzielenia, gdy wielkość wejściowa jest mniejsza niż dany próg,
rozwiązanie można znaleźć bezpośrednio. W przeciwnym razie dane
wejściowe są podzielone na rozłączne podzbiory. W kroku podboju
podproblemy związane z podzbiorami są rozwiązywane rekursywnie. Na
etapie łączenia rozwiązania podproblemów są łączone w celu uzyskania
rozwiązania pierwotnego problemu.

Ogólny algorytm sortowania przez scalanie jest podany poniżej, bez
rozważania, czy sekwencja  \\(S \\) jest zaimplementowana za pomocą tablicy,
czy połączonej listy::

 Divide: jeśli  \\(S \\) ma zero lub jeden element, zwróć  \\(S \\). W przeciwnym razie podziel  \\(S \\) na pół i umieść elementy w dwóch sekwencjach,  \\(S_1 \\) i \\(S_2 \\). 
 Conquer: rekurencyjnie sortuj sekwencje \\(S_1 \\) i \\(S_2 \\). 
 Combine: wstaw elementy z powrotem do \\(S \\), łącząc posortowane sekwencje \\(S_1 \\) i \\(S_2 \\) w posortowaną sekwencję. 
 
Poniżej podano kod Pythona dla sekwencji opartej na tablicach::


 1  def mergeSort(alist):
 2    print("Splitting ",alist)
 3    if len(alist) > 1:
 4      mid = len(alist)//2
 5      lefthalf = alist[:mid]
 6      righthalf = alist[mid:]
 7
 8      mergeSort(lefthalf)
 9      mergeSort(righthalf)
 10
 11     i=0
 12     j=0
 13     k=0
 14     while i < len(lefthalf) and j$< len(righthalf):
 15       if lefthalf[i] <= righthalf[j]:
 16         alist[k] = lefthalf[i]
 17         i=i+1
 18       else:
 19         alist[k] = righthalf[j]
 20         j=j+1
 21       k=k+1
 22
 23     while i < len(lefthalf):
 24       alist[k]=lefthalf[i]
 25       i=i+1
 26       k=k+1
 27
 28     while j <$len(righthalf):
 29       alist[k]=righthalf[j]
 30       j=j+1
 31       k=k+1
 32   print("Merging ",alist)

Rekurencyjne wywołanie funkcji ``mergesort`` jest wykonywane na lewej i
prawej połowie listy, zakładając, że zostały one posortowane. Pozostała
część funkcji łączy dwie posortowane listy w większą posortowaną listę.
Odbywa się to poprzez umieszczanie pozycji na oryginalnej liście
pojedynczo, pobierając najmniejszą pozycję z posortowanych list.

Poniższe rysunki przedstawiają odpowiednio procesy dzielenia i łączenia.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura64.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura65.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Aby przeanalizować złożoność tego algorytmu, weź pod uwagę, że lista
jest rekurencyjnie podzielona na dwie połowy: można to powtórzyć \\(log n\\) razy, jeśli lista ma długość \\(n\\). Każda para podlist jest łączona
w jedną listę, a to wymaga operacji  (n), ponieważ każdy element
podlisty musi zostać przetworzony. Stąd całkowity koszt algorytmu wynosi
\\(O (n  log n) \\). Zauważ, że ``Mergesort`` wymaga miejsca do przechowywania
podlist wyodrębnionych za pomocą operacji wycinania. Ten dodatkowy wymóg
może sprawić, że ten algorytm będzie miał krytyczne znaczenie, gdy
zostanie zastosowany na dużych zestawach danych.

Podrozdział 2.5: Quick sort
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Szybkie sortowanie wykorzystuje podejście dziel i rządź, i, w
odniesieniu do sortowania przez scalanie, nie wykorzystuje dodatkowej
przestrzeni do przechowywania podlist wyodrębnionych za pomocą operacji
wycinania. W ramach kompromisu może się zdarzyć, że dwie podlisty nie
mają tej samej długości, co spowoduje spowolnienie działania algorytmu.
Quicksort rozpoczyna wybieranie wartości, a mianowicie osi, która jest
zwykle pierwszą pozycją na liście; zostanie znaleziona jego poprawna
pozycja na liście i ta pozycja (punkt podziału) jest wykorzystywana w
procesie dzielenia listy na dwie podlisty. Quicksort będzie stosowany
rekurencyjnie na tych podlistach.

Dokładniej, po wybraniu elementu obrotowego dwa znaczniki (lewy i prawy)
są definiowane odpowiednio jako pierwsza i ostatnia pozycja pozostałych
pozycji na liście. Lewy znacznik jest zwiększany, dopóki nie zostanie
znaleziona wartość większa niż oś obrotu; następnie prawy znak jest
zmniejszany, aż zostanie znaleziona wartość niższa niż oś obrotu. Te
dwie wartości są nie na miejscu w stosunku do punktu podziału i są
wymieniane.

Ta procedura jest kontynuowana, dopóki prawy znak nie stanie się niższy
niż lewy; teraz punkt podziału został znaleziony. Wartość obrotu jest
wymieniana z zawartością punktu podziału, co oznacza, że oś obrotu jest
we właściwej pozycji. Na poniższym rysunku znajduje się prawidłowy punkt
podziału dla 54, a dwie wartości są zamieniane.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura66.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Zwróć uwagę, że elementy na lewo od punktu podziału są niższe niż
wartość przestawna, a elementy na prawo od punktu podziału są większe
niż wartość przestawna. Lista jest dzielona w punkcie podziału, a
szybkie sortowanie jest wywoływane rekursywnie na dwóch podlistach.

Funkcja QuickSort jest zdefiniowana w następujący sposób::

 1  def quickSort(alist,first,last):
 2    if first < last:
 3      splitpoint = partition(alist,first,last)
 4      quickSort(alist,first,splitpoint-1)
 5      quickSort(alist,splitpoint+1,last)
 6
 7  def partition(alist,first,last):
 8    pivotvalue = alist[first]
 9    leftmark = first+1
 10   rightmark = last
 11
 12   done = False
 13   while not done:
 14     while leftmark <= rightmark and alist[leftmark] <= pivotvalue:
 15       leftmark = leftmark + 1
 16     while leftmark <= rightmark and alist[rightmark] >= pivotvalue:
 17       rightmark = rightmark -1
 18
 19     if rightmark < leftmark:
 29       done = True
 20     else:
 21       temp = alist[leftmark]
 22       alist[leftmark] = alist[rightmark]
 23       alist[rightmark] = temp
 24
 25   temp = alist[first]
 26   alist[first] = alist[rightmark]
 27   alist[rightmark] = temp
 28   return rightmark

Jeśli partycja występuje zawsze w środku listy, ponownie wystąpią
podziały  (log n ), dla listy o długości  \\(n \\). Aby znaleźć punkt
podziału, każdy z elementów  \\(n \\) musi zostać porównany z wartością
przestawną. Oznacza to, że ogólne kroki to  \\(O (n  log n) \\). Co więcej,
nie jest używana dodatkowa pamięć.

Punkty podziału nie zawsze znajdują się na środku listy. W najgorszym
przypadku funkcja partycjonowania dzieli listę  \\(n \\) pozycji na listę
\\(1 \\) pozycji i listę  \\(n-1 \\) pozycji. Następnie lista \\ (n-1 \\) pozycji
jest dzielona odpowiednio na listę  \\(1 \\) pozycji i listę  \\(n-2 \\)
pozycji. Prowadzi to do  \\(O (n ^ 2) \\) liczby kroków.

Podrozdział 3: Linear time sorting
----------------------------------

Udowodniono, że czas \\(O(n \log n)\\) jest niezbędny do
posortowania sekwencji elementów \\(n\\) za pomocą algorytmu sortowania
opartego na porównaniach. Naturalnym pytaniem jest, czy istnieją
algorytmy sortowania, które działają szybciej. Takie algorytmy istnieją,
ale wymagają specjalnych ograniczeń dotyczących sortowania sekwencji.

Podrozdział 3.1: Bucket sort
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Biorąc pod uwagę sekwencję  \\(S \\)  \\(n \\) elementów, których klucze są
liczbami całkowitymi z zakresu  \\([0, N -1] \\), dla  \\(N \geq 2\\), załóżmy,
że  \\(S \\) należy posortować według kluczy wpisów. W takim przypadku można
napisać algorytm sortowania  \\(S \\) w czasie  \\(O (n + N) \\). W związku z
tym możemy osiągnąć liniową złożoność czasową algorytmu sortowania;
zależy to od ograniczeń nałożonych na elementy.

Sortowanie kubełkowe nie jest oparte na porównaniach, ale na używaniu
kluczy jako indeksów w tablicy kubełkowej  \\(B \\), której komórki są
indeksowane od  \\(0 \\) do  \\(N-1 \\). Wpis z kluczem  \\(k \\) jest umieszczany w
wiadrze  \\(B [k] \\), który sam w sobie jest sekwencją wpisów z kluczem \\ (k\\\). Po umieszczeniu każdego wpisu sekwencji wejściowej  \\(S \\) w jego
wiadrze, są one umieszczane z powrotem w  \\(S \\) w posortowanej
kolejności, po prostu przez wyliczenie zawartości wiader \\(B [0], B [1], \ldots, B [N -1] \\). Pseudokod sortowania kubełkowego wygląda
następująco::

 1  BucketSort(S):
 2  Input: sequence S of entries with integer keys in the range [0,N-1]
 3  Output: sequence S sorted in increasing order of the keys
 4  B is an array of N sequences, initially empty
 5
 6  for each entry e in S do
 7    k = the key of e
 8    remove e from S and insert it at the end of bucket B[k]
 9  for i = 0 to N-1 do
 10   for each entry e in sequence B[i] do
 11     remove e from B[i] and insert it at the end of S
 
Sortowanie kubełkowe działa w czasie  \\(O (n + N) \\) i zajmuje taką samą
ilość miejsca. Oznacza to, że jest wydajny tylko wtedy, gdy zakres \\(N\\)
wartości dla kluczy jest mały w porównaniu z rozmiarem sekwencji \\(n\\),
powiedzmy \\(N=O(n)\\) lub \\(N = O (n  log n)\\). Ważną właściwością algorytmu
sortowania kubełkowego jest to, że działa on poprawnie, nawet jeśli
istnieją różne elementy z tym samym kluczem. W szczególności wykazuje
właściwość bycia stabilnym. Niech   \\(S = ((k_0,v_0), \\ldots , (k_{n-1},v_{n-1}))\\) będzie ciągiem wpisów; algorytm sortowania jest stabilny,
jeśli dla dowolnej pary  \\((k_i, v_i) \\) i  \\((k_j, v_j) \\)  \\(S \\) taki, że
\\(k_i = k_j \\) i  \\((k_i , v_i) \\) poprzedza  \\((k_j, v_j) \\) w  \\(S \\) przed
sortowaniem, mamy, że  \\((k_i, v_i) \\) poprzedza  \\((k_j, v_j)\\) po
sortowaniu. Stabilność jest ważna dla algorytmu sortowania, ponieważ
aplikacje mogą chcieć zachować początkową kolejność elementów z tym
samym kluczem.

Podany wcześniej nieformalny opis sortowania kubełkowego gwarantuje
stabilność, o ile zapewniamy, że wszystkie sekwencje działają jak
kolejki, z elementami przetworzonymi i usuniętymi z przodu sekwencji i
włożonymi z tyłu.

Podrozdział 3.2: Radix sort
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Rozważmy ogólny przypadek, w którym chcemy posortować wpisy z kluczami
będącymi parami  \\((k, l) \\), gdzie  \\(k \\) i  \\(l \\) są liczbami całkowitymi
z zakresu  \\([0, N- 1] \\), dla  \\(N  \geq 2 \\). Kolejność tych kluczy można
zdefiniować przy użyciu konwencji leksykograficznej, gdzie  \\((k_1, l_1)
<(k_2, l_2) \\) if  \\(k_1 <k_2 \\) lub if  \\(k_1 = k_2 ) i  ( l_1 <l_2 \\).

Algorytm sortowania radix sortuje sekwencję  \\(S \\) wpisów z kluczami,
które są parami, przez dwukrotne zastosowanie stabilnego sortowania
kubełkowego w sekwencji; po pierwsze, używając jednego składnika z pary
jako klucza przy zamawianiu, a następnie używając drugiego składnika.
Pojawia się jeden problem: która kolejność jest prawidłowa? To znaczy,
czy powinniśmy sortować według pierwszego składnika, a następnie
drugiego, czy odwrotnie?

Rozważmy na przykład sekwencję  (S = ((3,3), (1,5), (2,5), (1,2), (2,3),
(1,7), (3, 2), (2,2)) ). 
Jeśli posortujemy  (S ) według pierwszego
składnika, otrzymamy ciąg  (S_1 = ((1,5), (1,2), (1,7), (2,5), (2,3 ),
(2.2), (3.3), (3.2)) ). 
Jeśli następnie posortujemy  (S_1 ) za pomocą
drugiego składnika, otrzymamy ciąg  (S\_ {1,2} = ((1,2), (2,2), (3,2),
(2,3 ), (3,3), (1,5), (2,5), (1,7)) ), który wyraźnie nie jest
posortowaną sekwencją. 
Z drugiej strony, powtarzając proces odpowiednio
na drugim i pierwszym komponencie, otrzymujemy końcową sekwencję  (S\_
{2,1} = ((1,2), (1,5), (1,7), (2,2), (2,3), (2,5), (3,2), (3,3)) ),
który jest uporządkowany leksykograficznie.

Ten wynik można rozszerzyć na przypadek ogólny. Sortując kolejność
według drugiego składnika, a następnie ponownie według pierwszego
składnika, gwarantujemy, że jeśli dwa wpisy są równe w drugim sortowaniu
(według pierwszego składnika), to ich względna kolejność w kolejności
początkowej (która jest posortowana według drugiego składnika) jest
zachowana.

Ogólnie, biorąc pod uwagę sekwencję  \\(S \\) par klucz-wartość  \\(n \\), z
których każda ma klucz \\((k_1,k_2, \ldots ,k_d)\\), gdzie \\ (k_i \\) jest
liczbę całkowitą z zakresu  \\([0, N -1] \\) dla  \\(N  \geq 2 \\),  \\(S \\) można
posortować leksykograficznie w czasie  \\(O (d (n + N))\\) za pomocą
sortowania radix. Sortowanie Radix można zastosować do dowolnego klucza
złożonego. Na przykład możemy go użyć do sortowania ciągów znaków o
średniej długości, ponieważ każdy pojedynczy znak może być
reprezentowany jako wartość całkowita.

Podrozdział 4: Najlepszy algorytm sortowania
--------------------------------------------

Istnieje kilka algorytmów do sortowania sekwencji. Wśród nich sortowanie
przez wstawianie, bąbelki lub selekcję ma złożoność przeciętnego i
najgorszego przypadku  \\(O (n ^ 2) \\), co czyni je słabo odpowiednimi do
użycia w rzeczywistych scenariuszach. Inne metody, takie jak scalanie i
szybkie sortowanie, mają  \\(O (n  log n) \\) złożoność czasową (jest to
również optymalna złożoność przypadków ogólnego problemu sortowania). W
przypadku wyspecjalizowanych typów kluczy istnieją algorytmy działające
w czasie liniowym, takie jak BucketSort i RadixSort.

Ogólnie rzecz biorąc, przy ocenie, jak dobry jest algorytm, należy wziąć
pod uwagę wiele czynników. Złożoność czasowa jest najważniejsza, ale
zawsze istnieją kompromisy z wydajnością, wykorzystaniem pamięci i
stabilnością. Na przykład czas wykonania sortowania wstawiania może
wynosić  \\(O (n + m) \\), gdzie  \\(m \\) jest liczbą inwersji (tj. Liczba par
nie posortowanych) w sekwencji. Sekwencje z małą liczbą inwersji mogą
być efektywnie sortowane przez sortowanie przez wstawianie.

Jeśli chodzi o szybkie sortowanie, jego złożoność czasowa wynosi  \\(O (n
log n) \\), gdy lista jest podzielona na dwie podlisty o tej samej
długości. Niestety nie możemy zagwarantować, że dzieje się tak za każdym
razem, a złożoność przypadku brzeczki nadal wynosi  \\(O (n ^ 2)\\).
Ponadto sortowanie szybkie nie jest metodą stabilną ze względu na
zamianę elementów podczas partycjonowania listy. Pomimo tych wszystkich
problemów jest uważany za najlepszy wybór dla ogólnego algorytmu
sortowania; na przykład jest używany w bibliotekach C, w systemie
operacyjnym Unix oraz w kilku wersjach Javy.

Jeśli chodzi o sortowanie przez scalanie, złożoność najgorszego
przypadku to  \\(O (n  log n) \\), ale nie jest to metoda lokalna, ze
względu na wymagania dodatkowej pamięci w celu alokacji tymczasowych
tablic i kopiowania między tablice. Sortowanie przez scalanie nie jest
tak atrakcyjne w porównaniu z algorytmem w miejscu, jak sortowanie
szybkie, ale jest to doskonała opcja, gdy dane wejściowe są podzielone
na różne poziomy hierarchii pamięci komputera (np. pamięć podręczna,
pamięć główna, pamięć zewnętrzna). W takim przypadku sposób, w jaki
procesy sortowania przez scalanie uruchamiają dane w długich
strumieniach scalania, najlepiej wykorzystuje wszystkie dane
przeniesione jako blok na poziom pamięci, zmniejszając całkowitą liczbę
transferów pamięci. Najnowsze wersje systemu operacyjnego Linux używają
sortowania przez scalanie wielokierunkowe. Standardowa metoda sortowania
klasy list Pythona i tablic w Javie 7 jest zasadniczo sortowaniem przez
scalanie.

Wreszcie, jeśli musimy sortować wpisy za pomocą małych kluczy
całkowitych lub ciągów znaków, sortowanie kubełkowe lub sortowanie
oparte na podstawach jest doskonałym wyborem, ponieważ działa w czasie
\\(O (d (n + N)) \\) gdzie  \\([0 , N-1] )\\ to zakres kluczy całkowitych (oraz
\\(d = 1 \\) dla sortowania kubełkowego). Tak więc, jeśli  \\(d (n + N) \\)
jest poniżej funkcji  \\(n  log n \\), ta metoda sortowania działa szybciej
niż inne.

Podrozdział 5: Selection
------------------------

Selekcja to problem wyboru  \\(k \\) - najmniejszego elementu z
nieposortowanej kolekcji  (n ) elementów. Natychmiastowym rozwiązaniem
jest posortowanie kolekcji, a następnie indeksowanie do sekwencji o
indeksie  \\(k-1 \\). Wymaga to czasu  \\(O (n  log n) \\).

Problem można rozwiązać w czasie \\ (O (n) \\) za pomocą techniki znanej
jako prune-and-search. W tym przypadku problem zdefiniowany na zbiorze
\\(n \\) obiektów jest rozwiązywany przez przycięcie części obiektów i
rekurencyjne rozwiązanie mniejszego problemu. Następnie, gdy napotkany
zostanie problem zdefiniowany na zbiorze obiektów o stałej wielkości,
można go rozwiązać za pomocą jakiejś bezpośredniej metody. Powrót do
wszystkich wywołań rekurencyjnych kończy rozwiązanie. Na przykład
wyszukiwanie binarne jest przykładem techniki przycinania i
wyszukiwania.

Randomizowana szybka selekcja to zastosowanie metody przycinania i
wyszukiwania w celu rozwiązania problemu selekcji. Biorąc pod uwagę
nieposortowaną sekwencję  \\(S \\) z  \\(n \\) porównywalnych elementów i liczbę
całkowitą  \\(k\\ ) w  \\([1, n] \\), wybierz losowo element obrotowy z  \\(S \\) i
użyj go, aby podzielić  \\(S \\) na trzy podciągi  \\(L \\),  \\(E\\) i  \\(G \\),
które zawierają elementy  \\(S \\) mniejsze niż, równe i większe niż
odpowiednio. Na etapie przycinania, na podstawie wartości  \\(k \\) i
wielkości tych podzbiorów, określa się, który z trzech podzbiorów
zawiera żądany element. Następnie na odpowiednim podzbiorze stosowana
jest rekursja, zwracając uwagę, że ranga żądanego elementu w podzbiorze
może różnić się od jego rangi w pełnym zbiorze.

Poniżej przedstawiono implementację randomizowanego szybkiego wyboru::

 1  def quickselect(S, k):
 2    # returns the k-th smallest element of S, for k from 1 to len(S)
 3    if len(S) == 1:
 4      return S[0]
 5    pivot = random.choice(S)            # pick random pivot element from S
 6    L = [x for x in S if x < pivot]     # elements less than pivot
 7    E = [x for x in S if x == pivot]    # elements equal to pivot
 8    G = [x for x in S if pivot < x]     # elements greater than pivot
 9    if k <= len(L):
 10     return quickselect(L, k)     # the k-th smallest is in L
 11   elif k <= len(L) + len(E):
 12     return pivot                 # the k-th smallest is equal to pivot
 13   else:
 14     j = k-len(L)-len(E)
 15     return quickselect(G, j)     # the k-th smallest is the j-th in G

Za pomocą argumentu probabilistycznego (którego nie pokazujemy),
algorytm ten działa w czasie  \\(O (n) \\) po wszystkich możliwych losowych
wyborach dokonanych przez algorytm. W najgorszym przypadku losowy szybki
wybór przebiega w czasie  \\(O (n ^ 2) \\).

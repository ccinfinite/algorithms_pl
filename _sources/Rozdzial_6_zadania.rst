Rozdział 6: Sortowanie – ćwiczenia i wyzwania
=============================================


Podrozdział 1: Przegląd teorii
------------------------------

Uzupełnij i wykonaj fragmenty kodu w poniższych sekcjach (kod można
znaleźć w rozdziale szóstym).

**Ćwiczenie 1.1**

Sortowanie bąbelkowe porównuje pary sąsiednich wartości na liście i
wymienia je, jeśli nie są we właściwej kolejności. Wykonuje wiele
przejść na liście. Dla każdego przejścia największa wartość jest
przepuszczana do właściwej lokalizacji. Kod Pythona sortowania
bąbelkowego jest podany w poniższym ActiveCode.

Możemy napisać krótkie sortowanie bąbelkowe, które zatrzymuje się, jeśli
stwierdzi, że lista została już posortowana.

**Ćwiczenie 1.2**

Sortowanie przez wybór powoduje wiele przejść na liście, szukając
największej wartości i przesuwając ją we właściwe miejsce. Oznacza to,
że na każdy karnet przypada tylko jedna wymiana. Po pierwszym przejściu
największy przedmiot znajduje się we właściwym miejscu. Po drugim
przejściu następny największy jest na swoim miejscu. Kod Pythona
sortowania Wybór jest podany w poniższym ActiveCode.

**Ćwiczenie 1.3**

Sortowanie przez wstawianie tworzy posortowaną podlistę na początkowych
pozycjach listy i wstawia do tej podlisty nowy element, tworząc nową
posortowaną podlistę. Zaczynając od listy zawierającej tylko pierwszą
pozycję, każda pozycja od drugiej do ostatniej jest porównywana z tymi z
już posortowanej podlisty; większe pozycje są przesuwane w prawo, a
bieżąca pozycja jest wstawiana do podlisty po osiągnięciu mniejszej
pozycji. Kod Pythona dla sortowania przez wstawianie jest napisany w
następującym ActiveCode.

**Ćwiczenie 1.4**

Sortowanie przez scalanie jest algorytmem rekurencyjnym. Jeśli lista
jest pusta lub zawiera jeden element, jest sortowana. Jeśli lista
zawiera więcej niż jeden element, lista jest dzielona,​a sortowanie
przez scalanie jest wywoływane rekursywnie na obu połowach. Po
posortowaniu dwóch połówek następuje scalenie. Kod Pythona dla sekwencji
opartej na tablicach jest podany w poniższym ActiveCode.

**Ćwiczenie 1.5**

Szybkie sortowanie rozpoczyna wybieranie wartości, elementu
przestawnego, który jest zwykle pierwszą pozycją na liście. Zostanie
znaleziona jego poprawna pozycja na liście i ta pozycja (punkt podziału)
jest wykorzystywana w procesie dzielenia listy na dwie podlisty.
Quicksort będzie stosowany rekurencyjnie na tych podlistach.

Dwa znaczniki, lewy i prawy, są zdefiniowane jako odpowiednio pierwsza i
ostatnia pozycja pozostałych pozycji na liście. Lewy znacznik jest
zwiększany, dopóki nie zostanie znaleziona wartość większa niż oś
obrotu; prawy znak jest zmniejszany, dopóki nie zostanie znaleziona
wartość niższa niż oś obrotu. Te dwie wartości są nie na miejscu w
stosunku do punktu podziału i są wymieniane.

Ta procedura jest kontynuowana, dopóki prawy znak nie stanie się niższy
niż lewy; teraz punkt podziału został znaleziony. Wartość obrotu jest
wymieniana z zawartością punktu podziału, co oznacza, że oś obrotu jest
we właściwej pozycji. Quicksort jest teraz wywoływany rekurencyjnie na
dwóch podlistach. Funkcja szybkiego sortowania jest zdefiniowana w
następujący sposób.

**Ćwiczenie 1.6**

Selekcja to problem wyboru  \\(k \\) - najmniejszego elementu z
nieposortowanej kolekcji  \\(n \\) elementów.

Biorąc pod uwagę nieposortowaną sekwencję  \\(S \\) z  \\(n \\) porównywalnych
elementów i liczbę całkowitą  \\(k \\) w  \\([1, n] \\), randomizowany szybki
wybór wybiera element przestawny z  \\(S \\) losowo i użyj go do podzielenia
\\(S \\) na trzy podciągi  \\(L \\),  \\(E \\) i  \\(G \\), które zawierają elementy
\\(S \\) mniejsze niż, odpowiednio równe i większe niż oś obrotu.
Następnie, na podstawie wartości  (k ) i wielkości tych podzbiorów,
określa się, który z trzech podzbiorów zawiera żądany element. Następnie
na odpowiednim podzbiorze stosowana jest rekursja. Poniżej przedstawiono
implementację losowego szybkiego wyboru.

Podrozdział 2: Problemy z sortowaniem
-------------------------------------

W tej sekcji pokażemy klasyczny algorytm sortowania. Sprawdź, uzupełnij
i uruchom ActiveCode w każdej podsekcji.

**Ćwiczenie 2.1**

Pigeonhole sort to algorytm, który nadaje się do sortowania sekwencji
elementów, w których liczba elementów i liczba możliwych wartości kluczy
są w przybliżeniu takie same. Wymaga czasu  \\(O (n + R) \\), gdzie  \\(n \\) to
liczba elementów w sekwencji, a  \\(R \\), zakres, to liczba możliwych
wartości.

Algorytm znajduje wartości minimalne i maksymalne w sekwencji
(odpowiednio min i max) oraz zakres  \\(R = max-min + 1 \\). Powstaje nowa
tablica przegródek, początkowo pusta. Jego rozmiar jest równy zakresowi
\\(R \\).

Każdy element  \\(arr [i] \\) tablicy początkowej jest umieszczany w otworze
o indeksie  \\(arr [i] - min \\). Następnie elementy tablicy pigeonhole,
niepuste dziury, są umieszczane z powrotem w oryginalnej tablicy.
Sortowanie z przegródkami ma ograniczone zastosowanie, ponieważ
wymagania są rzadko spełniane. W przypadku tablic, w których zakres jest
znacznie większy niż  \\(n \\), sortowanie kubełkowe jest uogólnieniem,
które jest bardziej wydajne w przestrzeni i czasie. Implementacja
sortowania z przegródkami jest pokazana w poniższym ActiveCode.

**Ćwiczenie 2.2**

Comb sort to ulepszenie sortowania bąbelkowego. Sortowanie bąbelkowe
porównuje sąsiednie wartości i usuwa wszystkie inwersje. Comb sort
wykorzystuje przerwy o rozmiarze większym niż 1. Luka zaczyna się od
dużej wartości i zmniejsza się o współczynnik 1,3 w każdej iteracji, aż
do osiągnięcia wartości 1. W ten sposób sortowanie grzebieniowe usuwa
więcej niż jeden licznik inwersji przy jednej wymianie.

Współczynnik skurczu został empirycznie ustalony na 1,3. W przeciętnym
przypadku ten algorytm działa lepiej niż sortowanie bąbelkowe. Średnia
złożoność czasowa algorytmu wynosi  \\( Omega (n ^ 2/2 ^ p) \\), gdzie  \\(p \\)
to liczba przyrostów. Złożoność najgorszego przypadku tego algorytmu to
\\(O (n ^ 2) \\), a złożoność najlepszego przypadku to  \\(O (nlogn) \\).
Poniżej znajduje się realizacja.

**Ćwiczenie 2.3**

Wyszukiwanie z przeskokiem to algorytm wyszukiwania posortowanych
tablic. Sprawdza mniej elementów (niż wyszukiwanie liniowe) poprzez
przeskakiwanie do przodu o ustalone kroki lub pomijanie niektórych
elementów, zamiast przeszukiwania wszystkich elementów. Załóżmy, że mamy
tablicę  \\(arr [] \\) o rozmiarze  \\(n \\) i blok (do przeskoczenia) o
rozmiarze  \\(m \\). Szukamy według indeksów  \\(arr[0], arr[m], arr[2m], \ldots, arr[km], \ldots \\) i tak dalej. Po znalezieniu
przedziału  \\(arr[km] <x <arr [(k + 1) m] \\), wykonujemy operację
wyszukiwania liniowego z indeksu  \\(km \\), aby znaleźć element \\ (x \\).

W najgorszym przypadku musimy wykonać skoki \\(n/m\\), a jeśli ostatnio
sprawdzana wartość jest większa niż szukany element, wykonujemy
porównania \\(m-1\\) dla wyszukiwania liniowego. Dlatego całkowita liczba
porównań w najgorszym przypadku wyniesie  \\(((n / m) + m-1) \\). Funkcja
\\(f (m) = (n / m) + m-1 \\) jest minimalna, gdy  \\(m =  sqrt n \\), dlatego
najlepszym rozmiarem kroku jest  \\(m =  sqrt n \\). Poniżej kod
implementujący wyszukiwanie Jump.

**Ćwiczenie 2.4**

Wyszukiwanie wykładnicze w posortowanej tablicy obejmuje dwa kroki::

	Find range where element x is present
	Do Binary Search in that range

Aby znaleźć zakres, zaczynamy od podtablicy o rozmiarze 1, porównujemy
jej ostatni element z  \\(x \\), następnie spróbuj rozmiar 2, potem 4 i tak
dalej, aż ostatni element podtablicy nie będzie większy niż \\(x \\). Gdy
już znajdziemy indeks  \\(i \\), wiemy, że element musi znajdować się między
\\(i / 2 \\) a  \\(i \\). Złożoność czasowa to  \\(O (log n) \\), a przestrzeń
wymagana przez wyszukiwanie binarne  \\(O (log n) \\). W przypadku
iteracyjnego wyszukiwania binarnego potrzebowalibyśmy tylko spacji \\(O
(1) \\).

Podrozdział 3: Ćwiczenia i ocena własna
---------------------------------------

**Ćwiczenie 3.1**

Biorąc pod uwagę następującą listę liczb do sortowania: [19, 1, 9, 7, 3,
10, 13, 15, 8, 12]; która lista jest uzyskiwana po trzech pełnych
przejściach Bubble Sort?

**Ćwiczenie 3.2**

Biorąc pod uwagę następującą listę liczb do sortowania: [11, 7, 12, 14,
19, 1, 6, 18, 8, 20]; jaka lista jest uzyskiwana po trzech pełnych
przejściach Select Sort?

**Ćwiczenie 3.3**

Biorąc pod uwagę następującą listę liczb do sortowania: [15, 5, 4, 18,
12, 19, 14, 10, 8, 20]; która lista jest uzyskiwana po trzech pełnych
przejściach Insertion sort?

**Ćwiczenie 3.4**

Biorąc pod uwagę następującą listę liczb: [21, 1, 26, 45, 29, 28, 2, 9,
16, 49, 39, 27, 43, 34, 46, 40]; jaka lista ma zostać posortowana po
trzech rekurencyjnych wywołaniach Mergesort?

**Ćwiczenie 3.5**

Biorąc pod uwagę następującą listę liczb: [21, 1, 26, 45, 29, 28, 2, 9,
16, 49, 39, 27, 43, 34, 46, 40]; jakie są pierwsze dwie listy do
scalenia?

**Ćwiczenie 3.6**

Podaj następującą listę liczb [14, 17, 13, 15, 19, 10, 3, 16, 9, 12]:
jaka jest lista po drugim podziale Quicksort?

**Ćwiczenie 3.7**

Które spośród sortowania według Shell-, Quick-, Merge- lub
Insertion-sort gwarantują w najgorszym przypadku  \\(O (n  log n) \\)?

**Ćwiczenie 3.8**

Algorytm sortujący wpisy klucz-wartość według klucza rozbija się, jeśli
w dowolnym momencie dwa wpisy  \\(e_i \\) i  \\(e_j \\) mają równe klucze, ale \\(e_i \\) pojawia się przed  \\(e_j \\) w danych wejściowych, a następnie
algorytm umieszcza w danych wyjściowych  \\(e_i \\) po  \\(e_j \\). Opisz zmianę
w algorytmie sortowania przez scalanie, aby stał się stabilny.

**Ćwiczenie 3.9**

Załóżmy, że mamy dwie  \\(n \\) - posortowane sekwencje  \\(A \\) i  \\(B \\) każda
z odrębnymi elementami, ale z pewnymi elementami w obu sekwencjach.
Znajdź \\ (O (n) \\) - metodę czasu do obliczenia sekwencji reprezentującej
sumę  \\(A \\) i  \\(B \\) (bez duplikatów) jako posortowaną sekwencję.

**Ćwiczenie 3.10**

Spośród \\(n! \\) możliwych danych wejściowych danego algorytmu sortowania
opartego na porównaniach, jaka jest maksymalna liczba danych
wejściowych, które można poprawnie posortować za pomocą samych  \\(n \\)
porównań?

**Ćwiczenie 3.11**

Załóżmy, że  \\(S \\) jest sekwencją wartości  \\(n \\), z których każda jest
równa 0 lub 1. Jak długo zajmie sortowanie \\ (S \\) za pomocą algorytmu
sortowania przez scalanie? A Quicksort?
Rozdział 5: Listy dowiązane – ćwiczenia i wyzwania
**************************************************

Podrozdział 1: Przegląd teorii
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Ćwiczenie 1.1**

Uzupełnij i wykonaj następujący fragment kodu (który można znaleźć w
rozdziale piątym), w którym stos ADT jest zaimplementowany za pomocą
pojedynczej połączonej listy. Pamiętaj, że każda instancja stosu
przechowuje dwie zmienne: ``_head`` (odwołanie do węzła na początku listy)
i ``_size`` (które śledzi bieżącą liczbę elementów). Dodaj kilka przykładów
działania stosu, używając wywołań metod ``pop``, ``push`` lub ``top``.

.. activecode:: stack-as-a-singly-linked-list
   :language: python
   :caption: Implementation of a stack by a singly linked list

   class LinkedStack:
   # Stack implementation with a singly linked list

     # Node class nested in stack
     class Node:
       __slots__ = '_element' , '_next'
       def __init__(self, element, next):
         self._element = element
         self._next = next

     # Stack methods
     def __init__(self):
       self._head = None
       self._size = 0

     def __len__(self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def push(self, e):
       self._head = self._Node(e, self._head)
       self._size += 1

     def top(self):
       if self.is_empty( ):
         raise Empty('Stack is empty')
       return self._head._element

     def pop(self):
       if self.is_empty( ):
         raise Empty('Stack is empty')
       answer = self._head._element
       self._head = self._head._next
       self._size −= 1
       return answer

           # INSERT NEW CODE HERE

**Ćwiczenie 1.2**

Uzupełnij i wykonaj następujący fragment kodu (który można znaleźć w
rozdziale piątym), w którym kolejka ADT jest zaimplementowana za pomocą
pojedynczej listy dowiązanej. Pamiętaj, że wykonujemy operacje na obu
końcach kolejki, co oznacza, że musimy utrzymywać referencje ``_head`` i
``_tail`` jako zmienne instancji dla każdej kolejki, wraz z jej ``_size``.
Przód kolejki jest wyrównany z nagłówkiem listy, a tył kolejki jest
wyrównany z końcem listy, co pozwala nam umieszczać w kolejce elementy z
tyłu i usuwać je z kolejki od przodu. Dodaj kilka przykładów działania
``queue`` używając najpierw ``first``, ``enqueue`` lub ``degueue``.

.. activecode:: queue-as-a-singly-linked-list
   :language: python
   :caption: Implementation of a queue by a singly linked list

   class LinkedQueue:
   # Queue implementation with a singly linked list

     class Node:
       __slots__ = '_element' , '_next'
       def __init__(self, element, next):
         self._element = element
         self._next = next

     def __init__ (self):
       self._head = None
       self._tail = None
       self._size = 0

     def __len__ (self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def first(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       return self._head._element

     def dequeue(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       answer = self._head._element
       self._head = self._head._next
       self._size −= 1
       if self.is_empty( ):       # special case if queue is empty
         self._tail = None        # removed head had been the tail
       return answer

     def enqueue(self, e):
       newnode = self._Node(e, None)
       if self.is_empty( ):
         self._head = newnode
       else:
         self._tail._next = newnode
       self._tail = newnode
       self._size += 1

              # INSERT NEW CODE HERE



**Ćwiczenie 1.3**

Uzupełnij i wykonaj następujący fragment kodu (który można znaleźć w
rozdziale piątym), w którym kolejka ADT jest zaimplementowana za pomocą
cyklicznie dowiązanej listy. Pamiętaj, że kolejka ma głowę i ogon, a
następne odniesienie ogona jest połączone z głową. Musimy zachować
zmienne instancji ``_tail`` (odniesienie do węzła ogona) i ``_size`` (liczbę
elementów w kolejce). Początek kolejki znajduje się podążając za
następną referencją ogona, czyli ``self._tail._next``. Przy metodzie
``rotate`` stara głowa staje się nowym ogonem, a węzeł po starej głowie
staje się nową głową. Dodaj kilka przykładów działania kolejki, używając
najpierw metod ``first``, ``enqueue`` lub ``dequeue``.

.. activecode:: queue-as-a-circularly-list
   :language: python
   :caption: Implementation of a queue by means of a circularly list

   class CircularQueue:
   # Queue implementation using circularly linked list

     class _Node:
       __slots__ = '_element' , '_next'
       def __init__(self, element, next):
         self._element = element
         self._next = next

     def __init__(self):
       self._tail = None
       self._size = 0

     def __len__ (self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def first(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       head = self._tail._next
       return head._element

     def dequeue(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       oldhead = self._tail._next
       if self._size == 1:
         self._tail = None
       else:
         self._tail._next = oldhead._next
       self._size −= 1
       return oldhead._element

     def enqueue(self, e):
       newnode = self._Node(e, None)
       if self.is_empty( ):
         newnode._next = newnode
       else:
         newnode._next = self._tail._next
         self._tail._next = newnode
       self._tail = newnode
       self._size += 1

     def rotate(self):
       if self._size > 0:
         self._tail = self._tail._next


      # INSERT NEW CODE HERE

**Ćwiczenie 1.4**

Uzupełnij i wykonaj następujący fragment kodu (który można znaleźć w
rozdziale piątym), w którym deque ADT jest zaimplementowane za pomocą
listy podwójnie dowiązanej. Należy zauważyć, że klasa ``LinkedDeque``
dziedziczy z klasy ``_DoublyLinkedBase``, używając jej metod do inicjowania
nowego wystąpienia w celu wstawienia elementu z przodu lub na końcu oraz
usunięcia elementu. Pierwszy element deque jest przechowywany po
nagłówku, a ostatni element deque jest przechowywany w węźle przed
zwiastunem. Dodaj kilka przykładów działania deque, używając wywołań
metod ``insert_first``, ``insert_last``, ``delete_first`` i ``delete_last``.

.. activecode:: deque-as-a-doubly_linked-list
   :language: python
   :caption: Implementation of a deque by means of a doubly linked list

   class _DoublyLinkedBase:

     class Node:
       __slots__ = '_element' , '_prev' , '_next'
       def __init__(self, element, prev, next):
         self._element = element
         self._prev = prev                # previous node reference
         self._next = next                # next node reference

     def __init__(self):
       self._header = self._Node(None, None, None)
       self._trailer = self._Node(None, None, None)
       self._header._next = self._trailer        # trailer is after header
       self._trailer._prev = self._header        # header is before trailer
       self._size = 0

     def __len__(self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def _insert_between(self, e, predecessor, successor):
       newnode = self._Node(e, predecessor, successor)
       predecessor._next = newnode
       successor._prev = newnode
       self._size += 1
       return newnode

     def _delete_node(self, node):
       predecessor = node._prev
       successor = node._next
       predecessor._next = successor
       successor._prev = predecessor
       self._size −= 1
       element = node._element                             # record deleted element
       node._prev = node._next = node._element = None      # useful for the garbage collection
       return element                                      # return deleted element

   class LinkedDeque(_DoublyLinkedBase):

     def first(self):
       if self.is_empty( ):
         raise Empty("Deque is empty")
       return self._header._next._element

     def last(self):
       if self.is_empty( ):
         raise Empty("Deque is empty")
       return self._trailer._prev._element

     def insert_first(self, e):
       self._insert_between(e, self._header, self._header._next)

     def insert_last(self, e):
       self._insert_between(e, self._trailer._prev, self._trailer)

     def delete_first(self):
       if self.is_empty( ):
         raise Empty("Deque is empty")
       return self._delete_node(self._header._next)

     def delete_last(self):
       if self.is_empty( ):
         raise Empty("Deque is empty")
       return self._delete_node(self._trailer._prev)

Podrozdział 2: Problemy z listami dowiązanymi
---------------------------------------------

W tym podrozdziale zajmiemy się kilkoma klasycznymi problemami, które
można rozwiązać za pomocą list dowiązanych. Przestudiuj każde ćwiczenie,
a następnie sprawdź, uzupełnij (jeśli to konieczne) i uruchom
dostarczony ActiveCode.

**Ćwiczenie 2.1**

Napisz funkcję zliczającą liczbę węzłów w danej liście połączonej
pojedynczo, zarówno w wersji iteracyjnej, jak i rekurencyjnej.
Iteracyjne rozwiązanie tego problemu znajduje się w następującym
pseudokodzie::

 initialize count as 0
 initialize a node pointer, current = head.
 do following while current is not NULL
   current = current -> next
   count++;
 return count

Sprawdź, uzupełnij i uruchom następujący kod ActiveCode.

.. activecode:: number-of-nodes-list-iterative
   :language: python
   :caption: Returns the number of nodes in a list, with an iteration

   class Node:
     def __init__(self, data):
       self.data = data
       self.next = None

   class LinkedList:
     def __init__(self):
       self.head = None

     def push(self, new_data):
       new_node = Node(new_data)
       new_node.next = self.head
       self.head = new_node

     def getCount(self):
       temp = self.head
       count = 0
       while (temp):
         count += 1
         temp = temp.next
       return count

   if __name__=='__main__':
     llist = LinkedList()
     llist.push(1)
     llist.push(3)
     llist.push(1)
     llist.push(2)
     llist.push(1)
     print ("Count of nodes is :",llist.getCount())

Rekurencyjne rozwiązanie tego problemu znajduje się w następującym
pseudokodzie::

 int getCount(head)
   if head is NULL, return 0
   else return 1 + getCount(head->next)

Sprawdź, uzupełnij i uruchom następujący kod ActiveCode.

.. activecode:: number-of-nodes-list-iterative-2
   :language: python
   :caption: Returns the number of nodes in a list, with an iteration

   class Node:
     def __init__(self, data):
       self.data = data
       self.next = None

   class LinkedList:
     def __init__(self):
       self.head = None

     def push(self, new_data):
       new_node = Node(new_data)
       new_node.next = self.head
       self.head = new_node

     def getCountRec(self, node):
        if (not node):
          return 0
        else:
          return 1 + self.getCountRec(node.next)

     def getCount(self):
       return self.getCountRec(self.head)

   if __name__=='__main__':
     llist = LinkedList()
     llist.push(1)
     llist.push(3)
     llist.push(1)
     llist.push(2)
     llist.push(1)
     print 'Count of nodes is :',llist.getCount()


**Ćwiczenie 2.2**

Mając listę dowiązaną i liczbę  \\(n \\), napisz funkcję, która zwraca
wartość w węźle  \\(n \\) - od końca listy dowiązanej. Proste rozwiązanie
polega na obliczeniu długości len listy, a następnie wydrukowaniu len-n
+ 1-ty węzeł od początku listy. Kod ActiveCode jest podany poniżej.

.. activecode:: Nth-node-from-the-end
   :language: python
   :caption: Returns the n-th node from the and of the list

   class Node:
     def __init__(self, new_data):
       self.data = new_data
       self.next = None

   class LinkedList:
     def __init__(self):
       self.head = None

     def push(self, new_data):
       new_node = Node(new_data)
       new_node.next = self.head
       self.head = new_node

     def printNthFromLast(self, n):
       temp = self.head
       length = 0
       while temp is not None:
         temp = temp.next
         length += 1

        if n > length:
          print('Location is greater than the length of the list')
          return
        temp = self.head
        for i in range(0, length - n):
          temp = temp.next
        print(temp.data)

   llist = LinkedList()
   llist.push(20)
   llist.push(4)
   llist.push(15)
   llist.push(35)
   llist.printNthFromLast(4)

Innym ciekawym rozwiązaniem jest zachowanie dwóch wskaźników, referencji
i głównych wskaźników. Początkowo oba wskazują na początek listy, a
wskaźnik odniesienia jest przesuwany do  \\(n \\) węzłów z nagłówka.
Następnie oba wskaźniki są przesuwane jeden po drugim, aż wskaźnik
odniesienia osiągnie koniec. Teraz główny wskaźnik będzie wskazywał  \\(n\\) - węzeł od końca. Kod ActiveCode jest podany poniżej.

.. activecode:: Nth-node-from-the-end-2
   :language: python
   :caption: Returns the n-th node from the and of the list

   class Node:
     def __init__(self, new_data):
       self.data = new_data
       self.next = None

   class LinkedList:
     def __init__(self):
       self.head = None

     def push(self, new_data):
       new_node = Node(new_data)
       new_node.next = self.head
       self.head = new_node

     def printNthFromLast(self, n):
       main_ptr = self.head
       ref_ptr = self.head

       count = 0
       if(self.head is not None):
         while(count < n ):
           if(ref_ptr is None ):
             print('Location is greater than the length of the list')
             return
           ref_ptr = ref_ptr.next
           count += 1

       if(ref_ptr is None):
         self.head = self.head.next
         if(self.head is not None):
           print('The node no. %d from last is %d', n, main_ptr.data)
      else:
        while(ref_ptr is not None):
          main_ptr = main_ptr.next
          ref_ptr = ref_ptr.next
        print('The node no. %d from last is %d', n, main_ptr.data)

   llist = LinkedList()
   llist.push(20)
   llist.push(4)
   llist.push(15)
   llist.push(35)
   llist.printNthFromLast(4)


**Ćwiczenie 2.3**

Problem ten nosi imię Flawiusza Józefa, żydowskiego historyka, który
walczył z Rzymianami. On i jego grupa żołnierzy zostali otoczeni przez
Rzymian w jaskini i wybrali morderstwo oraz samobójstwo zamiast poddania
się i schwytania. Postanowili, że wszyscy żołnierze usiądą w kręgu i
zaczynając od żołnierza siedzącego na pierwszej pozycji, każdy żołnierz
zabije żołnierza obok nich. Załóżmy więc, że 5 żołnierzy siedzi w kręgu
z pozycjami ponumerowanymi jako 1, 2, 3, 4, 5. Żołnierz 1 zabija 2,
potem 3 zabija 4, potem 5 zabija 1, potem 3 zabija 5, a ponieważ 3 jest
jedyny pozostały wtedy 3 popełnia samobójstwo. Teraz Józef nie chce
zostać zamordowany ani popełnić samobójstwa i musi wymyślić, w jakiej
pozycji powinien siedzieć w kręgu, aby był ostatnim stojącym
człowiekiem, a zamiast popełnić samobójstwo, podda się Rzymianom.

Istnieje kilka rozwiązań tego problemu (ogólnie, dla żołnierzy \\ (n = 2 ^ a + k \\) ocalałym jest  \\(2k + 1 \\) - żołnierz). Pokazujemy tutaj
rozwiązanie, które używa okrągłej połączonej listy.

.. activecode:: josephus-problem
   :language: python
   :caption: Josephus problem with a circular linked list

   class Node:
     def __init__(self, x):
       self.data = x
       self.next = None

   def getJosephusPosition(m, n):
     # Create a circular linked list of size n.
    head = Node(1)
    prev = head
    for i in range(2, n + 1):
      prev.next = Node(i)
      prev = prev.next
    prev.next = head

     # while only one node is left in the linked list
     ptr1 = head
     ptr2 = head
     while (ptr1.next != ptr1):
       count = 1
       while (count != m):
         ptr2 = ptr1
         ptr1 = ptr1.next
         count += 1
       ptr2.next = ptr1.next
       ptr1 = ptr2.next

     print("Last person left standing (Josephus Position) is ", ptr1.data)

   if __name__ == '__main__':
     n = 14
     m = 2
     getJosephusPosition(m, n)


**Ćwiczenie 2.4**

Zadanie polega na tym, aby utworzyć listę podwójnie dowiązaną ze
wskaźnikami głowy i ogona, wstawiając węzły w taki sposób, aby lista
pozostawała w porządku rosnącym podczas drukowania od lewej do prawej.
Pseudokod wygląda następująco::


 if the list is empty, then
   make left and right pointers point to the node to be inserted
   make its previous and next field point to NULL
 if node to be inserted has value lower than the value of first node of the list, then
   connect that node from previous field of first node
 if node to be inserted has value higher than the value of last node of the list, then
   connect that node from next field of last node
  if node to be inserted has value in between the value of first and last node, then
    check for appropriate position and make connections


Sprawdź, uzupełnij i uruchom następujący kod ActiveCode.

.. activecode:: insert-in-sorted-list
   :language: python
   :caption: Insert a new value in a sorted list

   class Node:
   def __init__(self, data):
     self.info = data
     self.next = None
     self.prev = None

   head = None
   tail = None

   def nodeInsert( key) :
     global head
     global tail

     p = Node(0)
     p.info = key
     p.next = None

     # if first node to be inserted in doubly linked list
     if ((head) == None):
       (head) = p
       (tail) = p
       (head).prev = None
       return

     # if node to be inserted has value lower than first node
     if ((p.info) < ((head).info)):
       p.prev = None
       (head).prev = p
       p.next = (head)
       (head) = p
       return

     # if node to be inserted has value higher than last node
     if ((p.info) > ((tail).info)):
       p.prev = (tail)
       (tail).next = p
       (tail) = p
       return

     # find the node before which we need to insert p.
     temp = (head).next
     while ((temp.info) < (p.info)):
       temp = temp.next

     # insert new node before temp
     (temp.prev).next = p
     p.prev = temp.prev
     temp.prev = p
     p.next = temp

   # print nodes from left to right
   def printList(temp) :
     while (temp != None):
       print( temp.info, end = " ")
       temp = temp.next

   nodeInsert( 30)
   nodeInsert( 50)
   nodeInsert( 90)
   nodeInsert( 10)
   nodeInsert( 40)
   nodeInsert( 110)
   nodeInsert( 60)
   nodeInsert( 95)

   print("Doubly linked list on printing from left to right\n" )
   printList(head)


Podrozdział 3: Ćwiczenia i ocena własna
---------------------------------------

**Ćwiczenie 3.1**

Napisz algorytm znajdowania przedostatniego węzła na liście pojedynczo
dowiązanej, w której ostatni węzeł jest wskazywany przez następne
odwołanie None.

**Ćwiczenie 3.2**

Napisz algorytm łączenia dwóch pojedynczo dowiązanych list L i M, mając
tylko odniesienia do pierwszego węzła każdej listy, w pojedynczą listę
L, która zawiera wszystkie węzły L, a następnie wszystkie węzły M.
(Wskazówka: Konkatenacja nie nie przeszukuj wszystkich elementów dwóch
list)

**Ćwiczenie 3.3**

Napisz rekurencyjny algorytm, który zlicza liczbę węzłów na liście
pojedynczo dowiązanej. (Wskazówka: użyj ``next`` jako
parametru do wywołania funkcji)

**Ćwiczenie 3.4**

Napisz funkcję, która zlicza liczbę węzłów na liście dowiązanej
cyklicznie. (Wskazówka: Śledź punkt początkowy i zatrzymaj się, gdy do
niego dotrzesz)

**Ćwiczenie 3.5**

Załóżmy, że x i y są odniesieniami do węzłów list dołączonych
cyklicznie, choć niekoniecznie tej samej listy. Napisz algorytm
określający, czy x i y należą do tej samej listy. (Wskazówka: Obejdź raz
jedną z list)

**Ćwiczenie 3.6**

W rozdziale piątym, sekcja 2.1, można znaleźć implementację kolejki z
listą dowiązaną cyklicznie. Klasa ``CircularQueue`` udostępnia metodę
``rotate()``, której semantyka odpowiada Q.enqueue (Q.dequeue()). W zasadzie
stara głowa staje się nowym ogonem, a węzeł po starej głowie staje się
nową głową. Napisz tę samą metodę dla klasy ``LinkedQueue`` z sekcji 1.2, w
której kolejka jest implementowana z pojedynczą listą. (Wskazówka:
Dostosuj łącza tak, aby pierwszy węzeł został przeniesiony na koniec
listy)

**Ćwiczenie 3.7**

Napisz metodę znajdowania środkowego węzła podwójnie dowiązanej listy z
nagłówkami i znacznikami nagłówka, za pomocą przeskakiwania linków (bez
użycia licznika). W przypadku parzystej liczby węzłów, zgłoś węzeł nieco
na lewo od środka jako środkowy. (Wskazówka: Przeszukaj listę zaczynając
od obu końców)

**Ćwiczenie 3.8**

Napisz szybki algorytm do łączenia dwóch podwójnie połączonych list L i
M, z nagłówkami i węzłami wartowniczymi nagłówka, w pojedynczą listę L.
(Wskazówka: odpowiednio połącz koniec L i początek M)

**Ćwiczenie 3.9**

Napisz metodę ``max(L)``, która zwraca maksymalny element z wystąpienia
``PositionalList`` L zawierającego liczby całkowite.

**Ćwiczenie 3.10**

Zaktualizuj klasę ``PositionalList`` w rozdziale piątym, podrozdział 4.2,
aby obsługiwała dodatkową metodę ``find(e)``, która zwraca pozycję
(pierwszego wystąpienia) elementu e na liście.

**Ćwiczenie 3.11**

Opisz implementację metod PositionalList ``add_last`` i ``add_before``, używając
tylko metod ``is_empty``, ``first``, ``last``, ``prev``, ``next``, ``add_after`` i ``add_first``.

**Ćwiczenie 3.12**

Napisz kompletną implementację stosu ADT i kolejki ADT za pomocą
pojedynczo dowiązanej listy, która używa nagłówka wartownika.

**Ćwiczenie 3.13**

Napisz metodę ``concatenate(Q2`` dla klasy ``LinkedQueue``, która pobiera
wszystkie elementy ``Q2`` i dołącza je na końcu oryginalnej kolejki.
(Wskazówka: pracuj nad członkami głowy i ogona obu list)

**Ćwiczenie 3.14**

Podaj rekurencyjną implementację pojedynczo dowiązanej klasy listy, tak
że wystąpienie niepustej listy przechowuje swój pierwszy element i
odwołanie do listy pozostałych elementów. Następnie napisz rekurencyjny
algorytm odwracania listy. (Wskazówka: łańcuch węzłów następujący po
węźle głównym jest listą. Aby odwrócić listę, powtarzaj na pierwszych
\\(n − 1 \\) pozycjach)

**Ćwiczenie 3.15**

Zaimplementuj funkcję ``bubble-sort``, która przyjmuje jako
parametr listę L (dowiązana pojedynczo lub podwójnie). (Wskazówka:
sortowanie bąbelkowe skanuje listę  \\(n − 1 \\) razy. Przy każdym
skanowaniu porównuje bieżący element z następnym i zamienia je (``swap``), jeśli
nie są w porządku. Pomocna byłaby funkcja wymiany)

**Ćwiczenie 3.16**

Tablica  \\(A \\) jest rzadka, jeśli większość jej wpisów jest pusta. Lista
L może być użyta do efektywnego zaimplementowania takiej tablicy. Dla
każdej niepustej komórki  \\(A [i] \\) możemy przechowywać wpis \\ ((i, e) \\) w
L, gdzie e jest elementem przechowywanym w  \\(A [i] \\). To pozwala nam
reprezentować  (A ) przy użyciu pamięci \\ (O (m) \\), z  \\(m \\) liczbą
niepustych wpisów w  \\(A \\). Napisz klasę ``SparseArray`` za pomocą metod
``getitem(j)`` i ``setitem(j, e)``.

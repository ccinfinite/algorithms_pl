Rozdział 3: Sekwencje oparte na tablicach – ćwiczenia i wyzwania
****************************************************************


Podrodział 1: Przegląd teorii
-----------------------------

**Ćwiczenie 1.1**

Uzupełnij i wykonaj następujący fragment kodu (który można znaleźć w
rozdziale trzecim, podrozdziale 3). Porównaj wyniki w swoim systemie z
wynikami przedstawionymi w rozdziale trzecim. 
*(Wskazówka: chociaż
prędkość może się różnić, asymptotyka powinna być podobna).*

Następnie zmodyfikuj kod, aby zademonstrować, że klasa *list* w Pythonie
czasami zmniejsza rozmiar swojej tablicy bazowej, gdy elementy są
usuwane z listy. 
*(Wskazówka: zrób listę wystarczająco dużą, zanim
zaczniesz usuwać wpisy).*

.. activecode:: size-of-data
   :language: python
   :caption: Prints the size of a dynamic array wrt its elements

   import sys               # includes the function getsizeof

   data = [ ] 			    # empty list
   for k in range(n):
     a = len(data) 			    # a is the number of elements
     b = sys.getsizeof(data) 	# size of data in bytes
     print('Length: {0:3d}; Size in bytes: {1:4d}'.format(a, b))
     data.append(None) 		    # increase length by one

           # INSERT NEW CODE HERE


**Ćwiczenie 1.2**

Implementacja ``insert`` dla klasy ``DynamicArray`` została przedstawiona
w rozdziale 3, podrozdziale 4.2. Gdy nastąpi zmiana rozmiaru, operacja
zmiany rozmiaru zajmuje trochę czasu, aby skopiować wszystkie elementy
ze starej tablicy do nowej, a następnie kolejna pętla w treści
``insert`` przesuwa wiele z tych elementów. Zmodyfikuj następujący
fragment kodu, aby w przypadku zmiany rozmiaru elementy zostały
przesunięte do ich ostatecznej pozycji podczas tej operacji, unikając w
ten sposób późniejszego przesunięcia. *(Wskazówka: użyj dwóch
niezagnieżdżonych pętli)*

.. activecode:: insert-for-dynamic-array
   :language: python
   :caption: Insert a value at position k in a dynamic array

   def insert(self, k, value):

     if self._n == self._capacity: 		# not enough room, resize the array
       self._resize(2*self._capacity)
     for j in range(self._n, k, −1): 	# shift to right, rightmost first
       self._A[j] = self._A[j−1]
     self._A[k] = value
     self. n += 1


Podrodział 2: Problem - tworzenie gry w kółko i krzyżyk
-------------------------------------------------------

W tym podrodziale zbudujemy dwuosobową grę w kółko i krzyżyk, w którą
możemy grać z poziomu wiersza poleceń. Sprawdź następujący kod,
następnie uzupełnij i uruchom *ActiveCode* na końcu sekcji.

Początkowo zbudujemy pustą planszę \\(3 \times 3\\)
ponumerowaną jak klawiatura numeryczna. Gracz może wykonać swój ruch na
planszy, wpisując numer z klawiatury numerycznej. W tym celu użyjemy
słownika, prymitywnego typu danych w Pythonie, który przechowuje dane w
formacie „klucz: wartość”. Stworzymy słownik o długości 9, a każdy
klawisz będzie reprezentował blok na tablicy. Odpowiadająca mu wartość
będzie reprezentować ruch wykonany przez gracza. Stworzymy funkcję
``printBoard()``, której będziemy mogli używać za każdym razem, gdy chcemy
wydrukować zaktualizowaną planszę w grze::

 theBoard = {'7': ' ' , '8': ' ' , '9': ' ' ,
             '4': ' ' , '5': ' ' , '6': ' ' ,
             '1': ' ' , '2': ' ' , '3': ' ' }

 def printBoard(board):
   print(board['7'] + '|' + board['8'] + '|' + board['9'])
   print('-+-+-')
   print(board['4'] + '|' + board['5'] + '|' + board['6'])
   print('-+-+-')
   print(board['1'] + '|' + board['2'] + '|' + board['3'])

Teraz w funkcji głównej najpierw pobierzemy dane wejściowe z odtwarzacza
i sprawdzimy, czy dane wejściowe są prawidłowym ruchem, czy nie. Jeśli
tak, wypełnimy ten blok. W przeciwnym razie poprosimy użytkownika o
wybranie innego bloku::

 def game():

   turn = 'X'
   count = 0

   for i in range(10):
     printBoard(theBoard)
     print("It's your turn," + turn + ".Move to which place?")

     move = input()
     if theBoard[move] == ' ':
       theBoard[move] = turn
       count += 1
     else:
       print("That place is already filled. Move to which place?")
       continue

             

Sprawdzamy teraz łącznie 8 warunków, szukając zwycięskiego. Niezależnie
od tego, który gracz wykonał ostatni ruch, ogłosimy tego gracza jako
zwycięzcę. W przeciwnym razie, jeśli plansza się zapełni i nikt nie
wygra, zadeklarujemy wynik jako remis::

 # check if player X or O has won, for every move after 5 moves.
 if count >= 5:
   if theBoard['7'] == theBoard['8'] == theBoard['9'] != ' ': # across the top
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['4'] == theBoard['5'] == theBoard['6'] != ' ': # across the middle
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['1'] == theBoard['2'] == theBoard['3'] != ' ': # across the bottom
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['1'] == theBoard['4'] == theBoard['7'] != ' ': # down the left side
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['2'] == theBoard['5'] == theBoard['8'] != ' ': # down the middle
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['3'] == theBoard['6'] == theBoard['9'] != ' ': # down the right side
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['7'] == theBoard['5'] == theBoard['3'] != ' ': # diagonal
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['1'] == theBoard['5'] == theBoard['9'] != ' ': # diagonal
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break

 # If neither X nor O wins and the board is full, declare the result as a tie.
 if count == 9:
   print("\nGame Over.\n")
   print("It's a Tie!!")

 # change the player after every move.
   if turn =='X':
     turn = 'O'
   else:
       turn = 'X'

Na koniec pytamy użytkowników, czy chcą ponownie uruchomić grę::


 board_keys = []
 for key in theBoard:
   board_keys.append(key)

 restart = input("Do want to play Again?(y/n)")
 if restart == "y" or restart == "Y":
   for key in board_keys:
     theBoard[key] = " "
   game()


Sprawdź i uzupełnij poniższy kod *ActiveCode* oraz uruchom grę w kółko i
krzyżyk.

.. activecode:: tic-tac-toe
   :language: python
   :caption: Play the game!

   theBoard = {'7': ' ' , '8': ' ' , '9': ' ' ,
              '4': ' ' , '5': ' ' , '6': ' ' ,
              '1': ' ' , '2': ' ' , '3': ' ' }

   board_keys = []

   for key in theBoard:
     board_keys.append(key)

   def printBoard(board):
     print(board['7'] + '|' + board['8'] + '|' + board['9'])
     print('-+-+-')
     print(board['4'] + '|' + board['5'] + '|' + board['6'])
     print('-+-+-')
     print(board['1'] + '|' + board['2'] + '|' + board['3'])

   def game():
     turn = 'X'
     count = 0
     for i in range(10):
       printBoard(theBoard)
       print("It's your turn," + turn + ".Move to which place?")

       move = input()

       if theBoard[move] == ' ':
         theBoard[move] = turn
         count += 1
       else:
         print("That place is already filled.\nMove to which place?")
         continue

       if count >= 5:
         if theBoard['7'] == theBoard['8'] == theBoard['9'] != ' ': # across the top
           printBoard(theBoard)
           print("\nGame Over.\n")
           print(" **** " +turn + " won. ****")
           break
       elif theBoard['4'] == theBoard['5'] == theBoard['6'] != ' ': # across the middle
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['1'] == theBoard['2'] == theBoard['3'] != ' ': # across the bottom
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['1'] == theBoard['4'] == theBoard['7'] != ' ': # down the left side
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['2'] == theBoard['5'] == theBoard['8'] != ' ': # down the middle
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['3'] == theBoard['6'] == theBoard['9'] != ' ': # down the right side
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['7'] == theBoard['5'] == theBoard['3'] != ' ': # diagonal
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['1'] == theBoard['5'] == theBoard['9'] != ' ': # diagonal
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break

       if count == 9:
         print("\nGame Over.\n")
         print("It's a Tie!!")

       if turn =='X':
         turn = 'O'
       else:
         turn = 'X'

     restart = input("Do want to play Again?(y/n)")
     if restart == "y" or restart == "Y":
       for key in board_keys:
         theBoard[key] = " "

       game()

   if __name__ == "__main__":
     game()


Podrodział 3: Ćwiczenia i ocena własna
--------------------------------------

**Ćwiczenie 3.1**

Niech (A) będzie tablicą o rozmiarze \\(n \geq 2\\) zawierającą
liczby całkowite od 1 do \\(n−1\\), z powtórzeniem dokładnie jednej. Opisz
szybki algorytm znajdowania liczby całkowitej w \\(A\\), która się powtarza.
*(Wskazówka: nie sortuj A).*

**Ćwiczenie 3.2**

Oblicz sumę wszystkich liczb w zbiorze danych \\(n \\times n\\),
reprezentowanym jako lista list, przy użyciu standardowych struktur
kontrolnych. *(Wskazówka: użyj zagnieżdżonych pętli).*

**Ćwiczenie 3.3**

Opisz, jak wbudowaną funkcję ``sum`` można połączyć ze składnią rozumienia
języka Python w celu obliczenia sumy wszystkich liczb w zbiorze danych
\\(n \\times n\\), reprezentowanym jako lista list. *(Wskazówka:
zbuduj listę sum częściowych, po jednej dla każdej zagnieżdżonej
listy).*

**Ćwiczenie 3.4**

Metoda shuffle modułu ``random`` pobiera listę Pythona i zmienia ją losowo,
co oznacza, że każda możliwa kolejność jest równie prawdopodobna. Zbuduj
swoją własną wersję takiej funkcji, używając funkcji ``randrange(n)`` modułu
random, która zwraca losową liczbę z zakresu od 0 do \\(n−1\\).

**Ćwiczenie 3.5**

Rozważmy implementację tablicy dynamicznej, ale zamiast kopiować
elementy do tablicy o podwójnej wielkości (czyli od \\(N\\) do \\(2N)\\) po
osiągnięciu jej pojemności, kopiujemy elementy do tablicy tablica z
\\(N/4\\) dodatkowymi komórkami, przechodząc od pojemności \\(N\\) do pojemności
\\(N+N/4\\). Udowodnij, że wykonywanie sekwencji operacji dopisywania \\(n\\)
nadal działa w czasie \\(O(n)\\).

**Ćwiczenie 3.6**

Biorąc pod uwagę następujący fragment kodu dla klasy ``DynamicArray``,
zaimplementuj i dodaj metodę ``pop``, która usuwa ostatni element tablicy
i zmniejsza pojemność \\(N\\) tablicy o połowę w dowolnym momencie liczby
elementów w tablicy znajdującej się poniżej \\(N/4\\).

.. activecode:: insert-for-dynamic-array-2
   :language: python
   :caption: Insert a value at position k in a dynamic array

   import ctypes					# provides low-level arrays

   class DynamicArray:
     def  __init__ (self):
       self._n = 0 				    # number of elements
       self._capacity = 1 			# initial capacity
       self._A = self._make_array(self._capacity) 			# define low-level array

     def __len__ (self):
       return self._n

     def __getitem__ (self, k):
       if not 0 <= k < self._n:
         raise IndexError('invalid index')
       return self._A[k]

     def append(self, obj):
       if self._n == self._capacity:			# full array
         self._resize(2*self._capacity) 		# double capacity
       self._A[self._n] = obj
       self._n += 1

     def _resize(self, c):
       B = self._make_array(c) 			  # new array
       for k in range(self._n):
         B[k] = self._A[k]
       self._A = B 						  # A is the new array
       self._capacity = c

     def _make_array(self, c):
       return (c*ctypes.py_object)( )


**Ćwiczenie 3.7**

Udowodnij, że podczas używania tablicy dynamicznej, która rośnie i
zmniejsza się, jak w poprzednim ćwiczeniu, następująca seria operacji
\\(2n\\) zajmuje \\(O(n)\\) czas: \\(n\\) operacje dołączania na początkowo pustej
tablicy , po którym następują operacje pop \\(n\\).

**Ćwiczenie 3.8**

Składnia Pythona ``data.remove(value)`` usuwa pierwsze wystąpienie wartości ``value``
elementu z danych ``data`` listy. Podaj implementację funkcji ``removeall(data, value)``, która usuwa wszystkie wystąpienia wartości ``value`` z podanej listy, tak,
że najgorszy czas działania funkcji to \\(O(n)\\) na liście z \\(n\\)
elementów.

**Ćwiczenie 3.9**

Niech \\(B\\) będzie tablicą o rozmiarze \\(n \geq 6\\) zawierającą
liczby całkowite od 1 do \\(n−5\\), z dokładnie pięcioma powtórzonymi. Opisz
algorytm znajdowania pięciu liczb całkowitych w \\(B\\), które się
powtarzają.

**Ćwiczenie 3.10**

Opisz sposób użycia rekurencji w celu dodania wszystkich liczb w zbiorze
danych \\(n \times n\\), reprezentowanym jako lista list.

**Ćwiczenie 3.11**

Napisz program w Pythonie dla klasy macierzowej, która potrafi dodawać i
mnożyć dwuwymiarowe tablice liczb, zakładając, że wymiary się zgadzają.

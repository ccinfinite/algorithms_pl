Rozdział 3: Sekwencje oparte na tablicach
*****************************************

Podrodział 1: Wprowadzenie - typy sekwencji w Pythonie
------------------------------------------------------

Wbudowane klasy sekwencji Pythona to list, krotka i *str*. Każdy z nich
obsługuje indeksowanie w celu uzyskania dostępu do elementu \\(k\\)
sekwencji przy użyciu składni takiej jak ``seq[k]``. Podzielają również
niskopoziomową koncepcję tablicy w celu reprezentacji. Różnią się one
tym, jak instancje tych klas są reprezentowane wewnętrznie przez
Pythona, jak zobaczymy w dalszej części tego rozdziału.

Chociaż wystarczy zrozumieć składnię i semantykę publicznego interfejsu
klasy, aby napisać poprawny kod, ważne jest, aby mieć pewien wgląd w
implementację tej samej klasy, w celu osiągnięcia dobrego poziomu
wydajności programów. Podstawowe użycie list, łańcuchów i krotek jest w
pewien sposób podobne, ale kilka szczegółów może wpłynąć na zachowanie i
wydajność programów, jeśli nie zostaną docenione.

Pozdrozdział 2: Tablice niskiego poziomu
----------------------------------------

Pamięć podstawowa komputera składa się z bitów informacji, które są
pogrupowane w większe jednostki w sposób zależny od architektury
systemu. Typową jednostką jest bajt, który odpowiada 8 bitom. Aby
śledzić informacje przechowywane w bajtach, używany jest adres pamięci:
każdy bajt jest powiązany z unikalną liczbą binarną, która służy jako
jego adres. Adresy pamięci są skoordynowane z fizycznym układem systemu
pamięci, dlatego są wyświetlane w sposób sekwencyjny. Pomimo tej
sekwencyjnej natury, każdy bajt pamięci głównej może być skutecznie
dostępny przy użyciu jego adresu pamięci. Oznacza to, że teoretycznie
każdy bajt pamięci może być przechowywany lub pobierany w czasie (O(1)).
Główna pamięć komputera działa jak pamięć o dostępie swobodnym (RAM).

Typowym zadaniem programistycznym jest śledzenie sekwencji powiązanych
obiektów. W tym przypadku moglibyśmy użyć wielu różnych zmiennych lub
użyć jednej nazwy dla grupy i numerów indeksu, aby odnosić się do
elementów w tej grupie. Osiąga się to poprzez przechowywanie zmiennych
jedna po drugiej w ciągłej części pamięci komputera i oznaczanie takiej
reprezentacji jako tablicę. Na przykład ciąg tekstowy „STRING” jest
przechowywany jako uporządkowany ciąg sześciu znaków. Każdy znak jest
reprezentowany przy użyciu zestawu znaków Unicode, a każdy znak Unicode
jest reprezentowany przez 16 bitów (tj. 2 bajty). Dlatego sześcioznakowy
ciąg będzie przechowywany w 12 kolejnych bajtach pamięci.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura31.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Sekwencję tę nazywamy tablicą sześciu znaków. Każda lokalizacja w
tablicy nazywana jest komórką, a do opisania jej położenia w tablicy
używamy indeksu zwiększającego liczbę całkowitą, zaczynając od 0 (na
przykład komórka poprzedniej tablicy z indeksem 2 zawiera znak R).

Każda komórka tablicy musi używać tej samej liczby bajtów. Pozwala nam
to na dostęp do dowolnej komórki tablicy w stałym czasie. Jeśli znamy
adres pamięci, pod którym zaczyna się tablica, liczbę bajtów na element
i pożądany indeks w tablicy, adres pamięci elementu to *start + rozmiar
komórki x indeks*. To obliczenie można wykonać na niskim poziomie
abstrakcji, będącym całkowicie transparentnym dla programisty.

Podrozdział 2.1: Tablice referencyjne
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Rozważmy strukturę opartą na tablicach, która z jakiegoś powodu
zachowuje nazwy niektórych kolorów. Na przykład w Pythonie możemy użyć
listy nazw, takich jak: ``[ White , Blue , Yellow , Red , Black ]``.
Elementami listy są ciągi o różnych długościach. Stoi to w sprzeczności
z wymogiem Pythona, że każda komórka tablicy musi używać tej samej
liczby bajtów. Pierwszym rozwiązaniem mogłoby być zarezerwowanie
wystarczającej ilości miejsca dla każdej komórki, aby pomieścić ciąg o
maksymalnej długości. Jest to wyraźnie nieefektywne (dużo pamięci jest
marnowane) i nie jest ogólne (nie wiemy, czy będzie inna nazwa dłuższa
niż te, które są już na liście).

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura32.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Bardziej wydajnym rozwiązaniem jest reprezentowanie instancji listy za
pomocą tablicy odniesień do obiektów: przechowywana jest kolejna
sekwencja adresów pamięci, z których każdy odnosi się do elementu
sekwencji. Zauważ, że nawet jeśli rozmiar każdego elementu może się
różnić, liczba bitów używanych do przechowywania adresu pamięci każdego
elementu jest stała. Umożliwia to stały dostęp do listy elementów na
podstawie jej indeksu.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura33.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Z uwagi na fakt, że w Phytonie listy są strukturami referencyjnymi,
instancja listy może zawierać wiele odniesień do tego samego obiektu, co
elementy listy lub pojedynczy obiekt może być elementem dwóch list. Na
przykład lista ``primes=[2,3,5,7,11,13,17]`` jest listą odniesień
do liczb, a nie listą liczb. Polecenie ``temp = primes[3:6]`` zwraca nową
instancję listy ``[7,11,13]``, a ta nowa lista ma odniesienia do tych samych
elementów, które są na oryginalnej liście.


.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura34.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Gdy elementy listy są niezmiennymi obiektami, żadna z list nie może
spowodować zmiany w udostępnionym obiekcie. Na przykład polecenie
``temp[2] = 15`` nie zmienia istniejącego obiektu liczb całkowitych 13.
Zamiast tego zmienia odwołanie w komórce 2 listy tymczasowej ``temp`` na nowe
odwołanie do innego obiektu.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura35.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

To samo dzieje się podczas tworzenia nowej listy jako kopii istniejącej,
za pomocą polecenia ``backup = list(primes)``. Daje to nową listę, która
jest płytką kopią, ponieważ odwołuje się do tych samych elementów, co na
pierwszej liście. Jeśli zawartość listy jest typu mutowalnego, "głęboka
kopia" (nowa lista z nowymi elementami) może zostać utworzona za pomocą
funkcji ``deepcopy`` z modułu ``copy``.

Podrozdział 2.2: Tablice kompaktowe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

W przeciwieństwie do tablic referencyjnych, tablice kompaktowe
przechowują bity reprezentujące dane bezpośrednio w strukturze. Na
przykład łańcuchy są reprezentowane w Phytonie jako tablice znaków, a
nie jako tablice odniesień do znaków. To rozwiązanie ma pewną przewagę
obliczeniową nad tablicami referencyjnymi. Struktura referencyjna
wykorzystuje 64-bity dla każdego adresu pamięci przechowywanego w
tablicy, a także liczbę bitów używanych do reprezentowania rzeczywistych
elementów. Co więcej, w zwartej tablicy dane są przechowywane kolejno w
pamięci, co nie ma miejsca w przypadku struktury referencyjnej, której
elementy znajdują się w częściach pamięci, które nie są kolejne. Dla
wydajnych obliczeń korzystne jest przechowywanie danych w pamięci w
pobliżu innych danych, które mogą być użyte w tych samych obliczeniach.

Podstawowa obsługa tworzenia kompaktowych tablic znajduje się w module
tablicy ``array``. Ten moduł definiuje tablicę klas ``array``, zapewniającą kompaktową
pamięć masową dla tablic pierwotnych typów danych. Zwarta tablica liczb
pierwszych jest deklarowana jako ``primes = array( i , [2, 3, 5, 7, 11, 13, 17, 19])``, gdzie “i” reprezentuje typ danych, które będą
przechowywane w tablicy, w tym przypadku to liczba całkowita. Dzięki
temu interpreter wie, ile bitów będzie potrzebnych do przechowywania
tablicy, podczas gdy interfejs klasy jest podobny do interfejsu listy.
Kody typów obsługiwane przez moduł tablicy są oparte na podstawowych
typach danych, takich jak ``char`` (signed lub unsigned), ``int`` (signed lub
unsigned) i ``float``.

Podrozdział 3: Tablica dynamiczna
---------------------------------

W poprzednim podrozdziale przedyskutowano, że aby utworzyć zwartą
tablicę, należy zadeklarować jej rozmiar. Kolejna część pamięci zostanie
przydzielona do przechowywania tych informacji. Jeśli chcemy dynamicznie
zwiększyć pojemność tablicy, pierwszym rozwiązaniem jest dodanie komórek
do wcześniej zdefiniowanej tablicy. Mie można tego łatwo zrobić,
ponieważ sąsiednie miejsca w pamięci mogą być już zajęte przez inne
informacje.

Rozważmy na przykład klasę ``list`` Pythona. Lista ma określoną długość, gdy
jest zdefiniowana, ale możemy dodawać elementy do listy w
nieskończoność. Jest to realizowane poprzez implementację listy za
pomocą tablicy dynamicznej. Instancja listy utrzymuje podstawową
tablicę, która ma większą pojemność niż bieżąca długość listy. Na
przykład, podczas gdy rzeczywista lista zawiera sześć elementów, system
tworzy podstawową tablicę, która przechowuje osiem lub więcej odniesień
do obiektów. Nowy element można dołączyć do listy za pomocą następnej
dostępnej komórki tablicy. Jednak ta dodatkowa pojemność nie wystarczy,
jeśli użytkownik stale dodaje elementy do listy. W takim przypadku
system musi dostarczyć większą tablicę, inicjując nową tablicę tak, aby
jej prefiks był zgodny z istniejącą mniejszą tablicą. Poniżej
pokazujemy, że klasa ``list`` Pythona jest oparta na tej strategii::

 1  import sys           # includes the function getsizeof
 2  data = [ ] 			 # empty list
 3  for k in range(n):
 4    a = len(data) 			# a is the number of elements
 5    b = sys.getsizeof(data) 	# size of data in bytes
 6    print( Length: {0:3d}; Size in bytes: {1:4d} .format(a, b))
 7    data.append(None) 		# increase length by one

 Length: 0; Size in bytes : 72
 Length: 1; Size in bytes : 104
 Length: 2; Size in bytes : 104
 Length: 3; Size in bytes : 104
 Length: 4; Size in bytes : 104
 Length: 5; Size in bytes : 136
 Length: 6; Size in bytes : 136
 Length: 7; Size in bytes : 136
 Length: 8; Size in bytes : 136
 Length: 9; Size in bytes : 200
 Length: 10; Size in bytes : 200
 Length: 11; Size in bytes : 200
 Length: 12; Size in bytes : 200
 Length: 13; Size in bytes : 200
 Length: 14; Size in bytes : 200
 Length: 15; Size in bytes : 200
 Length: 16; Size in bytes : 200
 Length: 17; Size in bytes : 272
 Length: 18; Size in bytes : 272
 Length: 19; Size in bytes : 272
 Length: 20; Size in bytes : 272
 Length: 21; Size in bytes : 272
 Length: 22; Size in bytes : 272
 Length: 23; Size in bytes : 272
 Length: 24; Size in bytes : 272
 Length: 25; Size in bytes : 272
 Length: 26; Size in bytes : 352

Zauważ, że pusta lista wymaga już pewnej liczby bajtów pamięci (w tym
przypadku 72). Dzieje się tak, ponieważ każdy obiekt w Pythonie (jako
instancja naszej listy) przechowuje pewne prywatne zmienne instancji,
takie jak liczba elementów aktualnie przechowywanych na liście,
maksymalna liczba elementów, które mogą być przechowywane w tablicy oraz
odniesienie do aktualnie przydzielona tablica (początkowo Brak). Jak
tylko pierwszy element zostanie wstawiony do listy, liczba bajtów skacze
z 72 do 104, biorąc pod uwagę przydział tablicy zdolnej do
przechowywania czterech odniesień do obiektów. Po dodaniu piątego
elementu do listy użycie pamięci skacze ze 104 bajtów do 136 bajtów, co
oznacza, że lista może zawierać do ośmiu odniesień do obiektów. Po
dziewiątym i siedemnastym wstawieniu rozmiar listy ponownie się
zwiększa.

Ponieważ lista jest strukturą referencyjną, wynik ``getsizeof`` dla
instancji listy zawiera tylko rozmiar reprezentujący jej strukturę
podstawową. Nie uwzględnia pamięci używanej przez obiekty będące
elementami listy. W naszym eksperymencie wielokrotnie dodajemy ``None`` do
listy, ponieważ nie dbamy o zawartość, ale możemy dołączyć dowolny typ
obiektu bez wpływu na liczbę bajtów raportowanych przez
``getsizeof(data)``.

Podrozdział 3.1: Wdrożenie
~~~~~~~~~~~~~~~~~~~~~~~~~~

W tej sekcji przedstawiamy implementację klasy ``DynamicArray``. Mając
tablicę A, która przechowuje elementy listy, pokażemy, jak dodać element
po osiągnięciu maksymalnego rozmiaru tablicy, wykonując cztery
następujące kroki::

 define a new array B, with double capacity;

 B[i] = A[i] (for i = 0,… n−1), with n the number of current items;

 A = B, , that is, use B as the array supporting the list;

 add the new element in the new array.

Python obsługuje tworzenie ``list`` z listą klas. Zapewniamy alternatywną
implementację w poniższym kodzie::

 1  import ctypes					# provides low-level arrays
 2
 3  class DynamicArray:
 4
 5    def  __init__ (self):
 6      self._n = 0 				# number of elements
 7      self._capacity = 1 			# initial capacity
 8      self._A = self._make_array(self._capacity) 			# define low-level array
 9
 10   def __len__ (self):
 11     return self._n
 12
 13   def __getitem__ (self, k):
 14     if not 0 <= k < self._n:
 15       raise IndexError('invalid index')
 16     return self._A[k]
 17
 18   def append(self, obj):
 19     if self._n == self._capacity:			# full array
 20       self._resize(2*self._capacity) 		# double capacity
 21     self._A[self._n] = obj
 22     self._n += 1
 23
 24   def _resize(self, c):
 25     B = self._make_array(c) 			# new array
 26     for k in range(self._n):
 27       B[k] = self._A[k]
 28     self._A = B 						# A is the new array
 29     self._capacity = c
 30
 31   def _make_array(self, c):
 32     return (c*ctypes.py_object)( ) # see ctypes documentation

W tym kodzie wykorzystujemy funkcjonalności ``append``: ``__len__`` i ``__getitem__``. Tworzymy nową tablicę niskopoziomową za pomocą modułu ``ctypes``.

Podrozdział 3.2: Analiza tablic dynamicznych
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

W tej sekcji analizujemy czas wykonywania operacji na tablicach
dynamicznych, korzystając z techniki zwanej amortyzacją algorytmiczną,
aby pokazać, że wprowadzona wcześniej strategia jest wystarczająco
wydajna.

Zastąpienie pełnej tablicy nową, większą tablicą wymaga czasu
\\(Omega(n)\\) do wykonania, z \\(n\\) bieżącą liczbą elementów w
tablicy. Dzieje się tak, gdy próbujemy dołączyć nowy element do tablicy,
która jest już pełna. Po podwojeniu pojemności tablicy możemy dołączyć
\\(n\\) nowe elementy do tablicy, zanim będzie trzeba ją ponownie wymienić.
Tak więc istnieje wiele prostych operacji dołączania dla każdej drogiej.

Aby pokazać skuteczność tej strategii, wyobrażamy sobie, że za stałą
ilość czasu obliczeniowego trzeba zapłacić monetą. Kiedy trzeba było
wykonać operację, musimy mieć wystarczającą ilość monet, aby zapłacić za
czas jej działania. Całkowita ilość monet wydanych na dowolne obliczenia
jest proporcjonalna do całkowitego czasu spędzonego na tym obliczeniu.

Udowadniamy, co następuje:

**Propozycja**

Niech \\(S\\) będzie sekwencją zaimplementowaną za pomocą tablicy
dynamicznej o pojemności początkowej równej 1 i podwojeniu rozmiaru
tablicy, gdy jest pełna. Całkowity czas wymagany do wykonania \\(n\\)
operacji dołączania w \\(S\\) wynosi \\(O(n)\\).

**Dowód**

Załóżmy, że jedna moneta to koszt każdej operacji dopisywania i załóżmy,
że podwojenie tablicy rozmiaru \\(k\\) (w razie potrzeby) kosztuje \\(k\\)
monet. Technika amortyzacji pozwala nam przeciążać niektóre proste
operacje, aby zapłacić za inne, które są droższe. Za każdą operację
dołączania naliczamy trzy monety (o dwie monety więcej niż rzeczywisty
koszt operacji), jeśli nie powoduje to przepełnienia tablicy. Dodatkowe
monety wykorzystamy na opłacenie kosztów operacji podwojenia.
Przepełnienie występuje, gdy rozmiar tablicy \\(S\\) ma elementy \\(2^i\\), dla
\\(i \geq 0\\). Podwojenie rozmiaru tablicy wymaga \\(2^i\\) monet.
Monety te zostały zapisane podczas poprzednich operacji dopisywania od
komórki \\(2^{i−1}\\) do \\(2^i −1\\) i mogą zostać użyte do opłacenia
rozszerzenia tablicy. Nasz schemat amortyzacji, w którym każda operacja
jest obciążona trzema monetami, pozwala nam zapłacić za wykonanie \\(n\\)
operacji append za pomocą \\(3n\\) monet. Zamortyzowany czas wykonywania
każdej operacji dołączania wynosi \\(O(1)\\), a całkowity czas wykonywania
\\(n\\) operacji dołączania wynosi \\(O(n)\\).

Zauważ, że kluczem do poprzedniej wydajności liniowej jest to, że
przestrzeń, którą dodajemy podczas rozszerzania tablicy, jest zawsze
proporcjonalna do bieżącego rozmiaru samej tablicy. W tym przypadku
rozmiar tablicy jest podwajany, gdy jest to konieczne. Moglibyśmy
zdecydować się na zwiększenie rozmiaru tablicy o, powiedzmy, 30% lub
200%. Jeśli rozszerzenie tablicy następuje zgodnie z takim wzrostem
geometrycznym, można udowodnić, że poprzedni zamortyzowany limit czasowy
jest nadal ważny, przy użyciu stałej liczby monet na każdą operację.

Używanie postępu arytmetycznego zamiast geometrycznego daje kwadratowy
koszt całkowity. Na przykład sytuacja, w której chcemy dodać jedną
komórkę za każdym razem, gdy trzeba zmienić rozmiar tablicy, prowadzi to
do zmiany rozmiaru dla każdej operacji dołączania, a to wymaga operacji
\\(1+2+…+n\\), czyli \\(Omega`(n^2)\\).

Ogólnie udowodnimy, że:

**Założenie**

Niech \\(S\\) będzie sekwencją zaimplementowaną za pomocą tablicy
dynamicznej o pojemności początkowej równej 1 i przy użyciu stałego
przyrostu przy każdej zmianie rozmiaru. Całkowity czas wymagany do
wykonania \\(n\\) operacji dołączania w \\(S\\) wynosi \\(O(n^2)\\).

**Dowód**

Niech \\(c > 0\\) będzie stałą liczbą komórek dodanych do tablicy dla każdej
zmiany rozmiaru. Podczas operacji dopisywania \\(n\\) potrzebny jest czas na
zainicjowanie tablic o rozmiarze \\(c\\), \\(2c\\), \\(3c\\), …, \\(mc\\), dla \\(m = n/c\\). 
Całkowity czas jest proporcjonalny do \\(c+2c+3c+ \ldots +mc\\), a suma ta wynosi \\(Omega(n^2)\\).

Wreszcie, geometryczny wzrost pojemności podczas zmiany rozmiaru tablicy
pokazuje interesującą właściwość struktury danych: ostateczny rozmiar
tablicy na końcu operacji \\(n\\) jest proporcjonalny do \\(O(n)\\). Ogólnie
rzecz biorąc, każdy kontener, który udostępnia operacje powodujące
dodanie lub usunięcie jednego lub więcej elementów, wymaga użycia
pamięci \\(O(n)\\). Wielokrotne dodawanie i odejmowanie elementu może
spowodować, że tablica rozrośnie się dowolnie, bez proporcjonalnej
relacji między rzeczywistą liczbą elementów a pojemnością tablicy po
usunięciu wielu elementów. Solidna implementacja takiej struktury danych
musi w miarę możliwości zmniejszać macierz, przy jednoczesnym zachowaniu
stałego zamortyzowanego ograniczenia poszczególnych operacji.

Podrozdział 4: Wydajność typów sekwencji Pythona: list, tuple i string
----------------------------------------------------------------------

W tym podrodziale najpierw rozważymy zachowanie krotek Pythona i list
niemutujących, następnie przeanalizujemy, co dzieje się z listami
mutacji.

Podrozdział 4.1: Niemutujące listy i klasy krotek w Pythonie
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Niemutujące metody klasy ``list`` i klasy ``tuple`` są takie same. Krotki i
listy niezmienne mają taką samą wydajność, ponieważ nie ma potrzeby
stosowania bazowej tablicy dynamicznej. Poniżej podsumowujemy koszt
niektórych operacji na instancjach tych klas.

**Operacje o stałym czasie**

Długość instancji listy lub krotki ``len(data)`` jest zwracana w stałym
czasie, ponieważ każda instancja jawnie przechowuje takie informacje.
Bezpośredni dostęp do elementu ``data[j]`` wymaga stałego czasu ze względu
na podstawowy dostęp do tablicy.

**Wyszukiwanie wystąpień wartości**

``data.count(value)``, ``data.index(value)`` i ``value`` w danych wymaga iteracji w
sekwencji od lewej do prawej. Zauważ, że iteracja dla ``count`` musi
przebiegać przez całą sekwencję długości \\(n\\), co oznacza, że jej
złożoność czasowa wynosi \\(O(n)\\). Pozostałe dwie metody mogą zatrzymać
iterację, gdy tylko zostanie znaleziony indeks lub odpowiednio,
zawartość zostanie zweryfikowana, co oznacza, że złożoność wynosi
\\(O(k)\\), gdzie \\(k\\) jest indeksem wystąpień skrajnych po lewej stronie
wartości ``value``.

**Porównania sekwencji**

Porównania między dwiema sekwencjami są zdefiniowane leksykograficznie.
Wymaga to, w najgorszym przypadku, iteracji trwającej czas
proporcjonalnie do długości krótszej z dwóch sekwencji, a więc \\(O(n)\\).

**Tworzenie nowych instancji**

Czas wykonywania operacji, takich jak wycinanie sekwencji ``data[a:b]`` lub
łączenie dwóch sekwencji ``data1+data2`` jest proporcjonalny do długości
wyniku.

Podrozdział 4.2: Python’s mutating List
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Najprostszą operacją mutacji na listach jest ``data[j] = val``, z oczywistą
sematyką. Jest on wspierany przez metodę ``__setitem__``, z najgorszym
przypadkiem złożoności czasowej \\(O(1)\\), ponieważ po prostu zastępuje
jeden element listy nowym. Poniżej pokazujemy złożoność tych metod,
które dodają lub usuwają elementy z listy.

**Dodawanie elementów do listy**

Metoda ``append`` wymaga \\(Omega(n)\\), w najgorszym przypadku,
gdy potrzebna jest zmiana rozmiaru tablicy. W amortyzowanym schemacie
złożoności używa czasu \\(O(1)\\). Inną metodą obsługiwaną przez klasę ``list``
jest ``insert(k, value)``, która wstawia do listy podaną wartość ``value`` pod
indeksem ``k``, jednocześnie przesuwając wszystkie kolejne elementy w prawo.
Implementacja tej metody to::

 1  def insert(self, k, value):
 2
 3    if self._n == self._capacity: 		# not enough room, resize the array
 4      self._resize(2*self._capacity)
 5    for j in range(self._n, k, −1): 		# shift to right, rightmost first
 6      self._A[j] = self._A[j−1]
 7    self._A[k] = value
 8    self._n += 1

Pamiętaj, że dodanie jednego elementu może wymagać zmiany rozmiaru
tablicy. W najgorszym przypadku kosztuje to czas liniowy, ale kosztuje
też \\(O(1)\\) zamortyzowanego czasu. Przesunięcie elementów \\(k\\) w celu
zrobienia miejsca na nowy element kosztuje amortyzowany czas \\(O(n−k+1)\\).
Oceniając średni czas na operację, widzimy, że wstawianie na początku
listy jest najdroższe, ponieważ wymaga liniowego czasu na operację.
Wstawianie w środku wymaga około połowy czasu wstawiania na początku,
ale nadal jest czasem liniowym. Wstawianie na końcu wymaga stałego
czasu.

**Usuwanie elementów z listy**

Najłatwiejszym sposobem usunięcia elementu z instancji klasy ``list`` w
Pythonie jest metoda ``pop()``, która usuwa ostatni element z listy. Jego
wykonanie wymaga \\(O(1)\\) zamortyzowanego czasu, biorąc pod uwagę, że
wszystkie elementy pozostają w oryginalnej komórce podstawowej tablicy
dynamicznej, z wyjątkiem okazjonalnego zmniejszania samej tablicy.
Metoda ``pop(k)`` usuwa \\(k\\)-ty element listy i przesuwa wszystkie kolejne
elementy w lewo. Wymaga to złożoności \\(O(n−k)\\)

Metoda ``remove(value)`` usuwa pierwsze wystąpienie ``value`` z listy. Wymaga
pełnego przeskanowania listy, najpierw wyszukania wartości, a następnie
przesunięcia wszystkich pozostałych elementów w lewo::

 1  def remove(self, value):
 2    for k in range(self._n):
 3      if self._A[k] == value:
 4        for j in range(k, self._n−1):
 5          self._A[j] = self._A[j+1]
 6        self._A[self._n −1] = None
 7        self._n −= 1
 8        return 					# exit immediately
 9    raise ValueError('value not found')


**Rozszerzanie listy**

Metoda ``extend`` służy do dodawania wszystkich elementów jednej listy na
koniec drugiej listy, z wywołaniem ``first.extend(second)``. Czas działania
jest proporcjonalny do długości drugiej listy ``second`` i jest amortyzowany,
ponieważ tablica reprezentująca pierwszą ``first`` może ulec zmianie.

**Tworzenie nowych list**

Python oferuje kilka sposobów tworzenia nowych list. W prawie wszystkich
przypadkach wymóg czasowy jest liniowy w stosunku do długości tworzonej
listy. Na przykład często tworzy się listę za pomocą operatora mnożenia,
jak w ``[0] * n``, który tworzy listę o długości n ze wszystkimi
wartościami 0.

Podrozdział 5: Wybrane algorytmy “on Python’s strings”
------------------------------------------------------

W tym podrodziale przeanalizujemy zachowanie niektórych metod i
niektórych dobrze znanych algorytmów na ciągach. Niech (n) i (m)
oznaczają długość napisów. Wydaje się naturalne, że ocenianie jako
liniowej (w długości łancucha) złożoności tych metod, które tworzą nowy
łancuchy. Również metody testujące warunki logiczne łańcucha czy
operatory porównania, zajmują czas (O(n)), ponieważ muszą sprawdzać
wszystkie znaki (n) w najgorszym przypadku, a zwierać jak tylko
odpowiedź zostanie znaleziona (np. ``islower`` zwraca ``False``, jeśli pierwszy
znak jest pisany wielką literą). To samo dzieje się z operatorami
porównania, takimi jak ``==`` lub ``<=``.

Podrozdział 5.1: Dopasowywanie wzorów
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

W klasycznym zadaniu dopasowywania wzorców otrzymujemy łańcuch tekstowy
\\(T\\) o długości \\(n\\) i łańcuch wzorcowy \\(P\\) o długości \\(m\\), i chcemy
sprawdzić czy \\(P\\) jest podciągiem \\(T\\). Rozwiązania tego problemu
wykazują kilka interesujących zachowań z algorytmicznego punktu
widzenia. Implementacja brute-force działa w czasie \\(O(mn)\\), ponieważ
uwzględnia \\(n−m+1\\) możliwe indeksy początkowe dla wzorca i spędza czas
\\(O(m)\\) przy każdym pozycja początkowa, sprawdzanie czy wzór pasuje.
Bardziej wyrafinowane rozwiązania działają w czasie \\(O(n)\\).

Formalne rozwiązanie problemu polega na znalezieniu najniższego indeksu
\\(j\\) w obrębie \\(T\\), od którego zaczyna się \\(P\\), taki, że \\(T[j:j+m]\\) równa
się \\(P\\). Problem dopasowywania wzorców jest powiązany z wieloma
zachowaniami klasy ``str`` Pythona, takimi jak ``in``, ``find``, ``index``,
``count`` i jest podzadaniem innych metod, takich jak ``partition``, ``split``
i ``replace``.

**Pierwsze rozwiązanie: Brute Force**

Ogólnie rzecz biorąc, brute-force polega na wyliczeniu wszystkich
możliwych konfiguracji zaangażowanych danych wejściowych i wybraniu
najlepszej ze wszystkich, zgodnie z pewnym rodzajem miary. Łatwo sobie
wyobrazić, że wyliczanie i wyszukiwanie wiąże się z nieoptymalną
konsumpcją czasu. Dlatego szukamy wszystkich możliwych rozmieszczeń \\(P\\)
w \\(T\\), zwracając najniższy indeks \\(T\\), od którego zaczyna się podłańcuch
\\(P\\) \\(lub -1\\), w następujący sposób::

 1  def find brute(T, P):
 2
 3    n, m = len(T), len(P)
 4    for i in range(n−m+1):
 5      k = 0
 6      while k < m and T[i + k] == P[k]:
 7        k += 1
 8      if k == m:			# we reached the end of P,
 9        return i 			# substring T[i:i+m] matches P
 10     return −1 			# no match


Poprzedni algorytm składa się z dwóch zagnieżdżonych pętli: zewnętrznej
pętli indeksującej wszystkie możliwe indeksy początkowe wzorca w tekście
\\(T\\); wewnętrznej pętli, która indeksuje każdy znak wzorca \\(P\\),
porównując go z odpowiednim znakiem w tekście. Oznacza to, że dla
każdego indeksu kandydującego w T wykonujemy do \\(m\\) porównania znaków -
stąd w najgorszym przypadku wykonujemy operacje \\(O(mn)\\).

**Drugie rozwiązanie: algorytm Boyera-Moore’a**

Algorytm dopasowywania wzorców Boyera-Moore’a pozwala uniknąć porównań
między \\(P\\) a częścią znaków w \\(T\\). Oznacza to, że nie musimy, jak w
podejściu brute-force, skanować każdego znaku \\(T\\) w celu znalezienia
dopasowania.

Dwa względy pozwalają nam napisać ten algorytm: *looking-glass
heuristic* oraz *character-jump heuristic*. Z pierwszym zaczynamy
porównanie od końca \\(P\\), przesuwając się wstecz do przodu. Jeśli
napotkamy niezgodność w określonym miejscu w T, możemy uniknąć
wszystkich pozostałych porównań, przesuwając \\(P\\) względem \\(T\\) przy
użyciu *character-jump heuristic*. W rzeczywistości, jeśli niezgodność
występuje między znakiem tekstowym \\(T[i]=c\\) a znakiem wzorca \\(P[k]\\),
mamy dwa przypadki: jeśli \\(c\\) nie należy do \\(P\\), to przesuwamy \\(P\\)
całkowicie poza \\(T[i]\\), w przeciwnym razie przesuwamy \\(P\\), aż
wystąpienie znaku \\(c\\) w P zostanie wyrównane z \\(T[i]\\). W obu przypadkach
pewna liczba porównań nie jest wykonywana.

Dokładniej, załóżmy, że znaleziono dopasowanie dla ostatniego znaku \\(P\\):
algorytm próbuje znaleźć dopasowanie dla przedostatniego znaku wzorca i
tak dalej, aż do znalezienia pełnego dopasowania lub pojawia się
niezgodność w jakiejś pozycji wzorca. Ponownie, jeśli zostanie
znaleziona niezgodność, a niezgodny znak tekstu nie występuje we wzorcu,
przesuwamy cały wzorzec poza tę lokalizację. Jeśli niezgodny znak
występuje w innym miejscu we wzorcu, musimy rozważyć dwa możliwe
podprzypadki w zależności od tego, czy jego ostatnie wystąpienie jest
(1) przed czy (2) po znaku wzorca, który został wyrównany z niezgodnym
znakiem.

Niech \\(i\\) reprezentuje indeks niedopasowanego znaku w tekście, \\(k\\)
reprezentuje odpowiedni indeks we wzorcu, a \\(j\\) reprezentuje indeks
ostatniego wystąpienia \\(T[i]\\) we wzorcu. Jeżeli (1) \\(j < k\\), przesuwamy
wzorzec o \\(k − j\\) jednostek, a zatem indeks \\(i\\) zwiększa się o \\(m−(j+1)\\). Jeśli (2) \\(j > k\\), przesuwamy wzorzec o jedną jednostkę, a indeks
\\(i\\) zwiększa się o \\(m−k\\). Poniżej przedstawiamy implementację algorytmu
w Pythonie::

 1  def boyer_moore(T, P):
 2	  # returns the index of the character in T where P begins, if it exists
 3    n, m = len(T), len(P)
 4    if m == 0: return 0
 5    last = { } 						# build ’last’ dictionary
 6    for k in range(m):
 7      last[ P[k] ] = k 				# later occurrence overwrites
 8
 9    i = m−1 			# index of T
 10   k = m−1 			# index of P
 11   while i < n:
 12     if T[i] == P[k]: 	# match
 13       if k == 0:
 14         return i 		# the pattern begins at index i of text
 15       else:
 16         i −= 1
 17         k −= 1
 18     else:				#no match
 19       j = last.get(T[i], −1) 		# last(T[i]) is -1 if not found
 20       i += m − min(k, j + 1) 		# case analysis for jump step
 21       k = m − 1 					# restart at the end of pattern
 22   return −1

Wydajność algorytmu opiera się na możliwości stworzenia funkcji ``last(c)``,
która zwraca indeks najbardziej prawego wystąpienia \\(c\\) w \\(P\\) (-1, jeśli
\\(c\\) nie jest w \\(P)\\). Jeśli alfabet ma skończony rozmiar, a znaki mogą
być używane jako indeksy tablicy, ``last(c)`` może być zaimplementowana jako
tablica przeglądowa o stałym koszcie czasu, zawierająca znaki wzorca.
Najgorszy czas działania algorytmu Boyera-Moore’a to
\\(O(nm+Sigma)\\). Ostatnia funkcja wykorzystuje czas
\\(O(nm+Sigma)\\), a poszukiwanie wzorca w najgorszym
przypadku zajmuje \\(O(nm)\\) czas, taki sam jak poprzedni algorytm
brute-force. Algorytm Boyera-Moore’a jest w stanie pominąć duże
fragmenty tekstu, a dowody eksperymentalne pokazują, że średnia liczba
porównań wykonywanych na postać wynosi jedną czwartą liczby porównań
stosowanych w przypadku algorytmu brute-force.

Bardziej wyrafinowane heurystyki osiągają czas działania \\(O(n+m+|Σ|)\\),
jak na przykład w algorytmie dopasowywania wzorców
Knutha-Morrisa-Pratta.

Podrodział 5.2: Komponowanie ciągów
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Załóżmy, że mamy ciąg o nazwie ``document`` i chcemy utworzyć ciąg``letters``
zawierający znaki alfabetu oryginalnego ciągu. Pierwsze rozwiązanie to::

 letters = ''
 for c in document:
   if c.isalpha( ):
     letters += c


Każdy znak w dokumencie dodajemy do ``letters`` tylko wtedy, gdy jest
alfabetyczny. Nawet jeśli rozwiązanie wydaje się całkiem naturalne, jest
wysoce nieefektywne. Ciąg jest niezmienny, co oznacza, że za każdym
razem, gdy dodajemy literę, musimy rozszerzyć listę. Widzieliśmy
wcześniej, że wymaga to czasu, który jest proporcjonalny do długości
listy. Aby uzyskać wynik końcowy ze znakami \\(n\\), seria konkatenacji
zajęłaby czas proporcjonalny do \\(1+2+3+ \ldots +n\\), czyli
\\(O(n^2)\\). Powodem, dla którego ``letters += c`` są nieefektywne, jest
to, że należy utworzyć nową instancję ciągu (oryginalny ciąg musi
pozostać niezmieniony, jeśli inna zmienna w programie odwołuje się do
tego ciągu). Ale jeśli nie ma innych odniesień do ciągu, polecenie może
być zaimplementowane wydajniej, na przykład przy użyciu tablicy
dynamicznej. Możemy wykryć czy nie ma innych odwołań do ciągu,
sprawdzając licznik odwołań - liczbę, która jest utrzymywana dla każdego
obiektu.

Innym rozwiązaniem jest zbudowanie tymczasowej listy do przechowywania
poszczególnych elementów, a następnie wykorzystanie metody ``join`` klasy
``str`` do skomponowania końcowego wyniku::

 temp = [ ]
 for c in document:
   if c.isalpha( ):
     temp.append(c)
 letters = ''.join(temp)


Wywołania do ``append`` wymagają czasu \\(O(n)\\), ponieważ jest co najwyżej
(n), a każde wywołanie kosztuje amortyzowany czas \\(O(1)\\). Wywołanie do
``join`` kosztuje liniowy czas w długości ``temp``.

Podrozdział 5.3: Sortowanie wyboru
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

W przypadku sekwencji opartej na tablicy algorytm sortowania przez wybór
porównuje pierwszy i drugi element tablicy. Jeśli drugi jest mniejszy
niż pierwszy, zamienia je. Następnie rozważa trzeci element tablicy,
zamieniając go w lewo, aż znajdzie się we właściwej kolejności z
pierwszymi dwoma elementami. Następnie rozważa czwarty element i
zamienia go w lewo, aż znajdzie się we właściwej kolejności z pierwszymi
trzema. Ten proces trwa do momentu posortowania tablicy::

 Algorithm InsertionSort(A):
   Input: an array A of n elements
   Output: the array A with elements rearranged in nondecreasing order
     for k from 1 to n − 1 do
       Insert A[k] at proper location within A[0], A[1], . . ., A[k].
           

Implementacja insert-sort w Pythonie używa zewnętrznej pętli nad każdym
elementem oraz wewnętrznej pętli, która umieszcza element we właściwej
lokalizacji w tablicy elementów znajdujących się po jego lewej stronie::

 1  def insertion sort(A):
 3    for k in range(1, len(A)):
 4      cur = A[k]
 5      j = k
 6      while j > 0 and A[j−1] > cur:
 7        A[j] = A[j−1]
 8        j −= 1
 9      A[j] = cur

Najgorszy czas wykonania to \\(O(n^2)\\), ale jeśli tablica jest prawie lub
idealnie posortowana, algorytmy działają w czasie \\(O(n)\\).

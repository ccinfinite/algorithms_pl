Rozdział 4: Stos, kolejki i dekolejki – ćwiczenia i wyzwania
************************************************************

Podrozdział 1: Przegląd teorii
------------------------------

**Ćwiczenie 1.1**

Uzupełnij i wykonaj następujący fragment kodu (który można znaleźć w
rozdziale czwartym), w którym stos ADT jest zaimplementowany za pomocą
listy Pythona. Pokaż kilka przykładów działania stosu, a następnie dodaj
klasę wyjątku ``Empty``, która jest wywoływana, gdy użytkownik wywoła ``pop``
lub ``top`` na pustym stosie.

.. activecode:: stack-as-a-list
   :language: python
   :caption: Implementation of a stack by means of a list

   class ArrayStack:
   # LIFO Stack implementation using a Python list as storage

     # Creates an empty stack
     def __ init __ (self):
       self._data = [ ]

     # Returns the number of elements in the stack
     def __ len __ (self):
       return len(self._data)

     # Returns True if the stack is empty
     def is_empty(self):
       return len(self._data) == 0

     # Adds element e to the top of the stack
     def push(self, e):
       self._data.append(e)

     # Returns (but don't remove) the element at the top of the stack
     # raising an exception if the stack is empty
     def top(self):
       if self.is_empty( ):
         raise Empty( Stack is empty )
       return self._data[−1]

     # Remove and return the element from the top of the stack
     # raising an exception if the stack is empty
     def pop(self):
       if self.is_empty( ):
         raise Empty( 'Stack is empty' )
       return self._data.pop( )

           # INSERT NEW CODE HERE

**Ćwiczenie 1.2**

Napisz prostą implementację kolejki ADT, dostosowując listę Pythona.
Pokaż kilka przykładów, jak działa kolejka (patrz Sekcja 2.1 Rozdziału
4, aby uzyskać kilka sugestii: kiedy wywoływana jest funkcja ``pop(0)`` w
celu usunięcia pierwszego elementu z listy, wszystkie pozostałe elementy
muszą zostać przesunięte w lewo).

.. activecode:: queue-as-a-list
   :language: python
   :caption: Implementation of a queue by means of a list

     # INSERT NEW CODE HERE


Następnie napisz wydajniejszą implementację kolejki ADT opartą na
tablicy kołowej (jak w podrozdziale 2.2 rozdziału czwartego). Pokaż
kilka przykładów działania kolejki. 


.. activecode:: queue-as-a-circular-array
   :language: python
   :caption: Implementation of a queue by means of a circular array

   class ArrayQueue:
   # queue implemented with a list
     DEFAULT = 10          # capacity of all queues

     def __init__(self):
       self._data = [None]*ArrayQueue.DEFAULT
       self._size = 0
       self._front = 0

     def __len__(self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def first(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       return self._data[self._front]

     def dequeue(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       element = self._data[self._front]
       self._data[self._front] = None
       self._front = (self._front + 1) % len(self._data)
       self._size −= 1
       return element

     def enqueue(self, e):
       if self._size == len(self._data):
         self._resize(2*len(self.data))           # double the array size
      position = (self._front + self._size) % len(self._data)
      self._data[position] = e
      self._size += 1

     def _resize(self, capacity):
       old = self._data                   # keeps track of existing list
       self._data = [None]*capacity       # allocate list with new capacity
       walk = self._front
       for k in range(self._size):
         self._data[k] = old[walk]        # shifts the indices
         walk = (1 + walk) % len(old)     # use old size
       self._front = 0                    # realigs front

          # INSERT NEW CODE HERE


**Ćwiczenie 1.3**

Napisz prostą implementację ADT z podwójną kolejką, dostosowując listę
Pythona. Pokaż kilka przykładów działania kolejki (patrz podrozdizał 3
rozdziału czwartego).

.. activecode:: deque-as-a-list
   :language: python
   :caption: Implementation of a deque by means of a list

     # INSERT NEW CODE HERE


Pozrozdział 2: Problemy na stosie, kolejkach, deque’s
-----------------------------------------------------

W tej sekcji zajmiemy się niektórymi klasycznymi problemami, które można
rozwiązać za pomocą stosu, kolejki i deque ADT. Przestudiuj każde
ćwiczenie, a następnie sprawdź, uzupełnij (jest to konieczne) i uruchom
dostarczony ActiveCode.

**Ćwiczenie 2.1**

Podaj algorytm, który konwertuje liczby całkowite dziesiętne (tj. liczby
o podstawie 10) na liczby binarne (tj. liczby o podstawie 2).

Rozwiązaniem tego problemu będzie użycie stosu do śledzenia cyfr
częściowego wyniku binarnego. Zaczynamy od liczby dziesiętnej większej
od 0. Następnie iterujemy dzielenie liczby przez 2 i odkładamy resztę (0
lub 1) na stos. Oznacza to, że budujemy liczbę binarną jako sekwencję
cyfr. Pierwsza reszta, którą obliczymy, będzie ostatnią cyfrą w
sekwencji. Na końcu iteracji cyfry binarne są usuwane ze stosu i
dołączane do prawego końca ciągu, który będzie reprezentował liczbę o
podstawie 2.

Sprawdź, uzupełnij i uruchom następujący kod ActiveCode.

.. activecode:: decimal-binary-convertion
   :language: python
   :caption: Convertion of a decimal number into binary representation

   from pythonds.basic import Stack

   def dectobin(decnumber):
     remstack = Stack()
     while decnumber > 0:
       remstack.push(decnumber % 2)
       decnumber = decnumber // 2

     binnumber = ""
     while not remstack.isEmpty():
       binnumber = binnumber + str(remstack.pop())
     return binnumber

   print(dectobin(51))

**Ćwiczenie 2.2**

Palindrom to ciąg, który odczytuje to samo do przodu i do tyłu (na
przykład radar, madam lub madam, I’m Adam). Napisz algorytm do
wprowadzania ciągu znaków i sprawdź, czy jest to palindrom.

Rozwiązaniem tego problemu będzie użycie deque do przechowywania znaków
ciągu. Dodajemy każdą nową postać z tyłu deque. Następnie porównujemy
przód deque (tj. pierwszy znak ciągu) i tył deque (tj. ostatni znak
ciągu) oraz kontynuujemy, jeśli pasują. Na koniec kończą nam się znaki
lub mamy tylko jeden w deque, w zależności od długości oryginalnego
ciągu (odpowiednio parzysty lub nieparzysty). W obu przypadkach ciąg
musi być palindromem.

Sprawdź, uzupełnij i uruchom kod w następującym ActiveCode.

.. activecode:: palindrome-checker
   :language: python
   :caption: Checking whether a string is a palindrome or not

   from pythonds.basic import Deque

   def palcheck(aString):
     word = Deque()
     for c in aString:
       word.addRear(c)

     equal = True
     while word.size() > 1 and equal:
       first = word.removeFront()
       last = word.removeRear()
       if first != last:
         equal = False
     return equal

   print(palcheck("lsdkjfskf"))
   print(palcheck("radar"))

**Ćwiczenie 2.3**

Notacja postfiksowa to jednoznaczny sposób zapisywania wyrażenia
arytmetycznego bez nawiasów. Jeśli *(exp1)* op *(exp2)* jest wyrażeniem
wrostkowym *infix*, którego operatorem jest *op*, jego wersją z przyrostkiem jest
*pexp1 pexp2 op*, gdzie *pexp1* jest wersją *exp1* z przyrostkiem, a *pexp2*
jest wersją *exp2* z przyrostkiem. Na przykład wersja przyrostkowa \\(((3+2) ∗ (5−3))/2\\) to \\(3 2 + 5 3 − ∗ 2  \\).

Aby zapewnić algorytm, który ocenia wyrażenie zapisane w notacji
postfiksowej, należy pamiętać, że za każdym razem, gdy operator jest
widoczny na wejściu, w ocenie zostaną użyte dwa ostatnie operandy.
Rozważmy na przykład wyrażenie przyrostkowe \\(3 2 5 * +\\) (czyli
wyrażenie wrostkowe \\( 3 + 2 * 5 ) \\).

Gdy przeglądasz wyrażenie od lewej do prawej, najpierw napotykasz
operandy 3, 2 i 5, a następnie umieszczasz każdy z nich na stosie,
udostępniając je, gdy następny pojawi się operator. Gdy operator jest
spełniony (*, w tym przykładzie), dwa ostatnie operandy muszą być użyte
w operacji mnożenia. Zdejmując stos dwukrotnie, możemy uzyskać
odpowiednie operandy, a następnie wykonać mnożenie (w tym przypadku
uzyskując wynik 10). Możemy teraz odłożyć ten wynik na stos, aby mógł
być użyty jako operand dla późniejszych operatorów w wyrażeniu. Po
przetworzeniu końcowego operatora na stosie pozostanie tylko jedna
wartość, wartość wyrażenia. Poniżej pokazujemy pseudokod tego algorytmu::


 Create an empty stack S
 Scan the expression from left to right
     if the element is an operand
       push it onto S;
     if the element is an operator, *, /, +, or -
       pop from S twice
       perform the arithmetic operation
       push the result back onto S
 When the expression ends, the result is on the stack.


Uzupełnij i uruchom następujący kod ActiveCode, w którym częściowo podam
definicję funkcji do oceny wyrażenia przyrostkowego ig. Aby pomóc w
arytmetyce, zdefiniowano funkcję pomocniczą ``doMath``, która pobiera dwa
operandy i operator, a następnie wykonuje odpowiednią operację
arytmetyczną.

.. activecode:: postfix-evaluation
   :language: python
   :caption: Evaluation of a postfix expression using a stack

   from pythonds.basic import Stack

   def postfixEval(postfixExpr):
     operandStack = Stack()
     tokenList = postfixExpr.split()

     for token in tokenList:
       if token in "0123456789":
         operandStack.push(int(token))
       else:
         operand2 = operandStack.pop()
         operand1 = operandStack.pop()
         result = doMath(token,operand1,operand2)
         operandStack.push(result)
     return operandStack.pop()

   def doMath(op, op1, op2):
     if op == "*":
       return op1 * op2
     elif op == "/":
       return op1 / op2
     elif op == "+":
       return op1 + op2
     else:
       return op1 - op2

   print(postfixEval('7 8 + 3 2 + /'))


Podrozdział 3: Ćwiczenia i ocena własna
---------------------------------------

**Ćwiczenie 3.1**

Jakie wartości są zwracane, gdy następująca seria operacji jest
wykonywana na pustym stosie? ``push(5)``, ``push(3)``, ``pop()``, ``push(2)``, ``push(8)``,
``pop()``, ``pop()``, ``push (9)``, ``push(1)``, ``pop()``, ``push (7)`` , ``push (6)``, ``pop()``,
``pop()``, ``push(4)``, ``pop()``, ``pop()``. *(Wskazówka: użyj jednej z poprzednich
implementacji stosu ADT).*

**Ćwiczenie 3.2**

Jaki jest obecny rozmiar stosu S, początkowo pustego, po wykonaniu 25
``push``, 12 ``top`` i 10 ``pop``, z których 3 zgłosiły błędy ``Empty``, które zostały
przechwycone i zignorowane? *(Wskazówka: użyj jednej z poprzednich
implementacji stosu ADT).*

**Ćwiczenie 3.3**

Napisz funkcję ``transfer(S,T)``, która przenosi elementy ze stosu S do
stosu T, tak aby element znajdujący się na górze S był pierwszym
wstawiany do T, a element na dole S kończy się na na górze T.
*(Wskazówka: Przesyłaj elementy pojedynczo).*

**Ćwiczenie 3.4**

Napisz metodę rekurencyjną, która usuwa wszystkie elementy ze stosu.

**Ćwiczenie 3.5**

Napisz funkcję odwracającą listę elementów. (Wskazówka: włóż elementy
listy na stos w jednej kolejności, a następnie przesuń i zapisz je z
powrotem na liście).

**Ćwiczenie 3.6**

Jakie wartości są zwracane, gdy następująca seria operacji jest
wykonywana w pustej kolejce? ``enqueue(5)``, ``enqueue(3)``, ``dequeue()``, ``enqueue(2)``, ``enqueue(8)``, ``dequeue()``, ``enqueue(9)``,
``enqueue(1)``, ``dequeue()``, ``enqueue(7)``, ``enqueue(6)``, ``dequeue()``, ``dequeue()``,
``enqueue(4)``, ``dequeue()``, ``dequeue()``. 
*(Wskazówka: użyj jednej z poprzednich implementacji kolejki
ADT).*

**Ćwiczenie 3.7**

Jaki jest obecny rozmiar kolejki Q, początkowo pustej, po wykonaniu 32
``enqueue``, 10 ``first`` oraz 15 ``dequeue``, z których 5 wywołało puste błędy,
które zostały przechwycone i zignorowane? *(Wskazówka: użyj jednej z
poprzednich implementacji kolejki ADT).*

**Ćwiczenie 3.8**

Jakie wartości są zwracane podczas następującej sekwencji operacji
wykonywanych na pustej deque? ``add_first(4)``, ``add_last(8)``, ``add_last(9)``, ``add_first(5)``, ``back()``, ``delete_first( )``,
``delete_last( )``, ``add_last(7)``, ``first( )``, ``last( )``, ``add_last(6)``, ``delete_first( )``, ``delete_first( )``.
*(Wskazówka: użyj jednej z poprzednich implementacji deque ADT).*

**Ćwiczenie 3.9**

Deque D zawiera liczby (1,2,3,4,5,6,7,8) w tej kolejności, a kolejka Q
jest początkowo pusta. Napisz fragment kodu, który używa tylko D i Q i
powoduje, że D przechowuje elementy w kolejności (1,2,3,5,4,6,7,8).
*(Wskazówka: użyj wartości zwracanej przez metodę usuwania jako
parametru metody wstawiania).*

**Ćwiczenie 3.10**

Powtórz poprzednie ćwiczenie, używając stosu S zamiast kolejki Q.

**Ćwiczenie 3.11**

Biorąc pod uwagę trzy różne liczby całkowite na stos S, w losowej
kolejności, napisz fragment pseudokodu (bez pętli lub rekurencji), który
używa tylko jednego porównania i tylko jednej zmiennej x, co powoduje,
że zmienna x przechowuje największą z trzech liczby całkowite.
*(Wskazówka: Pop górną liczbę całkowitą i zapamiętaj ją).*

**Ćwiczenie 3.12**

Mając trzy niepuste stosy R, S i T, opisz sekwencję operacji, która
powoduje, że S przechowuje wszystkie elementy pierwotnie w T poniżej
wszystkich oryginalnych elementów S, z obydwoma zestawami elementów w
ich pierwotnej kolejności. Na przykład, jeśli R = [1,2,3], S = [4,5] i T
= [6,7,8,9], początkowo końcowa konfiguracja powinna mieć S = [6,7,8
,9,4,5]. *(Wskazówka: użyj R jako tymczasowego przechowywania, bez
wyskakiwania jego oryginalnej zawartości).*

**Ćwiczenie 3.13**

Opisz, jak zaimplementować stos ADT przy użyciu pojedynczej kolejki jako
zmiennej wystąpienia i tylko stałej dodatkowej pamięci lokalnej w treści
metody. *(Wskazówka: Obróć elementy w kolejce).*

**Ćwiczenie 3.14**

Opisz, jak zaimplementować kolejkę ADT, używając dwóch stosów jako
zmiennych instancji. *(Wskazówka: użyj jednego stosu do zbierania
przychodzących elementów, a drugiego jako bufora dla elementów, które
mają zostać dostarczone).*

**Ćwiczenie 3.15**

Opisz, jak zaimplementować ADT z podwójną kolejką, używając dwóch stosów
jako zmiennych instancji. *(Wskazówka: użyj jednego stosu na każdy
koniec deque).*

**Ćwiczenie 3.16**

Mając stos S zawierający elementy (n) i kolejkę Q, początkowo pustą,
pokaż, jak używać Q do skanowania S w celu sprawdzenia, czy zawiera
określony element (x). Algorytm musi zwrócić elementy z powrotem do S w
ich pierwotnej kolejności.

**Ćwiczenie 3.17**

Podaj pełną implementację ``ArrayDeque`` ADT z podwójną kolejką.

**Ćwiczenie 3.18**

Stosy mogą służyć do zapewniania obsługi cofania ``undo`` w aplikacjach, takich
jak przeglądarka internetowa lub edytor tekstu. Ta funkcjonalność może
być zaimplementowana ze stosem nieograniczonym, ale wiele aplikacji
obsługuje ją ze stosem o stałej pojemności, jak następuje: gdy ``push`` jest
wywoływane ze stosem z pełną pojemnością, wypchnięty element jest
wstawiany na górze, a najstarszy element jest wypchnięty z dna stosu, aby
zrobić miejsce. Podaj implementację takiego stosu, używając okrągłej
tablicy o odpowiedniej pojemności.

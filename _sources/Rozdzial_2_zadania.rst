Rozdział 2: Rekurencja - ćwiczenia
**********************************

Podrozdział 1: Przegląd teorii
------------------------------

Uzupełnij i wykonaj fragmenty kodu w poniższych sekcjach (kod można
również znaleźć w rozdziale drugim).

**Ćwiczenie 1.1**

Pierwszym rozwiązaniem do znalezienia wartości docelowej w
nieposortowanej sekwencji jest wyszukiwanie sekwencyjne. Pętla służy do
sprawdzania każdego elementu, dopóki nie zostanie znaleziony cel lub
zestaw danych zostanie wyczerpany. Wyszukiwanie binarne to algorytm,
który znajduje wartość docelową wewnątrz posortowanej, indeksowalnej
sekwencji elementów \\(n\\), przy użyciu rekurencji w bardziej efektywny
sposób. Użyj funkcji zdefiniowanej w poniższym ActiveCode, aby znaleźć
wartość docelową w tablicy liczb całkowitych, która została wcześniej
posortowana.

.. activecode:: binary-search
   :language: python
   :caption: Function for the binary search

   def binary_search(data, target, low, high):
     # Return True if target is found in data, between low and high
     if low > high:
       return False               # no match found
     else:
       mid = (low + high) / 2
       if target == data[mid]:    # found a match
         return True
       elif target < data[mid]:
         return binary_search(data, target, low, mid - 1)   # call on the left portion
       else:
         return binary_search(data, target, mid + 1, high)  # call on the right portion

        # INSERT NEW CODE HERE
        
**Ćwiczenie 1.2**

Rozważ problem odwrócenia elementów \\(A\\) i indeksowanej sekwencji;
pierwszy element staje się ostatnim, drugi staje się przedostatni i tak
dalej. Implementacja rekurencyjna jest podana poniżej. Użyj go, aby
odwrócić szyk.

.. activecode:: reverse-a-sequence
   :language: python
   :caption: Function for reversing a sequence of elements

   def reverse(A, first, last):
     # Reverse elements in A, between first and last
     if first < last - 1:                           # if there are at least 2 elements
       A[first], A[last-1] = A[last-1], A[first]    # swap first and last
       reverse(A, first+1, last-1)

        # INSERT NEW CODE HERE

**Ćwiczenie 1.3**

Rozważ problem sumowania elementów indeksowanej sekwencji \\(A\\). Suma
wszystkich \\(n\\) liczb całkowitych w \\(A\\) jest sumą pierwszych \\(n-1\\) liczb
całkowitych w \\(A\\) plus jej ostatni element. Poniżej przedstawiamy
kodowanie Pythona, użyj go na sekwencji elementów.

.. activecode:: linear-sum
   :language: python
   :caption: Linear sum of a sequence of elements

   def linear_sum(A, n):
     # Returns the sum of the first n numbers of A
     if n == 0:
       return 0
     else:
       return A[n-1] + linear_sum(A, n-1)

    # INSERT NEW CODE HERE


Zamiast sumować ostatni element \\(A\\) z sumą pozostałych elementów, oblicz
sumę pierwszej połowy \\(A\\) i sumę drugiej połowy \\(A\\), rekurencyjnie, a
następnie dodaj te liczby.

.. activecode:: binary-sum
   :language: python
   :caption: Binary sum of a sequence of elements

   def binary_sum(A, first, last):
     # Return the sum of the numbers in A between first and last
     if first <= last:     # no elements
       return 0
     elif first == last-1:     # one element
       return A[first]
     else:
       mid = (first + last) / 2
       return binary_sum(A, first, mid) + binary_sum(A, mid, last)

    # INSERT NEW CODE HERE



**Ćwiczenie 1.4**

Liczby Fibonacciego można zdefiniować jako \\(F_0 = 0\\), lub \\(F_1 = 1\\), lub
\\(F_n = F_{n-2} + F_{n-1}\\). Kod Pythona dla tej funkcji znajduje się w
następującym ActiveCode. Spróbuj i sprawdź, czy możesz uruchomić tę
funkcję.

.. activecode:: bad-fibonacci
   :language: python
   :caption: Hard to compute Fibonacci function

   def fibonacci(n):
     if n <= 1:
       return n
     else:
       return fibonacci(n-2) + fibonacci(n-1)

    # INSERT NEW CODE HERE
    
    
Bardziej efektywny sposób zdefiniowania tego programu jest wspominany
wyżej. Porównaj go z poprzednim kodem.

.. activecode:: good-fibonacci
   :language: python
   :caption: Easy to compute Fibonacci function

   def good_fibonacci(n):
     if n <= 1:
       return (n,0)
     else:
       (a,b) = good_fibonacci(n-1)
       return (a+b,a)

    # INSERT NEW CODE HERE

**Ćwiczenie 1.5**

Problem jednoznaczności elementu polega na ustaleniu, czy wszystkie
elementy ciągu \\(A\\) są od siebie różne.

Pierwsze rozwiązanie to algorytm iteracyjny. Użyj tej funkcji w ogólnej
tablicy.

.. activecode:: iterative-uniqueness
   :language: python
   :caption: Iterative solution to the element uniqueness problem

   def unique1(A):
     for i in range(len(A)):
       for j in range(i+1, len(A)):
         if A[i] == A[j]:
           return False
     return True

    # INSERT NEW CODE HERE

Poniżej podano nieefektywną implementację rekurencyjną.

.. activecode:: recursive-uniqueness
   :language: python
   :caption: Recursive solution to the element uniqueness problem

   def unique3(A, first, last):
     # Returns True if there are no duplicate elements in A[first:last]
     if last - first <= 1:
       return True
     elif not unique3(A, first, last-1):
       return False
     elif not unique3(a, first+1, last):
       return False
     else:
       return A[first] != A[last-1]

     # INSERT NEW CODE HERE


Ulepszone rozwiązanie jest podane w poniższym skrypcie.

.. activecode:: improved-recursive-uniqueness
   :language: python
   :caption: Improved recursive solution to the element uniqueness problem

   def unique(A, first, last):
     if last-first <= 1:
       return True
     elif A[first] in A[first+1:]:
       return False
     else:
       return unique(A, first+1, last)

     # INSERT NEW CODE HERE
     

Podrozdział 2: Problemy z rekurencją
------------------------------------

W tej sekcji pokażemy klasyczny problem, który można rozwiązać za pomocą
funkcji rekurencyjnych. Sprawdź, uzupełnij i uruchom ActiveCode w każdej
podsekcji.

**Ćwiczenie 2.1**

Zagadka *Wieża Hanoi* została wynaleziona przez francuskiego matematyka
Edouarda Lucasa w 1883 roku. Legenda mówi o świątyni, w której kapłani
otrzymali trzy słupy i stos 64 złotych dysków, z których każdy jest
mniejszy niż ten znajdujący się pod nim. Musieli przenieść wszystkie 64
dyski z jednego z trzech biegunów na drugi, przestrzegając dwóch zasad:
po pierwsze, można było przenieść tylko jeden dysk na raz; wtedy większy
dysk nie może być umieszczony na mniejszym. Legenda głosiła, że kiedy
skończą pracę, świat zniknie.

Liczba ruchów wymaganych do prawidłowego przeniesienia wieży z 64
dyskami to \\(2^{64} -1\\), czyli 18.446.744.073.709.551.615. Przesuwając
jeden dysk na sekundę, kapłan będzie potrzebował 584.942.417.355 lat.

Aby znaleźć rekurencyjne rozwiązanie tego problemu, rozważmy następujący
przykład. Mamy trzy kołki, mianowicie kołek jeden, kołek dwa i kołek
trzy, i załóżmy, że mamy wieżę z czterech dysków, pierwotnie na jednym
słupku. Jeśli już wiemy, jak przenieść wieżę z trzech krążków na słupek
dwa, możemy łatwo przenieść dolny krążek na słupek trzy, a następnie
przenieść wieżę trzech krążków z słupek dwa na słupek trzy. Nie wiemy,
jak przenieść wieżę o wysokości trzy, ale jeśli wiemy, jak przenieść
wieżę o wysokości dwa, możemy przenieść trzeci krążek z jednego słupka
na drugi, a wieżę dwóch z jednego słupka na górę. Pod koniec tej
(rekurencyjnej) procedury będziemy mogli przesunąć pojedynczy słupek.

Ogólny zarys tego, jak przenieść wieżę z drążka startowego na drążek
bramkowy za pomocą drążka pośredniego, to:

-  Przenieś wieżę o wysokości-1 do pośredniego drążka, używając
   ostatniego drążka
-  Przenieś pozostały dysk do ostatniego bieguna
-  Przenieś wieżę o wysokości-1 z pośredniego drążka na ostatni drążek,
   używając oryginalnego drążka

Dopóki zawsze przestrzegamy zasady, że większe dyski pozostają na dnie
stosu, możemy rekurencyjnie korzystać z trzech powyższych kroków.
Trafiamy na podstawowy przypadek rekurencji, gdy mamy wieżę jednego
dysku. W tym przypadku musimy przenieść tylko jeden dysk do miejsca
docelowego. Funkcja Pythona jest zdefiniowana w następujący sposób
(zwróć uwagę na kolejność ``fromPole``, ``toPole`` i ``withPole`` w rekurencyjnych
wywołaniach ``moveTower``)::

  def moveTower(height, fromPole, toPole, withPole):
    if height >= 1:
        moveTower(height-1, fromPole, withPole, toPole)
        moveDisk(fromPole, toPole)
        moveTower(height-1, withPole, toPole, fromPole)

Poprzedni kod wykonuje dwa różne wywołania rekurencyjne: - w pierwszym,
wszystkie oprócz dolnego dysku są przenoszone z początkowej wieży do
pośredniego bieguna, - kolejna linia przesuwa dolny krążek do miejsca
ostatecznego spoczynku; - drugie wywołanie rekurencyjne przenosi wieżę z
bieguna pośredniego na szczyt największego dysku.

Przypadek podstawowy jest wykrywany, gdy wysokość wieży wynosi 0 i w tym
przypadku nie ma nic do zrobienia.

Funkcja ``moveDisk`` wypisuje po prostu, że przenosi dysk z jednego bieguna
na drugi::

  def moveDisk(from,to):
   print("moving disk from", from, "to", to)

Poniższy ActiveCode zapewnia całe rozwiązanie.

.. activecode:: tower-of-hanoi
   :language: python
   :caption: Recursive solution for the Tower of Hanoi problem

   def moveTower(height,fromPole, toPole, withPole):
       if height >= 1:
           moveTower(height-1,fromPole,withPole,toPole)
           moveDisk(fromPole,toPole)
           moveTower(height-1,withPole,toPole,fromPole)

   def moveDisk(fp,tp):
       print("moving disk from",fp,"to",tp)

   moveTower(3,"A","B","C")

**Ćwiczenie 2.2**


Sito Eratostenesa to prosty algorytm do znajdowania wszystkich liczb
pierwszych aż do określonej liczby całkowitej \\(n\\). Funkcja rekurencyjna
implementująca ten algorytm ma następujący pseudokod:

a) utwórz listę liczb całkowitych 2, 3, 4, …, n;
b) ustaw licznik *i* na 2 (pierwsza liczba pierwsza);
c) zaczynając od *i+i*, odlicz do *i* ORAZ usuń te liczby z listy
   (2\ *i, 3*\ i, …);
d) znaleźć pierwszy numer na liście po *i*, to jest kolejna liczba
   pierwsza;
e) ustawić licznik *i* na tę liczbę;
f) powtarzaj kroki c i d, aż *i* będzie większe niż *n*.

Poniższy program implementuje sito Eratostenesa w sposób iteracyjny.
Wydrukuje pierwszych 100 liczb pierwszych.

.. activecode:: sieve-of-eratosthenes-iterative
   :language: python
   :caption: Iterative solution for the sieve of Eratosthenes

   from math import sqrt
   def sieve(n):
     primes = list(range(2,n+1))
     max = sqrt(n)
     num = 2
     while num < max:
       i = num
       while i <= n:
         i += num
         if i in primes:
           primes.remove(i)
       for j in primes:
         if j > num:
           num = j
           break
     return primes
   print(sieve(100))
   
Poniżej podajemy rekurencyjne rozwiązanie tego samego problemu.

.. activecode:: sieve-of-eratosthenes-recursive
   :language: python
   :caption: Recursive solution for the sieve of Eratosthenes

   from math import sqrt
   def primes(n):
     if n == 0:
       return []
     elif n == 1:
       return []
     else:
       p = primes(int(sqrt(n)))
       nop = [j for i in p for j in range(i*2, n + 1, i)]
       p = [x for x in range(2, n + 1) if x not in nop]
       return p
     print(primes(100))



Podrozdział 3: Ćwiczenia i ocena własna
---------------------------------------

**Ćwiczenie 3.1**


Napisz rekurencyjny kod funkcji \\(f(n)=3*n\\), czyli wielokrotności 3.
(Wskazówka: rekurencyjną definicję tej funkcji można zapisać jako \\(f(1) = 3\\) i \\(f(n+1)=f(n)+3)\\).

**Ćwiczenie 3.2**


Napisz rekurencyjny kod funkcji \\(power(x,n)=x^n\\), czyli funkcję
potęgową. (Wskazówka: Prosta definicja rekurencyjna wynika z faktu, że
\\(x^n = x * x^{n-1}\\), for \\(n > 0)\\).

**Ćwiczenie 3.3**

Napisz rekurencyjny kod funkcji, która zwraca sumę pierwszych \\(n\\) liczb
całkowitych. (Wskazówka: rozwiązanie wynika z obserwacji, że suma
pierwszych \\(n\\) liczb jest równa sumie \\(n\\) i sumie pierwszych \\(n-1\\) liczb
całkowitych).

**Ćwiczenie 3.4**

Napisz funkcję rekurencyjną ``find_index()``, która zwraca indeks liczby w
ciągu Fibonacciego, jeśli liczba jest elementem tej sekwencji, a w
przeciwnym razie zwraca -1.

**Ćwiczenie 3.5**

Opisz rekurencyjny algorytm znajdowania maksimum i minimum elementów w
sekwencji \\(n\\) liczb całkowitych, bez używania żadnych pętli. Jaki jest
Twój czas pracy i wykorzystanie przestrzeni? (Wskazówka: rozważ
zwrócenie krotki, która zawiera zarówno wartość minimalną, jak i
maksymalną).

**Ćwiczenie 3.6**


Napisz funkcję rekurencyjną, aby odwrócić listę.

**Ćwiczenie 3.7**


Napisz funkcję rekurencyjną dla problemu jednoznaczności elementu, która
działa w czasie \\(O(n^2)\\), bez użycia sortowania. (Wskazówka:
stwierdzenie, czy elementy sekwencji są niepowtarzalne, można sprowadzić
do problemu stwierdzenia, czy wszystkie ostatnie elementy \\(n−1\\) są
niepowtarzalne i różne od pierwszego elementu).

**Ćwiczenie 3.8**


Napisz rekurencyjny algorytm do obliczania iloczynu dwóch dodatnich
liczb całkowitych, \\(m\\) i \\(n\\), używając tylko dodawania. (Wskazówka:
iloczyn \\(m\\) i \\(n\\) jest sumą \\(m\\), \\(n\\) razy).

**Ćwiczenie 3.9**


Napisz rekurencyjny algorytm obliczający wykładnik dwóch dodatnich liczb
całkowitych \\(m^n\\), używając tylko iloczynu. (Wskazówka: zastanów się,
jak można zdefiniować potęgowanie za pomocą iloczynu).

**Ćwiczenie 3.10**


Napisz funkcję rekurencyjną, która pobiera ciąg znaków *s* i wyprowadza
jego odwrotność. Na przykład odwróceniem puli byłoby zatrzymanie.
(Wskazówka: printuj po jednym znaku na raz, bez dodatkowych spacji).

**Ćwiczenie 3.11**


Napisz funkcję rekurencyjną, aby sprawdzić, czy łańcuch *s* jest
palindromem, czyli jest równy jego odwrotności. (Wskazówka: Sprawdź
równość pierwszego i ostatniego znaku oraz powtórz).

**Ćwiczenie 3.12**


Użyj rekurencji, aby napisać funkcję określającą, czy ciąg *s* ma więcej
samogłosek niż spółgłosek.

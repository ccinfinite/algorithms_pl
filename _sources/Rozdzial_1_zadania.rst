Rozdział 1: Analiza algorytmów - ćwiczenia
==================================

Podrozdział 1: Przegląd teorii
-----------------------------------

Uzupełnij następujące fragmenty kodu (można je również znaleźć w
rozdziale pierwszym) i uruchom programy. Dla każdego programu zapewnij
(1) analizę asymptoty i (2) eksperymentalną analizę zużycia czasu,
używając funkcji ``time()`` modułu czasu, która zwraca liczbę sekund, które
upłynęły od czasu odniesienia; run ``time()`` przed i po uruchomieniu
algorytmu, aby zmierzyć upływ czasu jako różnicę dwóch wartości. Użyj
Codelens do przechodzenia przez kod.

Pierwszy program ``sumofn`` jest przykładem, jak to zrobić. Drukuje on
dziesięć razy wynik (suma pierwszych 1000 liczb całkowitych) i ilość
czasu potrzebną do obliczenia. Sprawdź, co się stanie, jeśli obliczona
zostanie suma pierwszych 10 000 liczb całkowitych w odniesieniu do
wyników analizy asymptotycznej (czas każdego przebiegu powinien zająć 10
razy więcej sekund).

.. activecode:: sum-of-n2
   :language: python
   :caption: Prints the sum of the first n integers

   import time

   def sumofn(n):
     start = time.time()
     sum = 0
     for i in range(1,n+1):
       sum = sum + i
     end = time.time()
     return sum,end-start

   for i in range(10):
     print("Sum is %d required %10.7f seconds"%sumofn(1000))



.. activecode:: max-list
   :language: python
   :caption: Returns the maximun element from a list

   def find_max(data):
     # Return the maximum element from a list
     biggest = data[0]
       for val in data:
         if val > biggest:
           biggest = val
       return biggest

      # INSERT NEW CODE HERE



.. tabbed:: prefix-average

   .. tab:: Quadratic time

      .. activecode:: prefix-average1
         :language: python
         :caption: Quadratic time solution of the prefix average problem

         def prefix_average(S):
           n = len(S)
           A = [0]*n                # list of n zeros
           for j in range(n):
             total = 0
             for i in range(j + 1):
                total += S[i]
              A[j] = total / (j+1)    # the j-th average
           return A

          # INSERT NEW CODE HERE


   .. tab:: Linear time

       .. activecode:: prefix-average2
         :language: python
         :caption: Linear time solution of the prefix average problem

         def prefix_average(S):
           n = len(S)
           A = [0]*n                # list of n zeros
           for j in range(n):
             total = 0
             for i in range(j + 1):
                total += S[i]
              A[j] = total / (j+1)    # the j-th average
           return A

          # INSERT NEW CODE HERE



.. tabbed:: element-uniqueness

   .. tab:: Quadratic time

      .. activecode:: element-uniqueness1
         :language: python
         :caption: Quadratic time solution of the element uniqueness problem

         def unique1(S):
           for j in range(len(S)):
             for k in range(j+1, len(S)):
               if S[j] == S[k]:
                 return False        # there is a duplicate pair
           return True               # all elements are distinct

            # INSERT NEW CODE HERE

   .. tab:: Logaritmic time

       .. activecode:: element-uniqueness2
         :language: python
         :caption: nlogn time solution of the element uniqueness problem

         def unique2(S):
           temp = sorted(S)            # sorting of S
           for j in range(1, len(temp)):
             if temp[j-1] == temp[j]:
               return False            # there is duplicate pair
           return True                 # all elements are distinct

                       # INSERT NEW CODE HERE


Podrodział 2: Problem z wykrywaniem anagramów
-----------------------------------------------------------

Mając dwa ciągi znaków o jednakowej długości, rozwiązanie problemu
wykrywania anagramów polega na napisaniu funkcji, która zwraca ``True``,
jeśli pierwszy ciąg jest przeorganizowaniem drugiego, w przeciwnym razie
- ``False``. Istnieje kilka rozwiązań tego problemu, o różnej złożoności
czasowej. Poniżej przedstawiamy niektóre z nich. Przed sprawdzeniem
rozwiązań spróbuj znaleźć algorytm.

Podrodział 2.1: Zaznaczanie znaków
------------------------------------------

Jeśli ciągi są tej samej długości, a każdy znak pierwszego ciągu
występuje w drugim, to te dwa ciągi są anagramami. Za każdym razem, gdy
znak z pierwszej listy zostanie znaleziony w drugiej, musi być
zaznaczony, co oznacza, że zostanie zastąpiony przez ``None`` w drugim
ciągu. Jeśli wszystkie znaki są zaznaczone, funkcja zwróci ``True``. Ta
funkcja może być ujawniona w następującym ActiveCode. Spróbuj zmienić
ciągi, aby zobaczyć, jak działa funkcja. Zauważ, że dla (i)-tego znaku
na liście ``s1`` będą \\(i\\) w ``s2``, dla \\(i=0 \ldots n\\). Zatem
całkowita liczba wizyt jest sumą liczb całkowitych od 1 do \\(n\\), co
oznacza, że czasochłonność wynosi \\(O(n^2)\\).

.. reveal:: check-off
    :showtitle: Show solution
    :hidetitle: Hide solution

    .. activecode:: checking-off
       :language: python
       :caption: Detects anagrams checking-off the characters

       def anagramdetection1(s1,s2):
         stillgood = True
         if len(s1) != len(s2):
           stillgood = False
         alist = list(s2)

         pos1 = 0
         while pos1 < len(s1) and stillgood:
           pos2 = 0
           found = False
           while pos2 < len(alist) and not found:
               if s1[pos1] == alist[pos2]:
                 found = True
               else:
                 pos2 = pos2 + 1

           if found:
             alist[pos2] = None
           else:
             stillgood = False
           pos1 = pos1 + 1
       return stillgood

       print(anagramSolution1('ajcd','dcba'))

Podrodział 2.2: Sortowanie ciągów
----------------------------------------

Innym rozwiązaniem jest posortowanie dwóch ciągów alfabetycznie jest
przekształcenie każdego ciągu w listę i użycie wbudowanej metody
``sort`` na listach. Jeśli są anagramami, po uporządkowaniu będą miały
ten sam ciąg. Poniższy kod ActiveCode przedstawia to rozwiązanie.
Ponownie spróbuj obliczyć algorytm przed ujawnieniem rozwiązania.
Spróbuj również zmienić ciągi, aby zobaczyć, jak działa funkcja.

Sekcja while poprzedniego programu potrzebuje czasu \\(O(n)\\) na wykonanie,
ponieważ po sortowaniu istnieje \\(n\\) porównanie znaków. Ale metody
sortowania wymagają czasu \\(O(n^2)\\) lub \\(O(n logn)\\), więc operacje
sortowania dominują w iteracji.

.. reveal:: sorting
    :showtitle: Show solution
    :hidetitle: Hide solution

    .. activecode:: sorting-the-strings
       :language: python
       :caption: Detects anagrams sorting the strings

       def anagramdetection2(s1,s2):
         alist1 = list(s1)
         alist2 = list(s2)

         alist1.sort()
         alist2.sort()

         pos = 0
         matches = True

         while pos < len(s1) and matches:
           if alist1[pos]==alist2[pos]:
              pos = pos + 1
           else:
              matches = False

         return matches

         print(anagramSolution2('abcde','edcba'))


Podrodział 2.3: Liczenie i porównywanie
-----------------------------------------------


Jeśli dwa łańcuchy są anagramami, będą miały taką samą liczbę a, b, c i
tak dalej. Po pierwsze, moglibyśmy policzyć, ile razy wystąpił każdy
znak, używając listy 26 liczników, po jednym dla każdego możliwego
znaku. Każdy znak zostanie spełniony, licznik na tej pozycji będzie
zwiększany. Jeśli dwie listy liczników są identyczne, łańcuchy muszą być
anagramami.

Pierwsze dwie iteracje, używane do zliczania znaków, są oparte na
długości każdego ciągu. Trzecia iteracja zajmuje 26 kroków. Całkowite
zużycie czasu to \\(O(n)\\), czyli algorytm czasu liniowego. Zauważ, że
nawet jeśli jest to optymalne rozwiązanie, ten wynik osiąga się za
pomocą dwóch małych dodatkowych list, które utrzymują liczbę znaków.
Oznacza to, że musimy zaakceptować kompromis między wykorzystaniem czasu
i przestrzeni.

.. reveal:: counting
    :showtitle: Show solution
    :hidetitle: Hide solution

    .. activecode:: counting-the-characters
       :language: python
       :caption: Detects anagrams counting the characters

       def anagramSolution4(s1,s2):
         c1 = [0]*26
         c2 = [0]*26

         for i in range(len(s1)):
           pos = ord(s1[i])-ord('a')
           c1[pos] = c1[pos] + 1

         for i in range(len(s2)):
           pos = ord(s2[i])-ord('a')
           c2[pos] = c2[pos] + 1

         j = 0
         stillOK = True
         while j<26 and stillOK:
           if c1[j]==c2[j]:
              j = j + 1
           else:
              stillOK = False

         return stillOK

       print(anagramSolution4('ajcd','dcba'))



Podrodział 2.4: Znajdowanie wszystkich możliwych anagramów
---------------------------------------------------------------------------


Technika *Brute Force* do rozwiązania tego problemu próbuje wygenerować
listę wszystkich możliwych ciągów za pomocą znaków z ``s1``, a następnie
sprawdzić czy ``s2`` występuje wśród nich. Całkowita liczba ciągów
kandydujących wynosi \\(n∗(n−1)∗(n−2)∗ \ldots ∗3∗2∗1\\), czyli
\\(n!\\). Funkcja silnia rośnie szybciej niż \\(2^n\\), co oznacza, że podejście
brute force nie jest zalecane.


Podrozdział 3: Ćwiczenia i ocena własna
-----------------------------------------------

Podaj \\(O\\) charakteryzację, pod względem \\(n\\), czasu działania
następujących fragmentów kodu (wskazówki: weź pod uwagę, ile razy pętla
jest wykonywana i ile operacji pierwotnych występuje w każdej iteracji).

**Ćwiczenie 3.1**::


 1  def example1(S):
 2  ”””Return the sum of the elements in sequence S.”””
 3    n = len(S)
 4    total = 0
 5    for j in range(n):     # loop from 0 to n-1
 6      total += S[j]
 7    return total

**Ćwiczenie 3.2**::


 1  def example2(S):
 2  ”””Return the sum of the elements with even index in sequence S.”””
 3    n = len(S)
 4    total = 0
 5    for j in range(0, n, 2):     # increment of 2
 6      total += S[j]
 7    return total
 
**Ćwiczenie 3.3**::

 1  def example3(S):
 2  ”””Return the sum of the prefix sums of sequence S.”””
 3    n = len(S)
 4    total = 0
 5    for j in range(n):         # loop from 0 to n-1
 6      for k in range(1+j):     # loop from 0 to j
 7        total += S[k]
 8    return total

**Ćwiczenie 3.4**::

 1  def example4(S):
 2  ”””Return the sum of the prefix sums of sequence S.”””
 3    n = len(S)
 4    prefix = 0
 5    total = 0
 6    for j in range(n):
 7      prefix += S[j]
 8      total += prefix
 9    return total

**Ćwiczenie 3.5**::


 1  def example5(A, B): # assume that A and B have equal length
 2  ”””Return the number of elements in B equal to the sum of prefix sums in A.”””
 3    n = len(A)
 4    count = 0
 5    for i in range(n):         # loop from 0 to n-1
 6      total = 0
 7      for j in range(n):       # loop from 0 to n-1
 8        for k in range(1+j):   # loop from 0 to j
 9          total += A[k]
 10     if B[i] == total:
 11       count += 1
 12   return count


**Ćwiczenie 3.6**


Algorytm \\(A\\) wykonuje obliczenia czasu \\(O(\log n)\\) dla
każdego wpisu sekwencji \\(n\\)-elementów. Jaki jest jego najgorszy czas
działania?

(Wskazówka: obliczenie \\(O(\log n)\\) jest wykonywane \\(n\\)
razy)

**Ćwiczenie 3.7**


Biorąc pod uwagę sekwencję \\(n\\) elementów \\(S\\), algorytm \\(B\\) wybiera
losowo \\(\log n\\) elementy w \\(S\\) i wykonuje obliczenia czasu
\\(O(n)\\) dla każdego z nich. Jaki jest najgorszy czas działania \\(B\\)?

(Wskazówka: obliczenie \\(O(n)\\) jest wykonywane \\(\log n\\) razy)

**Ćwiczenie 3.8**


Biorąc pod uwagę \\(n\\)-elementową sekwencję \\(S\\) liczb całkowitych,
algorytm \\(C\\) wykonuje obliczenia czasu \\(O(n)\\) dla każdej liczby
parzystej w \\(S\\) i czasu \\(O(\log n)\\) obliczenia dla każdej
liczby nieparzystej w \\(S\\). Jakie są najlepsze i najgorsze czasy
działania \\(C\\)?

(Wskazówka: rozważ przypadki, w których wszystkie wpisy \\(S\\) są parzyste
lub nieparzyste)

**Ćwiczenie 3.9**


Mając ciąg \\(n\\)-elementowy \\(S\\), algorytm \\(D\\) wywołuje algorytm \\(E\\) na
każdym elemencie \\(S[i]\\). Algorytm \\(E\\) działa w czasie \\(O(i)\\) w momencie
jego wywołania \\(S[i]\\). Jaki jest najgorszy czas działania \\(D\\)?

(Wskazówka: scharakteryzuj czas działania \\(D\\) za pomocą sumowania)

**Ćwiczenie 3.10**


Opisz wydajny algorytm znajdowania dziesięciu największych elementów w
sekwencji o rozmiarze \\(n\\). Jaki jest czas działania twojego algorytmu?

(Wskazówka: 10 to liczba stała)

**Ćwiczenie 3.11**


Opisz algorytm znajdowania zarówno minimum, jak i maksimum \\(n\\) liczb
przy użyciu mniej niż \\(3n/2\\) porównań.

(Wskazówka: skonstruuj grupę kandydatów na minima i grupę kandydatów na
maksima)

**Ćwiczenie 3.12**

Sekwencja \\(S\\) zawiera \\(n−1\\) unikalnych liczb całkowitych z zakresu
\\([0,n−1]\\), to znaczy, że jest jedna liczba z tego zakresu, która nie
znajduje się w \\(S\\). Zaprojektuj algorytm \\(O(n)\\)-czas, który znajduje tę
liczbę. Możesz użyć tylko \\(O(1)\\) dodatkowego miejsca.

(Wskazówka: użyj funkcji liczb całkowitych w \\(S\\), która natychmiast
identyfikuje brakującą liczbę)

**Ćwiczenie 3.13**


Ciąg \\(S\\) zawiera \\(n\\) liczb całkowitych z przedziału \\([0,4n]\\), z
dozwolonymi powtórzeniami. Opisz efektywny algorytm wyznaczania liczby
całkowitej \\(k\\), która występuje najczęściej w \\(S\\). Jaki jest czas
działania algorytmu?

(Wskazówka: użyj tablicy, która przechowuje liczniki dla każdej
wartości)


Rozdział pierwszy: Analiza algorytmów
=====================================

Podrozdział 1: Wprowadzenie
-----------------------------------

*Struktura danych* w znaczeniu formalnym opiera się na zdefiniowaniu
sposobu organizacji i dostępu do zestawu danych. *Algorytm* to
procedura, która jest zdefiniowana i używana w celu wykonania pewnych
zadań na strukturze danych, w skończonej i obliczalnej ilości czasu
i/lub przestrzeni.

Czas i przestrzeń to zasoby, z których korzysta każdy model
obliczeniowy. Chcemy klasyfikować struktury danych i algorytmy jako
dobre lub złe, w zależności od ilości zasobów zużytych podczas obliczeń;
dlatego jesteśmy zainteresowani opracowaniem precyzyjnych narzędzi
analitycznych, które rejestrują czas działania i wykorzystanie
przestrzeni dla każdego algorytmu.

Ze względów teoretycznych czas i przestrzeń są ze sobą powiązane, a czas
jest uważany za najciekawszy zasób w informatyce. Wiele czynników może
wpływać na złożoność czasową algorytmu (środowisko sprzętowe, system
operacyjny, język programowania itp.), ale ważne jest, aby
scharakteryzować czas działania algorytmu jako funkcję rozmiaru danych
wejściowych. Wszystkie inne czynniki są równe, złożoność algorytmu jest
oceniana jako relacja między wielkością danych wejściowych a czasem
działania samego algorytmu.

W dalszej części przedstawimy kilka narzędzi, które pozwolą
przeprowadzić eksperymentalne badania złożoności algorytmu oraz
pokażemy, że stosowanie eksperymentów do oceny efektywności algorytmu ma
kilka ograniczeń. Następnie skupimy się na opracowaniu kilku narzędzi
matematycznych, które można wykorzystać do wyrażenia zależności między
czasem działania algorytmu a wielkością jego danych wejściowych.

Podrozdział 2: Eksperymentalna analiza algorytmów
-------------------------------------------------------------

Łatwym sposobem badania złożoności algorytmu jest wykonanie go na
różnych istotnych danych wejściowych testowych i rejestrowanie czasu
spędzonego podczas każdego wykonania. W Pythonie można to zrobić za
pomocą funkcji ``time()`` modułu czasu. Ta funkcja zgłasza liczbę sekund,
które upłynęły od czasu testu porównawczego. Jeśli ``time()`` zostanie
włączone bezpośrednio przed i po uruchomieniu algorytmu - jesteśmy w
stanie zmierzyć upływ czasu jako różnicę tych dwóch wartości.

Pomiar czasu w poprzedni sposób jest dobrym odzwierciedleniem złożoności
algorytmu, ale w żadnym wypadku nie jest doskonały. Upływający czas
będzie zależał na przykład od innych procesów uruchomionych na
komputerze podczas wykonywania testu (ponieważ jednostka centralna jest
współdzielona przez różne procesy): zamiast tego uczciwa miara powinna
uwzględniać statystycznie istotne testy powtarzane na identycznych
systemach komputerowych, z identycznymi danymi wejściowymi i
algorytmami. Wynik tych testów można zwizualizować na wykresie (wielkość
wejścia x czas działania), zapewniając pewną intuicję dotyczącą relacji
między wielkością wejścia a czasem wykonania algorytmu. Może to
prowadzić do zrozumienia najlepszej funkcji wielkości wejściowej danych
eksperymentalnych.

Chociaż eksperymentalna analiza czasów działania jest cenna, zwłaszcza
podczas dostrajania kodu o jakości produkcyjnej, istnieją pewne
ograniczenia:

1. Eksperymentalne czasy działania dwóch algorytmów są trudno
   porównywalne, chyba że eksperymenty są wykonywane w tym samym
   środowisku sprzętowym i programowym.
2. Dostępny jest tylko ograniczony zestaw wejść testowych.
3. W celu eksperymentalnego zbadania czasu działania algorytmu, ten
   pierwszy musi być w pełni zaimplementowany (to znaczy - musi być w
   pełni przetłumaczony na konkretny program).

To poważne wady wykorzystania badań eksperymentalnych.

Podrodział 3: Formalna analiza algorytmów
--------------------------------------------------

Terminem analiza formalna określamy podejście do analizy złożoności
algorytmów w sposób niezależny od środowiska sprzętowego i programowego,
badanie wysokopoziomowego opisu algorytmu (zamiast kompletnej
implementacji) oraz uwzględnienie wszystkich możliwych danych
wejściowych. W tym celu definiujemy zbiór operacji pierwotnych i
przeprowadzamy analizę kombinacji operacji tworzących algorytm (albo w
postaci rzeczywistego fragmentu kodu, albo pseudokodu niezależnego od
języka).

Zestaw operacji prymitywnych obejmuje:

-  dostęp do obiektu w celu przypisania lub odczytania;
-  operacje arytmetyczne lub porównanie;
-  wywołanie funkcji lub metody;
-  dostęp do listy według indeksu;
-  wracanie z funkcji.

Każda pierwotna operacja odpowiada instrukcji niskiego poziomu ze stałym
czasem wykonania. Zamiast mierzyć czas wykonania algorytmu, liczymy ile
prymitywnych operacji jest wykonywanych i używamy tej liczby jako miary
czasu działania algorytmu (zakładamy, że liczba operacji będzie
skorelowana z rzeczywistym czasem działania w określonym komputer,
zakładając, że każda operacja pierwotna odpowiada stałej liczbie
instrukcji i że istnieje tylko stała liczba operacji pierwotnych).

Aby uchwycić wzrost czasu działania algorytmu, skojarzymy z każdym
algorytmem funkcję \\(f(n)\\), która charakteryzuje liczbę wykonywanych
operacji pierwotnych w zależności od rozmiaru wejściowego (n).

Niektóre funkcje są szeroko stosowane w ramach złożoności obliczeniowej:

1. funkcja stała \\(f(n)=c\\), dla stałej liczby \\(c\\);
2. funkcja logarytmiczna \\(f(n)=log_b n\\), dla stałej liczby \\(b\\);
3. funkcja liniowa \\(f(n)=n\\);
4. funkcja \\(f(n)= n log n \\);
5. funkcja kwadratowa \\(f(n)=n^2\\);
6. funkcja wielomianowa \\(f(n)=n^k\\), dla stałej liczby \\(k\\);
7. funkcja wykładnicza \\(f(n)=b^n\\), dla stałej liczby \\(b\\).

Czas na kilka wskazówek dotyczących tych funkcji.

**Funkcja stała**

Dla dowolnego argumentu \\(n\\) funkcja stała zwraca wartość \\(c\\). Choć jest
to proste rozwiązanie, funkcja ta jest przydatna w analizie algorytmów,
ponieważ wyraża liczbę kroków potrzebnych do wykonania podstawowej
operacji na komputerze, takiej jak dodanie dwóch liczb, przypisanie
wartości do jakiejś zmiennej lub porównanie dwóch liczb. Stała złożoność
to najniższa wartość, jaką algorytm może osiągnąć.

**Funkcja logarytmiczna**

Ta funkcja jest zdefiniowana jako \\(x = log_b n\\), wtedy i tylko wtedy,
gdy \\(b^x = n\\). Z definicji \\(log_b 1 = 0\\). Wartość stała \\(b\\) nazywana
jest podstawą logarytmu. Najpopularniejszą podstawą funkcji
logarytmicznej w informatyce jest 2, ponieważ komputery reprezentują
liczby całkowite w notacji binarnej, a powszechną operacją w wielu
algorytmach jest wielokrotne dzielenie danych wejściowych na pół.
Podstawę pomijamy w notacji, gdy wynosi 2, co oznacza, że \\(log n =
log_2 n\\). Logarytmiczna złożoność jest typowa dla tych rekurencyjnych
algorytmów, które działają na coraz mniejszych częściach danych
wejściowych.

**Funkcja liniowa**

Biorąc pod uwagę wartość wejściową \\(n\\), funkcja liniowa zwraca samą
wartość \\(n\\). Funkcja ta jest spełniona w analizie algorytmów za każdym
razem, gdy wykonujemy pojedynczą operację podstawową dla każdego z
elementów \\(n\\). Na przykład porównanie liczby \\(x\\) z każdym elementem
sekwencji o rozmiarze \\(n\\) wymaga porównań \\(n\\). Funkcja liniowa
reprezentuje najlepszy czas działania dowolnego algorytmu
przetwarzającego obiekty, które nie znajdują się jeszcze w pamięci
komputera (odczyt obiektów \\(n\\) wymaga operacji \\(n\\). Ogólnie, algorytmy o
liniowej złożoności są uważane za najbardziej wydajne.

**Funkcja (nlog n)**

Ta funkcja przypisuje do wejścia \\(n\\) wartość \\(n\\) pomnożoną przez
logarytm o podstawie dwa z \\(n\\). Tempo wzrostu tej funkcji jest
oczywiście bardziej ostre niż funkcji liniowej, ale nadal pozostaje
poniżej funkcji kwadratowe. Dlatego też algorytm o czasie działania
proporcjonalnym do \\(n log n\\) musi być preferowanym w stosunku do
algorytmu z kwadratowym czasem działania. Kilka ważnych algorytmów
wykazuje czas działania proporcjonalny do tej funkcji. Na przykład
najlepsze algorytmy sortowania \\(n\\) dowolnych wartości wymagają czasu
proporcjonalnego do \\(nlog n\\).

**Funkcja kwadratowa**

Biorąc pod uwagę wartość wejściową \\(n\\), ta funkcja zwraca iloczyn \\(n^2\\).
Pojawia się w analizie algorytmów w przypadku napotkania pętli
zagnieżdżonych, w których pętla wewnętrzna wykonuje liniową liczbę
operacji, a pętla zewnętrzna jest wykonywana liniową liczbę razy. W
takich przypadkach algorytm wykonuje operacje \\(n\\) x \\(n = n^2\\).
Przykładowo, niektóre algorytmy sortowania mają kwadratową złożoność.

**Funkcja wielomianowa**

Kontynuując dyskusję na temat funkcji będących potęgami danych
wejściowych, rozważamy funkcję wielomianową \\(f(n) = n^k\\), gdzie \\(k\\) jest
stałą liczbą naturalną. Ta funkcja przypisuje wartości wejściowej \\(n\\)
iloczyn \\(n\\) z nią samą, \\(k\\) razy. Funkcja ta jest bardzo powszechna w
kontekście analizy algorytmów i stanowi, z przyczyn teoretycznych,
granicę tego, co jest możliwe do wykonania z komputerem. Musimy jednak
zauważyć, że algorytm z wielomianowym czasem działania, takim jak \\( n^{354} \\) *nie jest* uważany za wykonalny.

**Funkcja wykładnicza**

Inną funkcją wykorzystywaną w analizie algorytmów jest funkcja
wykładnicza \\(f(n)=b^n\\), gdzie \\(b\\) jest stałą dodatnią, zwaną bazą. Ta
funkcja przypisuje do argumentu wejściowego (n) wartość uzyskaną przez
pomnożenie podstawy \\(b\\) przez samą \\(n\\) razy. Podobnie jak w przypadku
funkcji logarytmicznej, najczęstszą podstawą funkcji wykładniczej w
analizie algorytmów jest 2. Na przykład, jeśli mamy pętlę, która zaczyna
się od wykonania jednej operacji, a następnie podwaja liczbę operacji
wykonywanych w każdej iteracji, to liczba operacje wykonywane w \\(n-\\)-tej
iteracji to \\(2^n\\). Funkcja wykładnicza i każda funkcja, która rośnie
podobnie, jest uważana za niewykonalną w informatyce. Jeśli uda się
udowodnić, że algorytm w najlepszym przypadku ma złożoność wykładniczą,
algorytm ten powinien być rzadko wykonywany w komputerze, niezależnie od
tego, jak sprawny obliczeniowo jest sprzęt. Wynika to z niezwykle
stromego rozwoju funkcji. Większość otwartych problemów w informatyce
jest w jakiś sposób związana z wykładniczą złożonością.

Formalna analiza złożoności algorytmu wymaga, aby czas działania był
wyrażony jako funkcja wielkości wejściowej. Zwykle pod uwagę brane są
najlepsze, najgorsze i przeciętne scenariusze.

Najlepszy scenariusz ma miejsce, gdy konfiguracja wejściowa jest taka,
że algorytm działa z najlepszą szybkością. Na przykład sortowanie
kolekcji danych, które są już posortowane, powinno wymagać minimalnej
ilości czasu. Najgorszy scenariusz ma miejsce, gdy algorytm musi wykonać
każdy możliwy krok, aby osiągnąć swój cel. Wyobraź sobie zbiór liczb,
które należy posortować od najmniejszej liczby, a zamiast tego są
sortowane od największej. Te dwa przypadki są o wiele łatwiejsze do
oceny niż przeciętny przypadek. W takim wypadku powinniśmy być w stanie
wyrazić czas działania algorytmu jako funkcję wielkości wejściowej,
uzyskanej przez pobranie średniej ze wszystkich możliwych danych
wejściowych o tej samej wielkości. Wymaga to zdefiniowania
probabilistycznego rozkładu wejść na wejściach i wykracza poza naszą
motywację tutaj. Zawsze rozważamy czasy działania w kategoriach
najgorszego przypadku, jako funkcję wielkości wejściowej algorytmu.

Podrozdział 4: Analiza asymptotyczna
--------------------------------------------

Zostało już przedstawione, że w analizie algorytmów należy
scharakteryzować złożoność czasową za pomocą funkcji, które odwzorowują
rozmiar danych wejściowych, \\(n\\), na wartości odpowiadające głównym
czynnikom, które zapewniają tempo wzrostu pod względem \\(n\\).

Poniżej przedstawimy kilka notacji, które wyrażają tę relację.

Każdy podstawowy krok w opisie algorytmu (jak również w jego
implementacji) odpowiada ustalonej liczbie operacji prymitywnych, a my
chcemy policzyć liczbę wykonanych operacji prymitywnych do stałego
współczynnika.

W poniższym fragmencie kodu funkcja find_max szuka największego elementu
danych listy Pythona:


.. code:: ipython3

     def find_max(data):
            # Return the maximum element from a list
            biggest = data[0]
            for val in data:
                if val > biggest:
                    biggest = val
            return biggest

Ten algorytm działa proporcjonalnie do (n), ponieważ pętla jest
wykonywana raz dla każdego elementu danych, a dla każdego przebiegu
wykonywana jest stała liczba operacji pierwotnych (porównanie i
przypisanie).



**Notacja \\( O \\).**

Niech \\(f\\) i \\(g\\) będą funkcjami odwzorowującymi dodatnie liczby całkowite
na dodatnie liczby rzeczywiste. Mówimy, że \\(f(n)\\) to \\( O(g(n)) \\), jeśli
istnieje rzeczywista (c > 0) i liczba całkowita \\(n_0 \geq 1\\) taka, że \\(f(n) \leq cg(n)\\), dla \\(n \geq n_0\\). Notacja ta jest wymawiana jako \\(f(n)\\) to
„duże-Oh” z \\(g(n)\\) lub \\(f(n)\\) „jest porządkiem” \\(g(n)\\) lub \\(f(n\\)
\\(f(n) \ in O(g(n))\\); funkcja \\(f\\) jest „mniejsza lub równa” innej
funkcji \\(g\\) aż do współczynnika stałego i w sensie asymptotycznym, gdy
\\(n\\) rośnie w kierunku nieskończoności.

Notacja \\(O\\) służy do scharakteryzowania czasów działania i granic
przestrzeni algorytmów pod kątem jakiegoś parametru \\(n\\), który zawsze
jest mierzony jako miara rozmiaru problemu. Na przykład, jeśli \\(n\\)
oznacza liczbę elementów danych sekwencji w poprzednim kodzie dla
find_max, powiemy, że algorytm ma czas działania \\(O(n)\\). Dzieje się tak,
ponieważ przypisanie przed pętlą wymaga stałej liczby operacji
pierwotnych i to samo dzieje się z każdą iteracją pętli. W końcu pętla
wykonuje \\(n\\) razy. Biorąc pod uwagę, że każda pierwotna operacja działa
w stałym czasie, mamy, że czas działania algorytmu na wejściu o
rozmiarze \\(n\\) jest co najwyżej stałym czasem \\(n\\), czyli \\(O(n) \\).

**Notacja \\(Omega \\).**

Niech \\(f\\) i \\(g\\) będą funkcjami odwzorowującymi dodatnie liczby całkowite
na dodatnie liczby rzeczywiste. Mówimy, że (f(n)) to (\\(Omega (g(n)) \\), jeśli \\(g(n)\\) to \\(O(f(n))\\), czyli istnieje rzeczywista \\(c > 0\\) i liczba całkowita \\(n_0 \geq 1\\) taka, że \\(f(n) \geq cg(n)\\), dla \\(n \geq n_0\\). Ten zapis jest wymawiany \\(f(n)\\) to „duża Omega” z \\(g(n)\\). Funkcja \\(f\\) jest „większa niż równa” funkcji \\(g\\), aż do współczynnika stałego w sensie asymptotycznym.

**Notacja \\(Theta \\).**

Niech \\(f\\) i \\(g\\) będą funkcjami mapującymi dodatnie liczby całkowite na
dodatnie liczby rzeczywiste. Mówimy, że \\(f(n)\\) to \\(Theta (g(n))\\), jeśli \\(f(n)\\) to \\(O(g(n))\\) i \\(f (n)\\) to \\(Omega (g(n))\\), czyli istnieją liczby rzeczywiste \\(c' > 0\\) i \\(c’’ > 0\\), a liczba całkowita \\(n_0 \geq 1 \\) tak, że \\(c'g(n) \leq f(n) \leq c''g(n)\\), dla \\(n \geq n_0\\). Oznacza to, że te dwie funkcje rosną w tym samym tempie.

**Analiza porównawcza.**

Notacja \\(O\\) może być użyta do uporządkowania klas funkcji według
asymptotycznego tempa wzrostu. Wprowadzonych wcześniej siedem funkcji
można uporządkować według rosnącego tempa wzrostu w następującej
kolejności: \\(1\\), \\(\log n\\), \\(n\\), \\(n \log n\\), \\(n^
k\\), \\(2^n\\). Oznacza to, że jeśli w sekwencji \\(f(n)\\) poprzedza \\(g(n)\\), to
\\(f(n)\\) jest \\(O(g(n))\\). Na przykład, jeśli dwa algorytmy rozwiązują ten
sam problem w czasie wykonywania odpowiednio \\(O(n)\\) i \\(O(n^2)\\), to
pierwszy jest asymptotycznie lepszy niż drugi, aż do pewnej wartości
\\(n\\).

Interesujące jest porównanie tempa wzrostu funkcji siódmej funkcji, jak
to przedstawia poniższa tabela.

======= ============= === ============== ========= ========== =============
\\(n\\) \\(\log n\\)  n   \\(n \log n\\) \\(n^2\\) \\(n^3\\)  \\(2^n\\)
======= ============= === ============== ========= ========== =============
8       3             8   24             64        512        256
16      4             16  64             256       4,096      65,536
32      5             32  160            1,024     32,768     4,294,967,296
64      6             64  384            4,096     262,144    1.84×10^19
128     7             128 896            16,384    2,097,152  3.40×10^38
256     8             256 2,048          65,536    16,777,216 1.15×10^77
======= ============= === ============== ========= ========== =============


W poniższej tabeli przedstawiono maksymalny rozmiar dozwolony dla
wystąpienia wejściowego, które jest przetwarzane przez algorytm w ciągu
1 sekundy, 1 minuty i 1 godziny, pokazując, że asymptotycznie wolniejszy
algorytm w dalekiej perspektywie jest wolniejszy od asymptotycznie
szybszego algorytmu, nawet jeśli współczynniki stałe przy szybszym
algorytmie są gorsze.

+------------------------------+-----------------------------------+
|                              | Maximum problem size ( \\(n\\) )  |
+------------------------------+------------+----------+-----------+
| Running time \\( \\mu\\)-sec | 1 second   | 1 minute | 1 hour    |
+==============================+============+==========+===========+
| \\(400n\\)                   | 2,500      | 150,000  | 9,000,000 |
+------------------------------+------------+----------+-----------+
| \\(2n^2\\)                   | 707        | 5,477    | 42,426    |
+------------------------------+------------+----------+-----------+
| \\(2^n\\)                    | 19         | 25       |    31     |
+------------------------------+------------+----------+-----------+



Poniższa tabela pokazuje nowy maksymalny rozmiar problemu możliwy do
osiągnięcia przez dowolny ustalony czas, przy założeniu, że algorytmy z
podanymi czasami działania są teraz uruchamiane na komputerze 256 razy
szybciej niż poprzednie. Każdy wpis jest funkcją m, poprzedniej
maksymalnej wielkości problemu. Nawet jeśli używamy szybszego sprzętu,
to nadal nie możemy przezwyciężyć ułomności asymptotycznie wolniejszego
algorytmu.

+------------------------------+---------------------------+
| Running time \\( \mu\\)-sec | New maximum problem size  |
+==============================+===========================+
| \\(400n\\)                   | \\(256m\\)                |
+------------------------------+---------------------------+
| \\(2n^2\\)                   | \\(16m\\)                 |
+------------------------------+---------------------------+
| \\(2^n\\)                    | \\(m+8\\)                 |
+------------------------------+---------------------------+



Zauważmy, że użycie notacji \\(O\\) może być mylące, jeśli współczynniki
stałe, które ukrywają, są bardzo duże. Rozważmy funkcję taką jak
\\(10^{100} n\\), która jest liniowa, ale zdecydowanie niewykonalna. Dzieje
się tak, ponieważ stały współczynnik \\(10^{100}\\) jest zbyt duży, aby mógł
go obsłużyć dowolny komputer. Nie rozciągając naszego przykładu do tej
granicy, powinniśmy wziąć pod uwagę, że czynniki stałe i wyrażenia
niższego rzędu zawsze kryją się w złożoności algorytmu. „Szybki”
algorytm, mówiąc ogólnie, to dowolny algorytm działający w czasie
\\(O(nlog n)\\) (z rozsądnym współczynnikiem stałym). W pewnym kontekście
złożoność czasu \\(O(n^2)\\) jest uważana za akceptowalną. Granica między
wydajnymi i nieefektywnymi algorytmami musi być wytyczona między tymi
algorytmami działającymi w czasie wielomianowym \\(O(n^k)\\) (z niskim \\(k\\))
a tymi działającymi w czasie wykładniczym \\(O(k^ n)\\). Rozróżnienie między
algorytmami czasu wielomianowego i czasu wykładniczego jest uważane za
solidną miarę wykonalności.

Podrozdział 5: Przykłady analizy algorytmów
----------------------------------------------------

W tej sekcji przedstawiamy kilka prostych algorytmów i pokazujemy
związaną z nimi analizę czasu działania, korzystając z wprowadzonych
wcześniej notacji. Użyjemy instancji klasy list Pythona, data, jako
reprezentacji tablicy wartości. Wywołanie funkcji len(data) jest
oceniane w stałym czasie, biorąc pod uwagę, że zmienna rejestrująca
długość listy jest utrzymywana dla każdego wystąpienia listy. W ten
sposób bezpośredni dostęp do tej zmiennej wykona zadanie w czasie
\\(O(1)\\). Co więcej, klasa list w Pythonie umożliwia dostęp do dowolnego
elementu listy (za pomocą data[j], dla każdego \\(j)\\) w stałym czasie.
Dzieje się tak, ponieważ listy Pythona są zaimplementowane jako
sekwencje oparte na tablicach, a odwołania do elementów listy są
przechowywane w kolejnym bloku pamięci. \\(j\\)-ty element listy można
znaleźć przy użyciu indeksu jako przesunięcia do tablicy bazowej.
Zauważ, że moglibyśmy użyć dowolnego języka programowania lub dowolnego
pseudojęzyka do opisania algorytmu, a analiza pozostanie taka sama.

Podrozdział 5.1: Znajdowanie maksimum
------------------------------------------------

Biorąc pod uwagę poprzednie rozważania dotyczące danych len i data[j],
możemy potwierdzić wstępną analizę, którą przeprowadziliśmy w
odniesieniu do algorytmu find_max, z (n) długością danych. Inicjalizacja
największego do data[0] zajmuje czas \\(O(1)\\). Pętla wykonuje \\(n\\) razy, a
w każdej iteracji wykonuje jedno porównanie i ewentualnie jedno
przypisanie. Na koniec instrukcja return w Pythonie używa czasu \\(O(1)\\).
Widzimy wówczas, że funkcja działa w czasie \\(O(n)\\), zgodnie z
oczekiwaniami.

Podrozdział 5.2: Średnia prefiksowa
------------------------------------------

Niech \\(S\\) będzie ciągiem \\(n\\) liczb. Celem jest obliczenie ciągu \\(A\\),
takiego, że \\(A[j]\\) jest średnią elementów \\(S[0], \ldots ,S[j]\\), dla \\(j = 0 , \ldots ,n-1\\). Ten
problem nazywa się średnimi prefiksów sekwencji i ma on kilka
zastosowań. Na przykład, biorąc pod uwagę roczne zwroty funduszu
inwestycyjnego, inwestor może chcieć zobaczyć średnie roczne zwroty
funduszu z ostatnich lat. Podobnie, biorąc pod uwagę dzienniki użycia
sieci Web, menedżer witryny sieci Web może chcieć śledzić średnie trendy
użytkowania w różnych okresach czasu.

Pierwsza implementacja algorytmu obliczania średnich prefiksów jest
podana w poniższym kodzie. Każdy element \\(A\\) jest obliczany osobno,
używając pętli do obliczenia sumy częściowej:


.. code:: ipython3

    def prefix_average(S):
            # Return list A such that A[j] equals average of S[0], ..., S[j], for all j
            n = len(S)
            A = [0]*n                # list of n zeros
            for j in range(n):
                total = 0
                for i in range(j + 1):
                    total += S[i]
                A[j] = total / (j+1)    # the j-th average
            return A

Przypisanie n = len(S) jest wykonywane w stałym czasie. Instrukcja (\\A =
[0] * n\\) tworzy i inicjalizuje listę o długości \\(n\\), ze wszystkimi wpisami
równymi zero. Wykorzystuje to stałą liczbę operacji pierwotnych na wpis,
a zatem działa w czasie \\(O(n)\\). Ciało zewnętrznej pętli jest
kontrolowane przez licznik \\(j\\) i jest wykonywane \\(n\\) razy, dla \\(j = 0, \ldots ,n-1\\). Oznacza to, że instrukcje \\(total = 0\\) i \\(A[j] = total / (j+1)\\) są wykonywane \\(n\\) razy każda. Przyczyniają się do wielu operacji pierwotnych proporcjonalnych do \\(n\\), czyli \\(O(n)\\) czasu. Treść pętli wewnętrznej, która
jest kontrolowana przez licznik \\(i\\), jest wykonywana \\(j+1\\) razy, w zależności od bieżącej wartości licznika pętli zewnętrznej \\(j\\). Zatem instrukcja \\(total += S[i]\\) jest wykonywana \\((1+2+3+ \ldots +n \\) razy, co równa się \\(n(n+1)/2\\). Oznacza to czas \\(O(n^2)\\). Czas wykonania tej implementacji jest sumą trzech wyrazów: pierwszy i drugi wyraz to \\(O(n)\\), a trzeci wyraz to \\(O(n^2)\\). Całkowity czas działania *prefix_average* wynosi \\(O(n^2)\\).

Algorytm czasu liniowego dla tego samego problemu jest podany w
poniższym kodzie:
Algorytm czasu liniowego dla tego samego problemu jest podany w
poniższym kodzie:

.. code:: ipython3

      def prefix_average_linear(S):
            # Return list A such that A[j] equals average of S[0], ..., S[j], for all j
            n = len(S)
            A = [0]*n                # list of n zeros
            total = 0
            for j in range(n):
                total += S[j]
                A[j] = total / (j+1)    # average based on current sum
            return A

W pierwszym algorytmie suma prefiksów jest obliczana od nowa dla każdej
wartości \\(j\\), co prowadzi do równiania kwadratowego. W tym nowym
algorytmie dynamicznie utrzymujemy bieżącą sumę prefiksów, efektywnie
obliczając \\(S[0]+S[1]+ \ldots +S[j]\\) jako \\(sumę += S[j]\\),
gdzie suma jest równa suma \\(S[0]+S[1]+ \ldots +S[ j-1]\\)
obliczona przez poprzedni przebieg pętli przez (j). Inicjalizacja
zmiennych wykorzystuje czas \\(O(1)\\). Inicjalizacja listy wykorzystuje
czas \\(O(n)\\). Istnieje pojedyncza pętla, kontrolowana przez licznik \\(j\\).
Utrzymanie tego licznika przez iterator zakresu kosztuje (O(n)) czas.
Ciało pętli jest wykonywane \\(n\\) razy, dla \\(j = 0, \ldots ,n-1\\). W ten sposób instrukcje \\(total += S[j]\\) i \\(A[j] = total / (j+1)\\) są wykonywane \\(n\\) razy. Ponieważ każda z tych instrukcji
używa czasu \\(O(1)\\) na iterację, ich całkowity wkład wynosi czas \\(O(n)\\).
Całkowity czas działania algorytmu prefix_average_linear wynosi \\(O(n)\\),
co jest znacznie lepsze niż czas kwadratowy pierwszego dostarczonego
przez nas algorytmu

Podrozdział 5.3: Rozłączność zbiorów trójdzielnych (HELP)
---------------------------------------------------------------------


Biorąc pod uwagę trzy ciągi liczb \\(A\\), \\(B\\) i \\(C\\) oraz to, że żaden
pojedynczy ciąg nie zawiera zduplikowanych wartości, problem
rozłączności zbiorów trójdzielnych polega na określeniu, czy przecięcie
trzy sekwencje są puste. Prosta implementacja Pythona jest podana
poniżej:

.. code:: ipython3

        def disjoint1(A, B, C):
            # Return True if there is no element in common to all three lists
            for a in A:
                for b in B:
                    for c in C:
                        if a == b == c:
                            return False    # there is a common value
            return True             # sets are disjoint


Ten algorytm zapętla każdą możliwą trójkę wartości z trzech zestawów,
aby sprawdzić, czy wartości te są równe. Jeśli każdy zestaw ma rozmiar
\\(n\\), to złożoność czasowa najgorszego przypadku wynosi \\(O(n^3)\\).
Zauważmy, że jeśli dwa wybrane elementy z \\(A\\) i \\(B\\) nie pasują do
siebie, nie ma sensu sprawdzanie dopasowania z wartościami \\(C\\). W związku
z tym poniższy kod daje ulepszoną wersję:

.. code:: ipython3

        def disjoint2(A, B, C):
            # Return True if there is no element in common to all three lists
            for a in A:
                for b in B:
                    if a == b:           # check C only if there is a match from A and B
                        for c in C:
                            if a == c:        # here a == b == c
                                return False   # there is a common value
            return True             # sets are disjoint


Pętla \\(A\\) wymaga czasu \\(O(n)\\). Pętla \\(B\\) odpowiada łącznie \\(O(n^2)\\)
czasowi, ponieważ ta pętla jest wykonywana \\(n\\) różne razy. Następnie
test \\(a == b\\) jest obliczany \\(O(n^2)\\) razy. Należy wziąć pod uwagę \\((n)x(n)\\)
pary \\((a,b)\\), ale przy poprzednim założeniu, że żadna pojedyncza
sekwencja nie zawiera zduplikowanych wartości, może być co najwyżej \\(n\\)
par z równym \\(b\\). Dlatego najbardziej wewnętrzna pętla \\(C\\) i polecenia w
jej ciele, używają co najwyżej \\(O(n^2)\\) czasu. Całkowity czas wykonania
to \\(O(n^2)\\).


Podrozdział 5.4: Unikalność elementu
--------------------------------------------


Mając pojedynczą sekwencję \\(S\\) z elementami \\(n\\), muismy zadecydować czy
wszystkie elementy tej kolekcji różnią się od siebie. Pierwsze
rozwiązanie tego problemu wykorzystuje prosty algorytm iteracyjny, który
sprawdza czy wszystkie odrębne pary indeksów \\( (j,k) \\), z \\(j < k\\),
odnoszą się do elementów, które są równe:


.. code:: ipython3

        def unique1(S):
            # Return True if there are no duplicate elements in sequence S
            for j in range(len(S)):
                for k in range(j+1, len(S)):
                    if S[j] == S[k]:
                        return False        # there is a duplicate pair
            return True               # all elements are distinct
    


Pierwsza iteracja pętli zewnętrznej powoduje \\(n-1\\) iteracji pętli
wewnętrznej, druga iteracja pętli zewnętrznej powoduje \\(n-2\\) iteracje
pętli wewnętrznej i tak dalej. Zatem najgorszy czas działania tej
funkcji jest proporcjonalny do \\((n-1)+(n-2)+ \ldots +2+1\\),
czyli \\(O(n^2)\\). Zauważmy, że jeśli sekwencja elementów zostanie
posortowana, wszystkie zduplikowane elementy zostaną umieszczone obok
siebie. Tak więc wszystko, co należy zrobić, to wykonać jednokrotne
przeszukanie posortowanej sekwencji, szukając kolejnych duplikatów.
Implementacja tego algorytmu w Pythonie wygląda następująco:


.. code:: ipython3

        def unique2(S):
            # Return True if there are no duplicate elements in sequence S
            temp = sorted(S)            # sorting of S
            for j in range(1, len(temp)):
                if temp[j-1] == temp[j]:
                    return False            # there is duplicate pair
            return True                 # all elements are distinct


Funkcja *sorted* zwraca kopię oryginalnej listy z elementami w
kolejności rosnącej. Średni czas działania przypadku to
\\(O(n \log n)\\). Kolejna pętla działa w czasie \\(O(n)\\), a więc
cały algorytm działa w czasie \\(O(n \log n)\\).

Podrozdział 6: Podejście matematyczne - niezmienniki funkcji
------------------------------------------------------------------------

W poprzednich rozdziałach omówione zostało kilka przykładów oceny
złożoności algorytmów. Podejście jest w zasadzie zawsze takie samo:
pisany jest algorytm (za pomocą pseudokodu lub języka programowania), a
ocena podstawowych (stałych) operacji i zagnieżdżonych pętli
występujących w algorytmach pozwala ustalić związek między wielkość
danych wejściowych i liczba wykonanych operacji. Innymi słowy, jesteśmy
w stanie znaleźć „zgodność” co do czasochłonności algorytmu, ale to nie
oznacza, że przedstawiliśmy formalny dowód tego ograniczenia
czasowego.

Jedną z technik stosowanych w dziedzinie analizy złożoności jest tzw.
*niezmiennik pętli*. Technika ta jest podobna, jeśli nie równa, do
techniki matematycznej zwanej *indukcją*. Wyobraźmy sobie, że chcemy
udowodnić, że zdanie \\(L\\) dotyczące pętli jest poprawne - definiujemy \\(L\\)
w kategoriach mniejszych stwierdzeń \\(L_0, L_1, \ldots ,L_k\\), przy czym:

-  początkowe twierdzenie \\(L_0\\) jest prawdziwe przed rozpoczęciem pętli;
-  jeśli \\(L_{j−1}\\) jest prawdziwe przed iteracją \\(j\\), to \\(L_j\\) będzie
   prawdziwe po iteracji \\(j\\);
-  stwierdzenie końcowe \\(L_k\\) oznacza, że stwierdzenie \\(L\\) jest
   prawdziwe.

Mając następujący kod funkcji *find*, która powinna znaleźć najmniejszy
indeks, przy którym występuje element *val* w sekwencji *Seq*, używamy
niezmiennika pętli, aby udowodnić jego poprawność:


.. code:: ipython3

        def find(Seq, val):
            # Return index j such that Seq[j] == val, -1 otherwise
            n = len(Seq)
            j = 0
            while j < n:
                if Seq[j] == val:
                    return j
                j += 1
            return -1
    


Aby pokazać, że *find* jest poprawne, definiujemy serię instrukcji
\\(L_j\\), po jednym dla każdej iteracji \\(j\\) pętli while:

\\(L_j\\): *val* różni się od któregokolwiek z pierwszych \\(j\\) elementów
*Seq*.

To twierdzenie jest prawdziwe w przypadku \\(j=0\\), czyli w pierwszej
iteracji pętli, ponieważ wśród pierwszych 0 w *Seq* nie ma żadnych
elementów. W \\(j\\)-tej iteracji element *val* jest porównywany z *Seq[j]*,
a \\(j\\) jest zwracany, jeśli dwa elementy są równe. Jeśli te dwa elementy
nie są równe, to jest jeszcze jeden element nierówny *val*, a indeks \\(j\\)
jest zwiększany. Zatem twierdzenie \\(L_j\\) jest prawdziwe dla nowej
wartości \\(j\\). Stąd też jest to prawdą na początku następnej iteracji.
Jeśli pętla while kończy się bez zwracania indeksu w *Seq*, to mamy \\(j =
n\\). Oznacza to, że \\(L_n\\) jest prawdziwe, ponieważ nie ma elementów *Seq*
równych *val*. Dlatego algorytm poprawnie zwraca −1.



Rozdział 2: Rekurencja
**********************


Podrozdział 1: Wstęp
--------------------

Pętle i iteracje (takie jak pętle for- i while-) są najczęstszym
sposobem implementacji koncepcji powtarzania zadań w Pythonie.
Alternatywnym sposobem, bliższym wewnętrznej naturze funkcji, jest
rekurencja.

Słowo „rekurencyjny” pochodzi od łacińskiego czasownika recurrere,
oznaczającego “przybiec z powrotem”. Najczęściej używanym i nadużywanym
przykładem definicji rekurencyjnej jest funkcja silni, którą można
zdefiniować jako \\(n! = n * (n-1)!\\), jeśli \\(n > 1\\) i  \\(0! = 1\\). Na
przykład \\(4!=4*3!=4*3*2!=4*3*2*1!=4*3*2*1*0!=24\\). Na
przykład: wzory fraktalne, lalki Matrioszki są rekurencyjne - każda
lalka jest wykonana z drewna lub zawiera inną lalkę.

Ogólnie rzecz biorąc, funkcję (lub program) nazywa się rekurencyjną, gdy
wywołuje siebie w swoim ciele, zwracając wartość tego wywołania funkcji.
Każde wywołanie musi być zastosowane na mniejszej wersji danych
wejściowych, idąc w kierunku przypadku podstawowego (przypadek, w którym
funkcja może być obliczona bez dalszej rekurencji). W poprzednim
przykładzie, gdy obliczenia trafią w przypadek \\(0!\\), wartość \\(1\\) jest
zwracana do poprzedniego wywołania.

Definicja silni w Pythonie jest następująca::

 1  def factorial(n):
 2    if n == 0:
 3      return 1
 4    else:
 5      return n * factorial(n-1)
 

Jak odbiorca może zauważyć, ``factorial(n)`` wywołuje ``factorial(n-1)`` i tak dalej,
aż do osiągnięcia przypadku bazowego \\(0!\\). Ta funkcja nie używa żadnych
wyraźnych pętli, a powtarzanie jest zapewniane przez rekurencyjne
wywołania silni. Za każdym razem, gdy funkcja jest wywoływana, jej
argument jest mniejszy o jeden, a po osiągnięciu przypadku bazowego
zwracana jest liczba całkowita \\(1\\).

Aby śledzić zachowanie rekurencji, możemy dodać kilka funkcji print() do
kodu::


    def factorial(n):
        print("factorial is called with n = " + str(n))
        if n == 1:
            return 1
        else:
            res = n * factorial(n-1)
            print("intermediate result for ", n, " * factorial(" ,n-1,"): ",res)
            return res
        print(factorial(4))

Ten skrypt daje następujące wyniki, gdy silnia jest wywoływana z
wejściem 4:

-  silnia jest wywoływana z n = 4
-  silnia jest wywoływana z n = 3
-  silnia jest wywoływana z n = 2
-  silnia jest wywoływana z n = 1
-  wynik pośredni dla 2 \* silnia( 1 ): 2
-  wynik pośredni dla 3 \* silnia ( 2 ): 6
-  wynik pośredni dla 4 \* silnia (3): 24

Program iteracyjny, który oblicza tę samą funkcję, to::


    def iterfactorial(n):
        res == 1
        for i in range(2,n+1):
            res *= i
        return res

Kilka języków programowania (takich jak Scheme lub Smalltalk) nie
obsługuje konstrukcji pętli i zamiast tego polega bezpośrednio na
rekurencji w celu wyrażenia powtórzeń. Większość języków programowania
obsługuje rekurencję przy użyciu tego samego mechanizmu, który jest
używany do obsługi wywołań funkcji: jeśli jedno wywołanie funkcji jest
rekurencyjne, to wywołanie jest zawieszane do czasu zakończenia
wywołania rekurencyjnego.

W Pythonie (i w większości języków programowania) za każdym razem, gdy
wywoływana jest funkcja, tworzona jest struktura znana jako rekord lub
ramka aktywacji w celu przechowywania informacji o wywołaniu funkcji.
Rekord aktywacji śledzi parametry wywołania funkcji, zmienne lokalne
oraz informacje o tym, które polecenie w ciele funkcji jest aktualnie
wykonywane. Gdy wykonanie funkcji prowadzi do kolejnego wywołania
funkcji, wykonanie poprzedniego wywołania zostaje zawieszone, a rekord
aktywacji przechowuje miejsce w kodzie źródłowym, do którego musi
powrócić przepływ sterowania po wykonaniu zagnieżdżonego wywołania.
Proces ten jest używany zarówno w standardowym przypadku jednej funkcji
wywołującej inną funkcję, jak i w przypadku rekurencyjnym, w którym
funkcja wywołuje samą siebie. W związku z tym dla każdego aktywnego
połączenia istnieje inny rekord aktywacji.

Rekurencja ma ogromne znaczenie w badaniu struktur danych i algorytmów.

Pozdrozdział 2: Przykłady algorytmów rekurencyjnych
---------------------------------------------------

Podrozdział 2.1: Wyszukiwanie binarne
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Wyszukiwanie binarne to algorytm rekurencyjny, który znajduje wartość
docelową wewnątrz posortowanej sekwencji elementów \\(n\\). Ta sekwencja
może być w Pythonie dowolną sekwencją indeksowaną, taką jak lista lub
tablica. Prostym podejściem do wyszukiwania wartości docelowej w
nieposortowanej sekwencji jest użycie algorytmu wyszukiwania
sekwencyjnego. Pętla służy do sprawdzania każdego elementu, dopóki nie
zostanie znaleziony cel lub zestaw danych zostanie wyczerpany. Ten
algorytm działa w czasie \\(O(n)\\) (tj. liniowym), ponieważ w najgorszym
przypadku każdy element musi zostać sprawdzony.

Gdy ``data`` sekwencji są sortowane i indeksowane, wyszukiwanie binarne jest
znacznie bardziej wydajne. Zauważ, że dla dowolnego indeksu \\(j\\)
wszystkie wartości przechowywane pod indeksami \\(0,\ldots,j-1\\) są mniejsze lub równe wartości pod indeksem \\(j\\), a wszystkie
wartości przechowywane pod indeksami \\(j+1,\ldots,n-1\\) są
większe lub równe wartości pod indeksem \\(j\\).

.. image:: https://bitbucket.org/ccinfinite/algorithms/downloads/figura23.png
    :height: 100px 
    :width: 200px 
    :scale: 10 
    :alt: alternate text 
    :align: center

Element sekwencji jest *“kandydatem”*, jeśli na obecnym etapie nie
możemy wykluczyć, że ten element pasuje do celu. Algorytm ma dwa
parametry, niski i wysoki, dzięki czemu wszyscy potencjalni kandydaci
mają indeks między niskim a wysokim. Początkowo \\(low = 0\\) i \\(high =n-1\\)
(czyli wszystkie elementy sekwencji są kandydatami).

Porównujemy wartość docelową z kandydatem mediany, to znaczy dane
``data[mid]`` o indeksie \\(mid = \lfloor(low+high)/2\rfloor\\). Mamy trzy przypadki:

-  cel jest równy ``data[mid]`` - wyszukiwanie powiodło się;
-  cel jest mniejszy niż ``data[mid]``, następnie powracamy w pierwszej
   połowie ciągu (na przedziale indeksów od niskiego do połowy-1);
-  cel jest większy niż ``data[mid]``, następnie powracamy w drugiej połowie
   ciągu (na przedziale indeksów od mid+1 do high).

Nieudane wyszukiwanie występuje, gdy \\(niski > wysoki\\). Należy zauważyć,
że podczas gdy wyszukiwanie sekwencyjne działa w czasie \\(O(n)\\),
wyszukiwanie binarne działa w czasie \\(O(log n)\\). Implementacja
wyszukiwania binarnego w Pythonie jest podana poniżej::


    def binary_search(data, target, low, high):
        # Return True if target is found in data, between low and high
        if low > high:
            return False               # no match found
        else:
            mid = (low + high) / 2
            if target == data[mid]:    # found a match
                return True
            elif target < data[mid]:
                return binary_search(data, target, low, mid - 1)   # call on the left portion
            else:
                return binary_search(data, target, mid + 1, high)  # call on the right portion

Na poniższym rysunku szukamy wartości 18.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura22.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center
   

Podrozdział 2.2: Odwracanie sekwencji
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Niech \\(A\\) będzie sekwencją indeksowaną. Rozważ problem odwrócenia
elementów \\(A\\): pierwszy element staje się ostatnim, drugi staje się
przedostatni i tak dalej. Zauważ, że możliwe jest odwrócenie sekwencji
poprzez zamianę pierwszego i ostatniego elementu oraz rekurencyjne
odwrócenie pozostałych elementów.

Implementacja Pythona jest podana poniżej::


    def reverse(A, first, last):
        # Reverse elements in A, between first and last
        if first < last - 1:                           # if there are at least 2 elements
            A[first], A[last-1] = A[last-1], A[first]    # swap first and last
            reverse(A, first+1, last-1)

Jeśli ``first == last`` -> nie ma sekwencji do odwrócenia. Jeśli ``first == last-1``, sekwencja składa się z jednego elementu.

W obydwu przypadkach osiągnęliśmy podstawowy przypadek rekurencji. Jeśli
istnieje sekwencja do odwrócenia, algorytm zatrzyma się po \\(1+\lfloor n/2\rfloor\\). Ponieważ każde
wywołanie wymaga stałej ilości pracy, algorytmy działają w czasie
(O(n)).

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura23.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Podrozdział 2.3: Sumowanie elementów sekwencji
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Niech \\(A\\) będzie sekwencją indeksowaną na liczbach całkowitych. Rozważ
problem sumowania elementów \\(A\\). Zauważ, że suma wszystkich \\(n\\) liczb
całkowitych w \\(A\\) jest sumą pierwszych \\(n-1\\) liczb całkowitych w \\(A\\)
plus jej ostatni element.

Jest to oczywiście definicja rekurencyjna i nazywa się rekurencją
liniową. Poniżej przedstawiamy kodowanie Pythona::


    def linear_sum(A, n):
        # Returns the sum of the first n numbers of A
        if n == 0:
            return 0
        else:
            return A[n-1] + linear_sum(A, n-1)

Dla danych wejściowych o rozmiarze \\(n\\) algorytm wykonuje wywołania
funkcji \\(n+1\\). Każde wywołanie wymaga stałej ilości pracy, a algorytm
działa w czasie \\(O(n)\\).

Inne podejście do rozwiązania tego problemu może być następujące:
zamiast sumować ostatni element \\(A\\) z sumą pozostałych elementów, możemy
obliczyć sumę pierwszej połowy \\(A\\) i suma drugiej połowy \\(A\\),
rekurencyjnie, a następnie dodać te liczby. Jest to przypadek rekurencji
binarnej, w której wykonywane są dwa wywołania rekurencyjne::

    def binary_sum(A, first, last):
        # Return the sum of the numbers in A between first and last
        if first <= last:     # no elements
            return 0
        elif first == last-1:     # one element
            return A[first]
        else:
            mid = (first + last) / 2
            return binary_sum(A, first, mid) + binary_sum(A, mid, last)

Aby przeanalizować złożoność tej wersji sumy, rozważymy przypadek, w
którym \\(n\\) jest potęgą 2. Przy każdym wywołaniu rekurencyjnym rozmiar
ciągu jest dzielony przez 2, co oznacza, że głębokość rekurencji jest
równa \\(1+\log n\\). Czas działania algorytmu nadal wynosi
\\(O(n)\\), ponieważ istnieją \\(2n-1\\) wywołania rekurencyjne, z których każde
wymaga stałego czasu. Ten sam wynik dotyczy sekwencji o dowolnej
długości.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura24.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center
   

Podrozdział 3: Rekurencja może być nieefektywna
-----------------------------------------------

W tej części przyjrzymy się niektórym problemom, w których algorytmy
rekurencyjne powodują drastyczną nieefektywność.


Podrozdział 3.1: Ciąg Fibonacciego
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sekwencja Fibonacciego została nazwana na cześć matematyka Leonarda da
Pisa, lepiej znanego jako Fibonacci. W swojej książce z 1200 roku „Liber
Abaci” wprowadził sekwencję jako ćwiczenie dotyczące sztucznej populacji
królików, spełniających następujące warunki: - populacja początkowa
składa się z nowo narodzonej pary królików, jednego samca, jednej
samicy; - króliki są w stanie kojarzyć się w wieku jednego miesiąca; -
króliki są nieśmiertelne; - para kryjąca zawsze produkuje jedną nową
parę (jeden samiec, jedna kobieta) co miesiąc, począwszy od drugiego
miesiąca.

Ciąg Fibonacciego \\(F_n\\) to liczby par królików po \\(n\\) miesiącach, czyli
po 10 miesiącach będziemy mieli \\( F_{10}\\) królików. Odkładając na bok
matematyczne znaczenie tego ciągu, zauważ, że liczby Fibonacciego można
zdefiniować jako \\(F_0 =0\\), lub \\(F_1 = 1\\), lub \\(F_n = F_{n-2} + F_{n-1}\\). To typowy przykład rekurencji binarnej i możemy łatwo napisać kod
Pythona dla tej funkcji::


    def fibonacci(n):
        # Returns the n-th Fibonacci number
        if n <= 1:
            return n
        else:
            return fibonacci(n-2) + fibonacci(n-1)

Niech \\(c_n\\) będzie liczbą wywołań wykonanych w wykonaniu \\(fibonacci(n)\\).
Mamy następujące wartości dla (c_n):

| \\(c_0 = 1\\)
| \\(c_1 = 1\\)
| \\(c_2 = 1+c_0+c_1 = 1+1+1 = 3\\)
| \\(c_3 = 1+c_1+c_2 = 1+1+3 = 5\\)
| \\(c_4 = 1+c_2+c_3 = 1+3+5 = 9\\)
| \\(c_5 = 1+c_3+c_4 = 1+5+9 = 15\\)
| \\(c_6 = 1+c_4+c_5 = 1+9+15 = 25\\)
| \\(c_7 = 1+c_5+c_6 = 1+15+25 = 41\\)
| \\(c_8 = 1+c_6+c_7 = 1+25+41 = 67\\)

Widzimy, że naturalna implementacja formuły Fibonacciego skutkuje
quasi-wykładniczym wzrostem liczby połączeń. Dzieje się tak, ponieważ
nasza definicja nie pamięta wcześniej obliczonych wartości sekwencji, co
oznacza, że każde wywołanie musi ponownie obliczyć wartości, które
zostały już obliczone. Na przykład podczas obliczania ``fibonacci(6)`` mamy
dwa wywołania ``fibonacci(4)``, jedno wykonane bezpośrednio przez
``fibonacci(6)``, a drugie wykonane przez ``fibonacci(5)``. Podobnie mamy trzy
wywołania ``fibonacciego(3)``: pierwsze, wykonane przez ``fibonacciego(5)``,
druga i trzecia wykonane przez dwie instancje ``fibonacciego(4)``.

Istnieją bardziej efektywne sposoby na przedefiniowanie tego programu.
Pierwsze podejście polega na zdefiniowaniu programu, który zwraca dwie
kolejne liczby Fibonacciego, \\(( F_n ,F_{n-1} )\\), zaczynając od \\(F_{1}=0\\). To pozwala nam zapamiętać wcześniej obliczoną wartość ciągu i
przekazać ją do następnego poziomu obliczeń, zamiast przeliczać ją
ponownie::


    def good_fibonacci(n):
        # Returns the n-th and the (n-1)-th Fibonacci number
        if n <= 1:
            return (n,0)
        else:
            (a,b) = good_fibonacci(n-1)
            return (a+b,a)

Funkcję ``good_fibonacci(n)`` można obliczyć w czasie \\(O(n)\\). Rzeczywiście,
każde wywołanie rekurencyjne zmniejsza argument \\(n\\) o 1, implikując
wywołania funkcji \\(n\\). Nierekurencyjna praca dla każdego wywołania
wymaga stałego czasu. W ten sposób ogólne obliczenia są wykonywane w
czasie \\(O(n)\\).

Pamięć możemy zaimplementować również za pomocą słownika do zapisywania
wcześniej obliczonych wartości::


    memo = {0:0, 1:1}
    def fibm(n):
        if not n in memo:
            memo[n] = fibm(n-1) + fibm(n-2)
        return memo[n]

Finalny program można zdefiniować za pomocą metody ``call``::


    class Fibonacci:
        def __init__(self, a=0, b=1):
            self.memo = { 0:i1, 1:i2 }
        def __call__(self, n):
            if n not in self.memo:
                self.memo[n] = self.__call__(n-1) + self.__call__(n-2)
            return self.memo[n]
    fib = Fibonacci()
    lucas = Fibonacci(2, 1)
    for i in range(1, 16):
        print(i, fib(i), lucas(i))




Program zwraca następujące dane wyjściowe:

| 1 1 1
| 2 1 3
| 3 2 4
| 4 3 7
| 5 5 11
| 6 8 18
| 7 13 29
| 8 21 47
| 9 34 76
| 10 55 123
| 11 89 199
| 12 144 322
| 13 233 521
| 14 377 843
| 15 610 1364


Liczby Lucasa lub szeregi Lucasa to ciąg liczb całkowitych nazwanych na
cześć matematyka Francois Edouarda Anatole Lucasa (1842–91). Liczby
Lucasa mają tą samą zasadę tworzenia, co liczba Fibonacciego, ale
wartości 0 i 1 są różne.

Podrodział 3.2: Problem unikatowości elementu
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mając ciąg \\(A\\) z elementami \\(n\\), problem jednoznaczności elementu polega
na ustaleniu czy wszystkie elementy tego ciągu różnią się od siebie.

Pierwsze rozwiązanie tego problemu wykorzystuje algorytm iteracyjny. W
szczególności, dla każdej pary indeksów \\(i\\) i \\(j\\), z \\(i<j\\), sprawdzamy,
czy \\(A[i]\\) i \\(A[j]\\) są równe::


    def unique1(A):
        # Returns True if there are no duplicate elements in A
        for i in range(len(A)):
            for j in range(i+1, len(A)):
                if A[i] == A[j]:
                    return False       # there is a duplicate pair
        return True              # no duplicate elements


Jak widać, istnieją dwie zagnieżdżone pętle; iteracje pętli zewnętrznej
powodują odpowiednio \\(n-1, n-2, \ldots, 2, 1\\) iteracje
pętli wewnętrznej. Oznacza to, że w najgorszym przypadku (gdy nie ma
zduplikowanych elementów) program działa w krokach \\((n-1)+(n-2)+\ldots +2+1\\), czyli dla \\(O( n^2)\\) kroków.

Drugi algorytm dla problemu jednoznaczności elementu opiera się na
wymaganiu, aby sekwencja \\(A\\) została posortowana. Oznacza to, że jeśli
istnieją duplikaty elementu, kopie te zostaną umieszczone razem w
sekwencji. Potrzebne jest pojedyncze przejście nad sekwencją, porównując
każdy element z następnym. Kodowanie tego algorytmu w Pythonie wygląda
następująco::


    def unique2(A):
        # Returns True if there are no duplicate elements in A
        temp = sorted(A)
            for i in range(1, len(temp)):
                if temp[i-1] == temp[i]:
                    return False       # there is a duplicate pair
            return True            # no duplicate elements




Jeśli sekwencja \\(A\\) jest posortowana, algorytm potrzebuje tylko kroków
\\(O(n)\\) do rozstrzygnięcia problemu. Całkowite zużycie czasu wynosi \\(O(n\log n)\), biorąc pod uwagę złożoność czasową funkcji
sortującej.

Bardzo niewydajny algorytm rekurencyjny opiera się na następującym
założeniu:

jako przypadek bazowy, gdy \\(n = 1\\), elementy są trywialnie unikalne; dla
\\(n > 2\\), elementy są niepowtarzalne wtedy i tylko wtedy, gdy pierwsze
\\(n-1\\) elementy są niepowtarzalne, ostatnie \\(n-1\\) elementy są
niepowtarzalne, a pierwszy i ostatni element są różne.

Implementacja rekurencyjna jest podana poniżej::


     def unique3(A, first, last):
        # Returns True if there are no duplicate elements in A[first:last]
        if last - first <= 1:
            return True               # there is only one item
        elif not unique3(A, first, last-1):
            return False              # the first part has a duplicate
        elif not unique3(a, first+1, last):
            return False              # the second part has duplicate
        else:
            return A[first] != A[last-1]       # first and last elements differ

Niech (n) będzie liczbą elementów w sekwencji, czyli \\(n\\) = ``last-first``. Jeśli \\(n=1\\), to czas działania unique3 wynosi \\(O(1)\\).
W ogólnym przypadku zauważmy, że wywołanie ``unique3`` skutkuje dwoma
rekurencyjnymi wywołaniami problemów o rozmiarze \\(n-1\\). Te dwa wywołania
mogą skutkować czterema wywołaniami problemów o rozmiarze \\(n-2\\) i tak
dalej. Całkowitą liczbę wywołań funkcji podaje suma \\(1+2+4+\ldots + 2^{n-1}\\), która jest równa \\(2^{n-1}\\). Zatem czas
działania funkcji ``unique3`` wynosi \\(O(2^n)\\).

Ulepszone rozwiązanie podane jest w poniższym skrypcie::


    def unique(A, first, last):
        if last-first <= 1:                # there is only one item
            return True
        elif A[first] in A[first+1:]:      # if first element is in the rest of the list
            return False
        else:
            return unique(A, first+1, last)  # next element
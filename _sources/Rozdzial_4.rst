Rozdział 4: Stosy, kolejki i dekolejki
**************************************

Podrodział 1: Abstrakcyjny typ danych stosu
-------------------------------------------

Stos to zbiór obiektów wstawianych i usuwanych zgodnie z zasadą
„ostatnie na wejściu – pierwsze na wyjściu” (LIFO). Użytkownik może
uzyskać dostęp tylko do stosu, dodając jeden obiekt lub usuwając
ostatnio wstawiony. Ten obiekt nazywa się wierzchołkiem stosu. Zatem
podstawowe operacje na stosie obejmują nakładanie obiektu na stos i
zdejmowanie obiektu ze stosu. Prostym przykładem jest stos talerzy w
obciążonym sprężyną dozowniku talerzy kafeteryjnych.

Stosy należą do podstawowych struktur danych i są używane w wielu
aplikacjach. Na przykład lista ostatnio odwiedzanych witryn w
przeglądarce internetowej jest zaimplementowana jako stos. Za każdym
razem, gdy witryna jest odwiedzana, jej adres jest umieszczany w stosie,
a za każdym razem, gdy naciskany jest przycisk „wstecz”, przeglądarka
wyskakuje do poprzednio odwiedzanej witryny. Innym przykładem jest
mechanizm „cofnij” w edytorach tekstu.

Formalnie stos \\(S\\) jest abstrakcyjnym typem danych obsługującym dwie
metody (błąd pojawia się za każdym razem, gdy \\(S\\) jest pusty):

-  **S.push(e)**: dodaj element e na górze S;
-  **S.pop()**: zwraca górny element z S i usuwa go.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura41.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Więcej metod:

-  **S.top()**: zwraca górny element S, nie usuwając go;
-  **S.is_empty()**: zwraca True jeśli S nie zawiera żadnych elementów;
-  **len(S)**: zwróć liczbę elementów w S.

Nowy stos jest tworzony pusty i nie ma limitu pojemności stosu. Elementy
dodawane do stosu mogą mieć dowolny typ.

Podrozdział 1.1: Implementacja za pomocą listy Pythona
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Stos można zaimplementować za pomocą listy Pythona. Klasa ``list`` obsługuje
dodawanie elementu na jego końcu (metodą ``append``) i usuwanie ostatniego
elementu (metodą ``pop``). Jeśli zdefiniujemy wierzchołek (definiowanego)
stosu jako koniec listy, poprzednie metody stosu można łatwo
zdefiniować. Wydaje się naturalne, że klasa ``list`` może być używana
bezpośrednio jako stos, bez definiowania klasy ``stack`` stosu, ale to złamałoby
czystą abstrakcję nowego ADT, biorąc pod uwagę, że klasa ``list`` zawiera
więcej metod niż te należące do stosu.


.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura42.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Poniżej pokażemy, jak używać listy do implementacji stosu. W
szczególności definiujemy nową klasę, która wykonuje niektóre z tych
samych funkcji istniejącej klasy, przepakowanej w wygodniejszy sposób.
Definiujemy nową klasę ``stack`` stosu w taki sposób, aby zawierała instancję
istniejącej klasy ``list`` jako pole ukryte, a każdą metodę nowej klasy
implementujemy za pomocą metod tej zmiennej instancji ukrytej. Ta
adaptacja została przedstawiona w poniższej tabeli.

============== ============================
Stack methods  Realization with Python list
============== ============================
S.push(e)      L.append(e)
S.pop( )       L.pop( )
S.top()        L[−1]
S.is empty( )  len(L) == 0
len(S)         len(L)
============== ============================

Formalna definicja znajduje się w następującym fragmencie kodu::

 1  class ArrayStack:
 2    # LIFO Stack implementation using a Python list as storage
 3
 4    # Creates an empty stack
 5    def __ init __ (self):
 6      self._data = [ ]
 7
 8    # Returns the number of elements in the stack
 9    def __ len __ (self):
 10     return len(self._data)
 11
 12   # Returns True if the stack is empty
 13   def is_empty(self):
 14     return len(self._data) == 0
 15
 16   # Adds element e to the top of the stack
 17   def push(self, e):
 18     self._data.append(e)
 19
 20   # Returns (but don't remove) the element at the top of the stack
 21   # raising an exception if the stack is empty
 22   def top(self):
 23     if self.is_empty( ):
 24       raise Empty( 'Stack is empty' )
 25     return self._data[−1]
 26
 27   # Remove and return the element from the top of the stack
 28   # raising an exception if the stack is empty
 29   def pop(self):
 30     if self.is_empty( ):
 31       raise Empty( 'Stack is empty' )
 32     return self._data.pop( )

Zauważ, że gdy użytkownik wywoła ``pop`` lub ``top``, gdy stos jest pusty - musi
wystąpić błąd. Aby tak się stało, definiujemy klasę wyjątku ``Empty`` w
następujący sposób::

 1  # Error when trying to access an element from an empty list
 2  class Empty(Exception):
 3    pass
 
Poniżej przedstawiamy przykład użycia klasy Stack, odpowiednio z
operacjami, zawartością stosu i danymi wyjściowymi.

==================== ===================== =======
Operations           Contents of the stack Output
==================== ===================== =======
S=Stack( )           [ ]
S.push(5)            [5]
S.push(3)            [5, 3]
print(len(S))        [5, 3]                2
print(S.pop( ))      [5]                   3
print(S.is empty())  [5]                   False
print(S.pop( ))      [ ]                   5
print(S.is empty())  [ ]                   True
S.push(7)            [7]
S.push(9)            [7, 9]
print(S.top( ))      [7, 9]                9
S.push(4)            [7, 9, 4]
print(len(S))        [7, 9, 4]             3
==================== ===================== =======

Analiza poprzedniej (opartej na tablicach) implementacji stosu
odzwierciedla analizę klasy listy. ``top``, ``is_empty`` i ``len`` używają, w
najgorszym przypadku, stałego czasu. Typowe wywołanie ``push`` i ``pop`` używa
stałego czasu, ale możliwy jest najgorszy przypadek \\(O(n)\\)-czasu \\((n\\) to
bieżąca liczba elementów na stosie), gdy operacja powoduje wyświetlenie
listy, aby zmienić rozmiar wewnętrznej tablicy. Wykorzystanie miejsca na
stosie to \\(O(n)\\).

Stos z poprzedniego fragmentu kodu zaczyna się od pustej listy, która
jest rozwijana w razie potrzeby. Bardziej wydajną implementację można
uzyskać konstruując listę o początkowej długości \\(n\\), jeśli z jakiegoś
powodu wiemy z góry, że stos osiągnie ten rozmiar. W takim przypadku
konstruktor może zaakceptować parametr określający maksymalną pojemność
stosu i zainicjować element danych do listy o tej długości. Oznacza to,
że rozmiar stosu nie będzie już równoznaczny z długością listy, a push i
pop nie będą wymagały zmiany długości listy.

Podrozdział 1.2: Przykład - odwracanie stosu
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Stos może służyć jako narzędzie do odwracania sekwencji danych. Jeśli
wartości 1, 2 i 3 są odkładane na stos w tej kolejności, zostaną zdjęte
ze stosu w kolejności 3, 2 i 1. To zachowanie może być używane w różnych
przypadkach. Załóżmy na przykład, że chcemy wydrukować wiersze pliku w
odwrotnej kolejności. Można to osiągnąć, czytając każdy wiersz i
wkładając go na stos, a następnie zapisując wiersze w kolejności, w
której wyskakują. Implementacja takiego procesu jest podana w poniższym
kodzie::

 1  def reverse_file(filename):
 2    # Overwrite a file with its contents line-by-line reversed
 3    S = ArrayStack()
 4    original = open(filename)
 5    for line in original:
 6      S.push(line.rstrip( '\n' ))   # re-insert newlines when writing
 7    original.close( )
 8
 9    # overwrite with contents in LIFO order
 10   output = open(filename,'w' )       # reopening file overwrites original
 11   while not S.is_empty( ):
 12     output.write(S.pop( ) + '\n' )       # reinsert newline characters
 13   output.close( )

Podrozdział 1.3: Dopasowywanie nawiasów
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dwa przydatne zastosowania stosów są związane z testowaniem par
pasujących ograniczników, takich jak nawiasy lub znaczniki HTML. Rozważ
wyrażenia arytmetyczne, które mogą zawierać różne pary symboli
grupujących, takie jak nawiasy "(” and ")”, nawiasy klamrowe "{” and "}”
oraz nawiasy kwadratowe "[” and "]”, gdzie każdy symbol otwierający musi
odpowiadać odpowiadającemu mu symbol zamknięcia. Na przykład ciągi ( )((
)){([( )])} i ((( )(( )){([( )])})) są poprawne, natomiast )(( )){([ (
)])}, ({[ ])} i ( są nieprawidłowe.

Ważnym zadaniem podczas przetwarzania wyrażeń arytmetycznych jest
upewnienie się, że symbole grupujące są prawidłowo dopasowane. W
poniższym kodzie czytelnik może znaleźć implementację takiego algorytmu
w Pythonie::


 1  def ismatched(expr):
 2    # Returns True if all delimiters match properly; False otherwise
 3    lefty = '({['           # opening delimiters
 4    righty = ')}]'          # closing delimiters
 5    S = ArrayStack()
 6    for c in expr:
 7      if c in lefty:
 8        S.push(c)           # push left delimiter on stack
 9      elif c in righty:
 10       if S.is_empty( ):
 11         return False          # nothing to match with
 12       if righty.index(c) != lefty.index(S.pop( )):
 13         return False          # mismatched
 14    return S.is_empty( )       # all symbols matched

Biorąc pod uwagę sekwencję znaków jako dane wejściowe, wykonywane jest
skanowanie od lewej do prawej. Za każdym razem, gdy napotkany zostanie
symbol otwierający, ten symbol jest wstawiany do \\(S\\). Za każdym razem,
gdy napotkany zostanie symbol zamykający, wykonywane jest zdjęcie ze
stosu \\(S\\) (zakładając, że \\(S\\) nie jest puste) i sprawdzane jest, czy te
dwa symbole tworzą poprawną parę. Jeśli osiągnięto koniec wyrażenia, a
stos jest pusty, oznacza to, że oryginalne wyrażenie zostało prawidłowo
dopasowane. W przeciwnym razie na stosie musi znajdować się symbol
otwarcia bez pasującego symbolu zamknięcia.

Jeśli długość oryginalnego wyrażenia wynosi \\(n\\), algorytm wykona co
najwyżej \\(n\\) wywołania ``push`` i \\(n\\) wywołania ``pop``, działając w czasie
\\(O(n)\\).

Podrozdział 1.4: Dopasowywanie znaczników HTML
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Inną użyteczną aplikacją dopasowywania ograniczników jest sprawdzanie
języków takich jak HTML lub XML. HTML jest językiem znaczników i
reprezentuje standardowy format dokumentów w Internecie; XML to
rozszerzalny język znaczników używany do różnych ustrukturyzowanych
zestawów danych.

Fragmenty tekstu w dokumencie HTML są oddzielone znacznikami. Znacznik
otwierający jest zapisany jako <name>, a odpowiadający mu
znacznik zamykający to </name>. Typowe znaczniki to
<body> (treść dokumentu), <h1> (nagłówek sekcji),
<center> (wyśrodkowanie), <p> (akapit) wraz z tagami dla
list uporządkowanych i numerowanych. Każdy tag otwierający powinien
pasować do tagu zamykającego, tak jak w poprzednim problemie z
dopasowywaniem nawiasów. Aby sprawdzić, czy dokument HTML ma tę
właściwość, zdefiniowany jest następujący kod, taki jak otwierające
znaczniki są wpychane do stosu i dopasowywane do zamykających
znaczników, gdy są zdejmowane ze stosu. Algorytm działa w czasie \\(O(n)\\),
gdzie \\(n\\) to liczba znaków w nieprzetworzonym źródle HTML::

 1  def is_matched_html(raw):
 2    # Returns True if all HTML tags match properly; False otherwise
 3    S = ArrayStack()
 4    j = raw.find('<')                 # find first < character
 5    while j != −1:
 6      k = raw.find('>' , j+1)         # find next > character
 7      if k == −1:
 8        return False                  # invalid tag
 9      tag = raw[j+1:k]                # strip away < >
 10     if not tag.startswith( '/' ):   # this is an opening tag
 11       S.push(tag)
 12     else:                           # this is a closing tag
 13       if S.is_empty( ):
 14         return False                # nothing to match with
 15       if tag[1:] != S.pop():
 16         return False                # mismatched delimiter
 17     j = raw.find( '<' , k+1)          # find next < character (if any)
 18   return S.is empty( )              # were all opening tags matched?


Podrozdział 2: Typ danych abstrakcji kolejki
--------------------------------------------

Kolejka to zbiór obiektów, które są wstawiane i usuwane zgodnie z zasadą
“pierwsze na wejściu, pierwsze na wyjściu” (FIFO). Oznacza to, że
elementy wchodzą w kolejkę z tyłu i są usuwane z przodu. Poza oczywistym
zastosowaniem kolejek, są one wykorzystywane przez wiele urządzeń
komputerowych, takich jak drukarka sieciowa, serwer WWW odpowiadający na
żądania czy systemy operacyjne.

Aby wymusić zasadę FIFO, abstrakcyjny typ danych kolejki definiuje
kolekcję obiektów, w których dostęp i usuwanie elementów są dozwolone na
pierwszym elemencie w kolejce, a wstawianie elementów jest dozwolone z
tyłu sekwencji.

Formalnie kolejka \\(Q\\) jest abstrakcyjnym typem danych obsługującym dwie
metody (występuje błąd, jeśli \\(Q\\) jest puste):

-  **Q.enqueue(e)**: dodaj element e z tyłu kolejki Q;
-  **Q.dequeue()**: usuń i zwróć pierwszy element z kolejki Q.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura43.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Kolejka ADT zawiera również następujące metody wspierające:

-  **Q.first()**: zwraca element na początku kolejki Q, bez usuwania go;
-  **Q.is_empty()**: zwraca True jeśli kolejka Q nie zawiera żadnych
   elementów.
-  **len(Q)**: zwraca liczbę elementów w kolejce Q.

Nowa kolejka jest tworzona pusta i nie ma limitu pojemności kolejki.
Elementy dodawane do kolejki mogą mieć dowolny typ.

Podrozdział 2.1: Implementacja za pomocą listy Pythona
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Prostym podejściem do implementacji ADT kolejki ``queue`` może być zaadaptowanie
klasy ``list`` Pythona, w ten sam sposób, w jaki zrobiliśmy to dla ADT
stosu ``stack``. Możemy umieścić element e w kolejce, wywołując ``append(e)`` w celu
dodania go na koniec listy oraz możemy użyć składni ``pop(0)`` (w
przeciwieństwie do ``pop()``), aby usunąć pierwszy element z listy. To
rozwiązanie jest wysoce nieefektywne. Kiedy ``pop`` jest wywoływany na
liście z indeksem innym niż domyślny, wszystkie elementy poza podanym
indeksem muszą być przesunięte w lewo, aby “wypełnić dziurę” w sekwencji
spowodowanej przez pop. Wywołanie ``pop(0)`` zawsze powoduje najgorszy
scenariusz, gdy cała lista musi zostać przesunięta (w czasie \\(O(n)\\)).

Inne rozwiązanie może całkowicie uniknąć wywołania ``pop(0)``. Moglibyśmy
zastąpić usunięty z kolejki element w tablicy referencją do ``None``
(unikając szkodliwego przesunięcia) i przechowywać indeks elementu,
który aktualnie znajduje się na początku kolejki w zmiennej. Taki
algorytm dekolejki działałby w czasie \\(O(1)\\).

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura44.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Takie podejście ma wadę: długość podstawowej listy może być dowolnie
duża, podczas gdy elementów kolejki jest bardzo niewiele. Rozważmy
przypadek powtarzającej się sekwencji kolejkowania i usuwania elementów
(czyli kolejek o małym rozmiarze, ale używanych przez długi czas). Z
biegiem czasu rozmiar podstawowej listy wzrośnie do \\(O(m)\\), gdzie \\(m\\)
jest całkowitą liczbą operacji wpisywania do kolejki od momentu
utworzenia kolejki, a nie rzeczywistą liczbą elementów w kolejka.

Podrozdział 2.2: Implementacja za pomocą szyku kołowego
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Wydajną implementację kolejki można stworzyć przy użyciu struktury
tablicy kołowej. Załóżmy, że podstawowa tablica ma stałą długość \\(N\\),
która jest większa niż rzeczywista liczba elementów w kolejce.
Pozwalamy, aby nagłówek kolejki przesuwał się w prawo (jak w poprzedniej
implementacji), ale pozwalamy również, aby nowy element był dodawany do
pierwszego wolnego elementu tablicy, zaczynając od początku do indeksu
\\(N−1\\) i kontynuując w indeksie 0, a następnie 1, jeśli indeksowane
elementy są wolne.

Jedynym wymaganiem jest obliczenie indeksu frontowego \\(f\\), gdy element
jest usuwany z kolejki. Ta wartość to \\((f+1)% N\\) , z \\(a% b\\) pozostałą
częścią \\(a\\) podzieloną przez \\(b\\). Na przykład, jeśli lista ma długość
równą 10, a indeks frontowy 7, nowy indeks frontowy to \\((7+1) % 10\\), co
jest zgodne z oczekiwaniami. Jeśli indeks przedni to 9 (ostatni w
tablicy), indeks „zaawansowany” otrzyma indeks 0, co oznacza, że musimy
umieścić nowy element w kolejce na pierwszej pozycji tablicy.


.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura45.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Implementacja kolejki ``queue`` ADT przy użyciu listy cyklicznej Pythona jest
przedstawiona w poniższym kodzie. Nowa klasa utrzymuje trzy zmienne
instancji:

-  _data: odwołanie do wystąpienia listy o stałej pojemności;
-  _size: aktualna liczba elementów przechowywanych w kolejce;
-  _front: indeks pierwszego elementu kolejki w \_data.

Chociaż kolejka ma rozmiar zero, początkowo rezerwujemy listę o
umiarkowanym rozmiarze do przechowywania danych, z indeksem frontowym
równym zero. Wyjątek Empty jest zgłaszany, gdy ``front`` lub ``dequeue`` są
wywoływane bez elementów w kolejce. W poniższym kodzie przedstawiamy
implementację kolejki wykorzystującą poprzednie pomysły::

 1  class ArrayQueue:
 2    # queue implemented with a list
 3    DEFAULT = 10          # capacity of all queues
 4
 5    def __init__(self):
 6      self._data = [None]*ArrayQueue.DEFAULT
 7      self._size = 0
 8      self._front = 0
 9
 10   def __len__(self):
 11     return self._size
 12
 13   def is_empty(self):
 14     return self._size == 0
 15
 16   def first(self):
 17     if self.is_empty( ):
 18       raise Empty('Queue is empty')
 19     return self._data[self._front]
 20
 21   def dequeue(self):
 22     if self.is_empty( ):
 23       raise Empty('Queue is empty')
 24     element = self._data[self._front]
 25     self._data[self._front] = None
 26     self._front = (self._front + 1) % len(self._data)
 27     self._size −= 1
 28     return element
 29
 30   def enqueue(self, e):
 31     if self._size == len(self._data):
 32       self._resize(2*len(self.data))           # double the array size
 33     position = (self._front + self._size) % len(self._data)
 34     self._data[position] = e
 35     self._size += 1
 36
 37   def _resize(self, capacity):
 38     old = self._data                   # keeps track of existing list
 39     self._data = [None]*capacity       # allocate list with new capacity
 40     walk = self._front
 41     for k in range(self._size):
 42       self._data[k] = old[walk]        # shifts the indices
 43       walk = (1 + walk) % len(old)     # use old size
 44     self._front = 0                    # realigs front

Metoda ``enqueue`` dodaje nowy element do kolejki. Aby to zrobić, odpowiedni
indeks jest obliczany przez ``position = (self._front + self._size) % len(self._data)``. Rozważmy na przykład kolejkę o pojemności 10, bieżącym
rozmiarze 3 i pierwszym elemencie o indeksie 5. Oznacza to, że elementy
takiej kolejki są przechowywane w indeksach 5, 6 i 7; Nowy element jest
umieszczany pod indeksem (5+3) % 10 = 8. Jeśli kolejka ma 3 elementy z
pierwszym pod indeksem 8, nowy element jest umieszczany pod indeksem
(8+3) % 10 = 1, ponieważ trzy istniejące elementy zajmują indeksy 8, 9 i
0.

Gdy wywoływana jest metoda ``dequeue``,
``element = self._data[self._front]`` jest elementem, który zostanie
usunięty i zwrócony, a self._front jest indeksem tej wartości. Następnie
wartość ``_front`` jest aktualizowana, aby odzwierciedlić usunięcie
elementu, a drugi element staje się nowym pierwszym; ze względu na
konfigurację kołową wartość ta jest obliczana przez arytmetykę
modularną, jak poprzednio.

Metoda ``resize`` jest wywoływana, gdy rozmiar kolejki i rozmiar podstawowej
listy \\((N)\\) są równe, po prostu podwajając długość listy. Odbywa się to
poprzez utworzenie tymczasowego odniesienia do starej listy,
przydzielenie nowej listy, która jest dwa razy większa, oraz skopiowanie
odwołań ze starej listy do nowej. Podczas kopiowania przód kolejki jest
wyrównywany z indeksem 0 w nowej tablicy.

Rozsądnym ulepszeniem poprzedniego kodu powinno być zredukowanie tablicy
do połowy jej pojemności, gdy rzeczywista liczba elementów jest równa
lub mniejsza niż jedna czwarta jej pojemności. W rzeczywistości dana
implementacja powiększa macierz za każdym razem, gdy zostaje osiągnięta
jej pełna pojemność, ale nigdy nie zmniejsza macierzy. Oznacza to, że
pojemność tablicy bazowej jest proporcjonalna do maksymalnej liczby
elementów, które kiedykolwiek były przechowywane w kolejce, a nie do
bieżącej liczby elementów.

Aby uzyskać tę właściwość, możemy dodać następujące dwa wiersze kodu w
metodzie ``dequeue``, tuż przed ``return element`` zwracającym wiersz::

 1  if 0 < self._size < len(self._data) / 4:
 2    self._resize(len(self._data) / 2)

Na koniec zauważamy, że wszystkie metody opierają się na stałej liczbie
instrukcji zawierających operacje arytmetyczne, porównania i
przypisania. Dlatego każda metoda działa w najgorszym przypadku \\(O(1)\\),
z wyjątkiem narzędzia zmiany rozmiaru ``resize``.

Podrozdział 3: Podwójna kolejka
-------------------------------

Kolejka z podwójnym zakończeniem (deque) to struktura danych, która
obsługuje wstawianie i usuwanie zarówno z przodu, jak i z tyłu kolejki.
Ten abstrakcyjny typ danych jest bardziej ogólny niż ADT stosu ``stack`` i
kolejki ``queue``.

Formalnie ``deque`` ADT jest zdefiniowany tak, że obsługuje następujące
metody (występuje błąd, jeśli deque jest pusty):

-  **D.add_first(e)**: dodaj element e na początku deque D;
-  **D.add_last(e)**: dodaj element e z tyłu deque D;
-  **D.delete_first()**: usuń i zwróć pierwszy element z deque D;
-  **D.delete_last()**: usuń i zwróć ostatni element z deque D.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura46.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Deque będzie zawierał następujące akcesory:

-  **D.first()**: zwraca (ale nie usuwa) pierwszy element deque D;
-  **D.last()**: zwraca (ale nie usuwa) ostatni element deque D;
-  **D.is_empty()**: zwraca True jeśli deque D nie zawiera żadnych
   elementów;
-  **len(D)**: zwróć liczbę elementów w deque D.

Podrozdział Sekcja 3.1: Implementacja za pomocą szyku kołowego
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``Deque`` ADT można zaimplementować w taki sam sposób, jak kolejka podana
wcześniej; trzy zmienne instancji muszą być przechowywane jak
poprzednio: ``_data``, ``_size`` i ``_front``. Indeks z tyłu deque lub pierwszy
dostępny slot poza tyłem deque można obliczyć za pomocą arytmetyki
modularnej. Na przykład implementacja metody ``last()`` wykorzystuje indeks
``back = (self._front + self._size − 1) % len(self._data)``. Nowa
implementacja metody ``deque.add_last`` jest zasadniczo taka sama jak w
przypadku ``queue.enqueue``, wliczając w to narzędzie zmiany rozmiaru.
Podobnie implementacja metody ``deque.delete_first`` jest taka sama jak
``queue.dequeue``. Implementacje ``add_first`` i ``delete_last`` wykorzystują
podobne techniki. Wywołanie ``add_first`` może wymagać zawinięcia wokół
początku tablicy, więc ponownie używamy arytmetyki modularnej do
dekrementacji indeksu, jako ``self._front = (self._front − 1) % len(self._data)``. Skuteczność deque jest podobna do kolejki; wszystkie
operacje mają czas działania \\(O(1)\\), przy czym to powiązanie jest
amortyzowane dla operacji, które mogą zmienić rozmiar listy bazowej.
